﻿namespace Closers.Repacker.NBT
{
    using Closers.Repacker.IO;
    using System;
    using System.IO;
    using System.IO.Compression;
    using System.Text;

    public abstract class NBTValue
    {
        protected NBTValue()
        {
        }

        public static NBTValue Create(NBTType type)
        {
            switch (type)
            {
                case NBTType.Boolean:
                    return new NBTBoolean();

                case NBTType.SByte:
                    return new NBTSByte();

                case NBTType.Byte:
                    return new NBTByte();

                case NBTType.Short:
                    return new NBTShort();

                case NBTType.UShort:
                    return new NBTUShort();

                case NBTType.Integer:
                    return new NBTInteger();

                case NBTType.UInteger:
                    return new NBTUInteger();

                case NBTType.Long:
                    return new NBTLong();

                case NBTType.ULong:
                    return new NBTULong();

                case NBTType.Float:
                    return new NBTFloat();

                case NBTType.Double:
                    return new NBTDouble();

                case NBTType.String:
                    return new NBTString();

                case NBTType.Compound:
                    return new NBTCompound();
            }
            return null;
        }

        public override bool Equals(object obj)
        {
            NBTValue value2 = obj as NBTValue;
            return ((value2 != null) && (value2.Type == this.Type));
        }

        public override int GetHashCode()
        {
            return (int) this.Type;
        }

        protected abstract void OnRead(BinaryReader reader);
        protected abstract void OnWrite(BinaryWriter writer);
        public static NBTValue Read(BinaryReader reader)
        {
            NBTValue value2 = Create((NBTType) reader.ReadInt32());
            value2.OnRead(reader);
            return value2;
        }

        public static NBTValue ReadCompressionASCII(string raw)
        {
            int num2;
            byte[] buffer = new byte[raw.Length / 2];
            for (int i = 0; i < buffer.Length; i = num2)
            {
                string str = raw.Substring(i * 2, 2);
                buffer[i] = Convert.ToByte(str, 0x10);
                num2 = i + 1;
            }
            MemoryStream stream = new MemoryStream(buffer);
            NBTValue value2 = ReadCompressionBinary(stream);
            IOUtil.CloseAndDisposeQuitly(stream);
            return value2;
        }

        public static NBTValue ReadCompressionBinary(Stream stream)
        {
            DeflateStream input = new DeflateStream(stream, CompressionMode.Decompress, true);
            BinaryReader reader = new BinaryReader(input);
            NBTValue value2 = Read(reader);
            IOUtil.CloseAndDisposeQuitly(reader);
            IOUtil.CloseAndDisposeQuitly(input);
            return value2;
        }

        public static void Write(BinaryWriter writer, NBTValue value)
        {
            writer.Write((int) value.Type);
            value.OnWrite(writer);
        }

        public static string WriteCompressionASCII(NBTValue value)
        {
            int num2;
            MemoryStream stream = new MemoryStream();
            WriteCompressionBinary(stream, value);
            byte[] buffer = stream.ToArray();
            IOUtil.CloseAndDisposeQuitly(stream);
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < buffer.Length; i = num2)
            {
                builder.Append(buffer[i].ToString("X2"));
                num2 = i + 1;
            }
            string str = builder.ToString();
            builder.Clear();
            return str;
        }

        public static void WriteCompressionBinary(Stream stream, NBTValue value)
        {
            DeflateStream output = new DeflateStream(stream, CompressionMode.Compress, true);
            BinaryWriter writer = new BinaryWriter(output);
            Write(writer, value);
            IOUtil.CloseAndDisposeQuitly(writer);
            IOUtil.CloseAndDisposeQuitly(output);
        }

        public abstract NBTType Type { get; }
    }
}

