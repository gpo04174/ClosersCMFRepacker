﻿namespace Closers.Repacker.NBT
{
    using System;
    using System.IO;

    public class NBTBoolean : NBTValue
    {
        private bool _Value;

        public NBTBoolean()
        {
            this._Value = false;
        }

        public NBTBoolean(bool value)
        {
            this._Value = false;
            this.Value = value;
        }

        protected override void OnRead(BinaryReader reader)
        {
            this.Value = reader.ReadBoolean();
        }

        protected override void OnWrite(BinaryWriter writer)
        {
            writer.Write(this.Value);
        }

        public override NBTType Type
        {
            get
            {
                return NBTType.Boolean;
            }
        }

        public bool Value
        {
            get
            {
                return this._Value;
            }
            set
            {
                this._Value = value;
            }
        }
    }
}

