﻿namespace Closers.Repacker.NBT
{
    using System;
    using System.IO;

    public class NBTByte : NBTNumber
    {
        private byte _Value;

        public NBTByte()
        {
            this._Value = 0;
        }

        public NBTByte(byte value)
        {
            this._Value = 0;
            this.Value = value;
        }

        public override byte GetAsByte()
        {
            return this.Value;
        }

        public override double GetAsDouble()
        {
            return (double) this.Value;
        }

        public override float GetAsFloat()
        {
            return (float) this.Value;
        }

        public override int GetAsInteger()
        {
            return this.Value;
        }

        public override long GetAsLong()
        {
            return (long) this.Value;
        }

        public override object GetAsObject()
        {
            return this.Value;
        }

        public override sbyte GetAsSByte()
        {
            return (sbyte) this.Value;
        }

        public override short GetAsShort()
        {
            return this.Value;
        }

        public override uint GetAsUInteger()
        {
            return this.Value;
        }

        public override ulong GetAsULong()
        {
            return (ulong) this.Value;
        }

        public override ushort GetAsUShort()
        {
            return this.Value;
        }

        protected override void OnRead(BinaryReader reader)
        {
            this.Value = reader.ReadByte();
        }

        protected override void OnWrite(BinaryWriter writer)
        {
            writer.Write(this.Value);
        }

        public override NBTType Type
        {
            get
            {
                return NBTType.Byte;
            }
        }

        public byte Value
        {
            get
            {
                return this._Value;
            }
            set
            {
                this._Value = value;
            }
        }
    }
}

