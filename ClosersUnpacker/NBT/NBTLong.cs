﻿namespace Closers.Repacker.NBT
{
    using System;
    using System.IO;

    public class NBTLong : NBTNumber
    {
        private long _Value;

        public NBTLong()
        {
            this._Value = 0L;
        }

        public NBTLong(long value)
        {
            this._Value = 0L;
            this.Value = value;
        }

        public override byte GetAsByte()
        {
            return (byte) this.Value;
        }

        public override double GetAsDouble()
        {
            return (double) this.Value;
        }

        public override float GetAsFloat()
        {
            return (float) this.Value;
        }

        public override int GetAsInteger()
        {
            return (int) this.Value;
        }

        public override long GetAsLong()
        {
            return this.Value;
        }

        public override object GetAsObject()
        {
            return this.Value;
        }

        public override sbyte GetAsSByte()
        {
            return (sbyte) this.Value;
        }

        public override short GetAsShort()
        {
            return (short) this.Value;
        }

        public override uint GetAsUInteger()
        {
            return (uint) this.Value;
        }

        public override ulong GetAsULong()
        {
            return (ulong) this.Value;
        }

        public override ushort GetAsUShort()
        {
            return (ushort) this.Value;
        }

        protected override void OnRead(BinaryReader reader)
        {
            this.Value = reader.ReadInt64();
        }

        protected override void OnWrite(BinaryWriter writer)
        {
            writer.Write(this.Value);
        }

        public override NBTType Type
        {
            get
            {
                return NBTType.Long;
            }
        }

        public long Value
        {
            get
            {
                return this._Value;
            }
            set
            {
                this._Value = value;
            }
        }
    }
}

