﻿namespace Closers.Repacker.NBT
{
    using System;

    public abstract class NBTNumber : NBTValue
    {
        public static NBTNumber Create(object value)
        {
            NBTNumber number = null;
            if (value is byte)
            {
                return new NBTByte((byte) value);
            }
            if (value is sbyte)
            {
                return new NBTSByte((sbyte) value);
            }
            if (value is short)
            {
                return new NBTShort((short) value);
            }
            if (value is ushort)
            {
                return new NBTUShort((ushort) value);
            }
            if (value is int)
            {
                return new NBTInteger((int) value);
            }
            if (value is uint)
            {
                return new NBTUInteger((uint) value);
            }
            if (value is long)
            {
                return new NBTLong((long) value);
            }
            if (value is ulong)
            {
                return new NBTULong((ulong) value);
            }
            if (value is float)
            {
                return new NBTFloat((float) value);
            }
            if (value is double)
            {
                number = new NBTDouble((double) value);
            }
            return number;
        }

        public abstract byte GetAsByte();
        public abstract double GetAsDouble();
        public abstract float GetAsFloat();
        public abstract int GetAsInteger();
        public abstract long GetAsLong();
        public abstract object GetAsObject();
        public abstract sbyte GetAsSByte();
        public abstract short GetAsShort();
        public abstract uint GetAsUInteger();
        public abstract ulong GetAsULong();
        public abstract ushort GetAsUShort();
    }
}

