﻿namespace Closers.Repacker.NBT
{
    using System;
    using System.IO;

    public class NBTULong : NBTNumber
    {
        private ulong _Value;

        public NBTULong()
        {
            this._Value = 0L;
        }

        public NBTULong(ulong value)
        {
            this._Value = 0L;
            this.Value = value;
        }

        public override byte GetAsByte()
        {
            return (byte) this.Value;
        }

        public override double GetAsDouble()
        {
            return (double) this.Value;
        }

        public override float GetAsFloat()
        {
            return (float) this.Value;
        }

        public override int GetAsInteger()
        {
            return (int) this.Value;
        }

        public override long GetAsLong()
        {
            return (long) this.Value;
        }

        public override object GetAsObject()
        {
            return this.Value;
        }

        public override sbyte GetAsSByte()
        {
            return (sbyte) this.Value;
        }

        public override short GetAsShort()
        {
            return (short) this.Value;
        }

        public override uint GetAsUInteger()
        {
            return (uint) this.Value;
        }

        public override ulong GetAsULong()
        {
            return this.Value;
        }

        public override ushort GetAsUShort()
        {
            return (ushort) this.Value;
        }

        protected override void OnRead(BinaryReader reader)
        {
            this.Value = reader.ReadUInt64();
        }

        protected override void OnWrite(BinaryWriter writer)
        {
            writer.Write(this.Value);
        }

        public override NBTType Type
        {
            get
            {
                return NBTType.ULong;
            }
        }

        public ulong Value
        {
            get
            {
                return this._Value;
            }
            set
            {
                this._Value = value;
            }
        }
    }
}

