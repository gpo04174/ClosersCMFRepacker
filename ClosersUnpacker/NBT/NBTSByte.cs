﻿namespace Closers.Repacker.NBT
{
    using System;
    using System.IO;

    public class NBTSByte : NBTNumber
    {
        private sbyte _Value;

        public NBTSByte()
        {
            this._Value = 0;
        }

        public NBTSByte(sbyte value)
        {
            this._Value = 0;
            this.Value = value;
        }

        public override byte GetAsByte()
        {
            return (byte) this.Value;
        }

        public override double GetAsDouble()
        {
            return (double) this.Value;
        }

        public override float GetAsFloat()
        {
            return (float) this.Value;
        }

        public override int GetAsInteger()
        {
            return this.Value;
        }

        public override long GetAsLong()
        {
            return (long) this.Value;
        }

        public override object GetAsObject()
        {
            return this.Value;
        }

        public override sbyte GetAsSByte()
        {
            return this.Value;
        }

        public override short GetAsShort()
        {
            return this.Value;
        }

        public override uint GetAsUInteger()
        {
            return (uint) this.Value;
        }

        public override ulong GetAsULong()
        {
            return (ulong) this.Value;
        }

        public override ushort GetAsUShort()
        {
            return (ushort) this.Value;
        }

        protected override void OnRead(BinaryReader reader)
        {
            this.Value = reader.ReadSByte();
        }

        protected override void OnWrite(BinaryWriter writer)
        {
            writer.Write(this.Value);
        }

        public override NBTType Type
        {
            get
            {
                return NBTType.SByte;
            }
        }

        public sbyte Value
        {
            get
            {
                return this._Value;
            }
            set
            {
                this._Value = value;
            }
        }
    }
}

