﻿namespace Closers.Repacker.NBT
{
    using System;
    using System.IO;

    public class NBTUShort : NBTNumber
    {
        private ushort _Value;

        public NBTUShort()
        {
            this._Value = 0;
        }

        public NBTUShort(ushort value)
        {
            this._Value = 0;
            this.Value = value;
        }

        public override byte GetAsByte()
        {
            return (byte) this.Value;
        }

        public override double GetAsDouble()
        {
            return (double) this.Value;
        }

        public override float GetAsFloat()
        {
            return (float) this.Value;
        }

        public override int GetAsInteger()
        {
            return this.Value;
        }

        public override long GetAsLong()
        {
            return (long) this.Value;
        }

        public override object GetAsObject()
        {
            return this.Value;
        }

        public override sbyte GetAsSByte()
        {
            return (sbyte) this.Value;
        }

        public override short GetAsShort()
        {
            return (short) this.Value;
        }

        public override uint GetAsUInteger()
        {
            return this.Value;
        }

        public override ulong GetAsULong()
        {
            return (ulong) this.Value;
        }

        public override ushort GetAsUShort()
        {
            return this.Value;
        }

        protected override void OnRead(BinaryReader reader)
        {
            this.Value = reader.ReadUInt16();
        }

        protected override void OnWrite(BinaryWriter writer)
        {
            writer.Write(this.Value);
        }

        public override NBTType Type
        {
            get
            {
                return NBTType.UShort;
            }
        }

        public ushort Value
        {
            get
            {
                return this._Value;
            }
            set
            {
                this._Value = value;
            }
        }
    }
}

