﻿namespace Closers.Repacker.NBT
{
    using System;

    public enum NBTType
    {
        Boolean,
        SByte,
        Byte,
        Short,
        UShort,
        Integer,
        UInteger,
        Long,
        ULong,
        Float,
        Double,
        String,
        Compound
    }
}

