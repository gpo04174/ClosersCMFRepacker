﻿namespace Closers.Repacker.NBT
{
    using Closers.Repacker.IO;
    using System;
    using System.IO;

    public class NBTString : NBTValue
    {
        private string _Value;

        public NBTString()
        {
            this._Value = null;
        }

        public NBTString(string value)
        {
            this._Value = null;
            this.Value = value;
        }

        protected override void OnRead(BinaryReader reader)
        {
            this.Value = IOUtil.ReadString(reader);
        }

        protected override void OnWrite(BinaryWriter writer)
        {
            IOUtil.WriteString(writer, this.Value);
        }

        public override NBTType Type
        {
            get
            {
                return NBTType.String;
            }
        }

        public string Value
        {
            get
            {
                return this._Value;
            }
            set
            {
                this._Value = value;
            }
        }
    }
}

