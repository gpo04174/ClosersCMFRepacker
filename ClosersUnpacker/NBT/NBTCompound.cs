﻿namespace Closers.Repacker.NBT
{
    using Closers.Repacker.IO;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class NBTCompound : NBTValue
    {
        private readonly Dictionary<string, NBTValue> _Map = new Dictionary<string, NBTValue>();

        public void Clear()
        {
            this.Map.Clear();
        }

        public bool ContainsTag(string name)
        {
            return this.Map.ContainsKey(name);
        }

        public bool GetBoolean(string name)
        {
            return this.GetBoolean(name, false);
        }

        public bool GetBoolean(string name, bool defaultValue)
        {
            NBTBoolean tag = this.GetTag(name) as NBTBoolean;
            return ((tag != null) ? tag.Value : defaultValue);
        }

        public byte GetByte(string name)
        {
            return this.GetByte(name, 0);
        }

        public byte GetByte(string name, byte defaultValue)
        {
            NBTNumber tag = this.GetTag(name) as NBTNumber;
            return ((tag != null) ? tag.GetAsByte() : defaultValue);
        }

        public NBTCompound GetCompound(string name)
        {
            NBTCompound tag = this.GetTag(name) as NBTCompound;
            return (tag ?? new NBTCompound());
        }

        public double GetDouble(string name)
        {
            return this.GetDouble(name, 0.0);
        }

        public double GetDouble(string name, double defaultValue)
        {
            NBTNumber tag = this.GetTag(name) as NBTNumber;
            return ((tag != null) ? tag.GetAsDouble() : defaultValue);
        }

        public float GetFloat(string name)
        {
            return this.GetFloat(name, 0f);
        }

        public float GetFloat(string name, float defaultValue)
        {
            NBTNumber tag = this.GetTag(name) as NBTNumber;
            return ((tag != null) ? tag.GetAsFloat() : defaultValue);
        }

        public int GetInteger(string name)
        {
            return this.GetInteger(name, 0);
        }

        public int GetInteger(string name, int defaultValue)
        {
            NBTNumber tag = this.GetTag(name) as NBTNumber;
            return ((tag != null) ? tag.GetAsInteger() : defaultValue);
        }

        public long GetLong(string name)
        {
            return this.GetLong(name, 0L);
        }

        public long GetLong(string name, long defaultValue)
        {
            NBTNumber tag = this.GetTag(name) as NBTNumber;
            return ((tag != null) ? tag.GetAsLong() : defaultValue);
        }

        public sbyte GetSByte(string name)
        {
            return this.GetSByte(name, 0);
        }

        public sbyte GetSByte(string name, sbyte defaultValue)
        {
            NBTNumber tag = this.GetTag(name) as NBTNumber;
            return ((tag != null) ? tag.GetAsSByte() : defaultValue);
        }

        public short GetShort(string name)
        {
            return this.GetShort(name, 0);
        }

        public short GetShort(string name, short defaultValue)
        {
            NBTNumber tag = this.GetTag(name) as NBTNumber;
            return ((tag != null) ? tag.GetAsShort() : defaultValue);
        }

        public string GetString(string name)
        {
            return this.GetString(name, "");
        }

        public string GetString(string name, string defaultValue)
        {
            NBTString tag = this.GetTag(name) as NBTString;
            return ((tag != null) ? tag.Value : defaultValue);
        }

        public NBTValue GetTag(string name)
        {
            if (this.ContainsTag(name))
            {
                return this.Map[name];
            }
            return null;
        }

        public uint GetUInteger(string name)
        {
            return this.GetUInteger(name, 0);
        }

        public uint GetUInteger(string name, uint defaultValue)
        {
            NBTNumber tag = this.GetTag(name) as NBTNumber;
            return ((tag != null) ? tag.GetAsUInteger() : defaultValue);
        }

        public ulong GetULong(string name)
        {
            return this.GetULong(name, 0L);
        }

        public ulong GetULong(string name, ulong defaultValue)
        {
            NBTNumber tag = this.GetTag(name) as NBTNumber;
            return ((tag != null) ? tag.GetAsULong() : defaultValue);
        }

        public ushort GetUShort(string name)
        {
            return this.GetUShort(name, 0);
        }

        public ushort GetUShort(string name, ushort defaultValue)
        {
            NBTNumber tag = this.GetTag(name) as NBTNumber;
            return ((tag != null) ? tag.GetAsUShort() : defaultValue);
        }

        protected override void OnRead(BinaryReader reader)
        {
            int num3;
            this.Clear();
            int num = reader.ReadInt32();
            for (int i = 0; i < num; i = num3)
            {
                string name = IOUtil.ReadString(reader);
                NBTValue tag = NBTValue.Read(reader);
                this.SetTag(name, tag);
                num3 = i + 1;
            }
        }

        protected override void OnWrite(BinaryWriter writer)
        {
            writer.Write(this.Map.Count);
            foreach (KeyValuePair<string, NBTValue> pair in this.Map)
            {
                string key = pair.Key;
                NBTValue value2 = pair.Value;
                IOUtil.WriteString(writer, key);
                NBTValue.Write(writer, value2);
            }
        }

        public void SetBoolean(string name, bool value)
        {
            this.SetTag(name, new NBTBoolean(value));
        }

        public void SetByte(string name, byte value)
        {
            this.SetTag(name, new NBTByte(value));
        }

        public void SetCompound(string name, NBTCompound value)
        {
            this.SetTag(name, value);
        }

        public void SetDouble(string name, double value)
        {
            this.SetTag(name, new NBTDouble(value));
        }

        public void SetFloat(string name, float value)
        {
            this.SetTag(name, new NBTFloat(value));
        }

        public void SetInteger(string name, int value)
        {
            this.SetTag(name, new NBTInteger(value));
        }

        public void SetLong(string name, long value)
        {
            this.SetTag(name, new NBTLong(value));
        }

        public void SetSByte(string name, sbyte value)
        {
            this.SetTag(name, new NBTSByte(value));
        }

        public void SetShort(string name, short value)
        {
            this.SetTag(name, new NBTShort(value));
        }

        public void SetString(string name, string value)
        {
            this.SetTag(name, new NBTString(value));
        }

        public void SetTag(string name, NBTValue tag)
        {
            this.Map[name] = tag;
        }

        public void SetUInteger(string name, uint value)
        {
            this.SetTag(name, new NBTUInteger(value));
        }

        public void SetULong(string name, ulong value)
        {
            this.SetTag(name, new NBTULong(value));
        }

        public void SetUShort(string name, ushort value)
        {
            this.SetTag(name, new NBTUShort(value));
        }

        public Dictionary<string, NBTValue> Map
        {
            get
            {
                return this._Map;
            }
        }

        public override NBTType Type
        {
            get
            {
                return NBTType.Compound;
            }
        }
    }
}

