﻿namespace Closers.Repacker.Commands
{
    using Closers.Repacker;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class CommandClientPath : Command
    {
        public override void Execute(CommandSender sender, string alias, string[] args)
        {
            Configuration configuration = ClosersRepacker.Instance.Configuration;
            if (args.Length == 0)
            {
                string clientPath = configuration.ClientPath;
                if (clientPath == null)
                {
                    sender.SendMessage("클로저스 설치 경로가 지정되지 않았습니다.");
                }
                else
                {
                    sender.SendMessage("현재 지정된 클로저스 설치 경로는 \"" + clientPath + "\" 입니다.");
                }
                sender.SendMessage("\"" + this.Label + " [경로]\" 를 입력하면 클로저스 설치 경로를 지정합니다.");
                sender.SendMessage("[경로]에는 클로저스가 설치된 경로를 입력해야합니다.");
            }
            else
            {
                string path = Util.MergeToString<string>(args, null, ' '.ToString());
                if (!Directory.Exists(path))
                {
                    sender.SendMessage("클로저스 설치 경로에 접근할 수 없습니다.");
                    sender.SendMessage("입력한 클로저스 설치 경로는 \"" + path + "\" 입니다.");
                    sender.SendMessage("명령어를 통해서 클로저스 설치 경로를 다시 지정해주세요.");
                }
                else
                {
                    configuration.ClientPath = path;
                    sender.SendMessage("클로저스 설치 경로를 지정했습니다.");
                    sender.SendMessage("새로 지정된 클로저스 설치 경로는 \"" + path + "\" 입니다.");
                    configuration.Save();
                }
            }
        }

        public override string Description
        {
            get
            {
                return "클로저스가 설치된 폴더의 경로를 지정하거나 확인합니다. 일부 명령어사용에 필요합니다.";
            }
        }

        public override List<string> HelpMessage
        {
            get
            {
                List<string> list = new List<string>();
                list.Add(this.Label + " : 현재 지정된 클로저스 설치 경로를 확인합니다.");
                list.Add(this.Label + " [경로] : 클로저스가 설치 경로를 지정합니다.");
                return list;
            }
        }

        public override string Label
        {
            get
            {
                return "ClientPath";
            }
        }
    }
}

