﻿namespace Closers.Repacker.Commands
{
    using System;
    using System.Collections.Generic;

    public abstract class CommandSender
    {
        public abstract string ReceiveMessage();
        public abstract void SendMessage();
        public void SendMessage(IEnumerable<string> message)
        {
            foreach (string str in message)
            {
                this.SendMessage(str);
            }
        }

        public abstract void SendMessage(string message);
    }
}

