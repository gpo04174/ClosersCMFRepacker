﻿namespace Closers.Repacker.Commands
{
    using Closers.Repacker;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class CommandUnpack : Command
    {
        public override void Execute(CommandSender sender, string alias, string[] args)
        {
            int num5;
            ClosersRepacker instance = ClosersRepacker.Instance;
            DirectoryManager directoryManager = instance.DirectoryManager;
            string path = null;
            string originalUnpackDirectory = directoryManager.GetOriginalUnpackDirectory();
            if (args.Length == 0)
            {
                path = directoryManager.GetOriginalCMFDirectory();
            }
            else
            {
                path = Util.MergeToString<string>(args, null, ' '.ToString());
                if (!Directory.Exists(path))
                {
                    sender.SendMessage("해당 폴더에 접근할 수 없습니다.");
                    sender.SendMessage("입력값을 확인해주세요.");
                    return;
                }
            }
            path = Util.Suffix(path, @"\");
            sender.SendMessage("'" + path + "' 폴더의 모든 CMF파일을 찾아 분해합니다.");
            sender.SendMessage("CMF파일을 찾는중 입니다.");
            string[] strArray = Directory.GetFiles(path, "*.CMF", SearchOption.AllDirectories);
            string format = "D" + (((int) Math.Log10((double) strArray.Length)) + 1);
            sender.SendMessage("발견된 파일 개수 : " + strArray.Length);
            double num = ((double) strArray.Length) / 100.0;
            for (int i = 0; i < strArray.Length; i = num5)
            {
                string str4 = strArray[i];
                string fileName = str4.Replace(path, "");
                int num3 = i + 1;
                instance.CMFManager.Unpack(directoryManager.GetOriginalUnpackDirectory(fileName), str4);
                object[] objArray1 = new object[] { num3.ToString(format), "/", strArray.Length, " = ", (((double) num3) / num).ToString("F2"), "%" };
                sender.SendMessage(string.Concat(objArray1));
                num5 = i + 1;
            }
            GC.Collect();
            sender.SendMessage("분해가 완료되었습니다.");
        }

        public override string Description
        {
            get
            {
                return "특정 폴더의 모든 CMF파일을 찾아 분해합니다.";
            }
        }

        public override List<string> HelpMessage
        {
            get
            {
                List<string> list = new List<string>();
                ClosersRepacker instance = ClosersRepacker.Instance;
                DirectoryManager directoryManager = instance.DirectoryManager;
                string str = directoryManager.GetOriginalCMFDirectory().Replace(instance.StartPath, "");
                string str2 = directoryManager.GetOriginalUnpackDirectory().Replace(instance.StartPath, "");
                list.Add("현재 프로그램이 실행중인 경로에 위치한 '" + str + "' 폴더에서 모든 CMF파일을 찾아 분해합니다.");
                list.Add(this.Label + " [분해 폴더]를 입력하면 해당 폴더에서 CMF파일을 찾습니다.");
                list.Add("분해된 데이터는 '" + str2 + "' 폴더에 생성됩니다.");
                return list;
            }
        }

        public override string Label
        {
            get
            {
                return "Unpack";
            }
        }
    }
}

