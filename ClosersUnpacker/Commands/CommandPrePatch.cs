﻿namespace Closers.Repacker.Commands
{
    using Closers.Repacker;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;

    public class CommandPrePatch : Command
    {
        public override void Execute(CommandSender sender, string alias, string[] args)
        {
            Configuration configuration = ClosersRepacker.Instance.Configuration;
            if (configuration.ClientPath == null)
            {
                sender.SendMessage("클로저스 설치 경로가 지정되지 않았습니다.");
                sender.SendMessage("명령어 동작에는 클로저스 설치 경로 지정이 필요합니다.");
            }
            else
            {
                string clientPath = configuration.ClientPath;
                if (!Directory.Exists(clientPath))
                {
                    sender.SendMessage("클로저스 설치 폴더에 접근할 수 없습니다.");
                    sender.SendMessage("현재 지정된 클로저스 설치 경로는 \"" + clientPath + "\" 입니다.");
                    sender.SendMessage("명령어를 통해서 클로저스 설치 경로를 다시 지정해주세요.");
                }
                else
                {
                    sender.SendMessage("사전패치를 실행합니다.");
                    ProcessStartInfo info = new ProcessStartInfo();
                    info.WorkingDirectory = clientPath;
                    info.FileName = clientPath + "LAUNCHER.EXE";
                    info.Arguments = "_G 106497 _P _A _C http://closers.dn.nexoncdn.co.kr/CLOSERS_LIVE_NEXON/ _L 211.39.129.134";
                    Process process = new Process();
                    process.StartInfo = info;
                    process.Start();
                }
            }
        }

        public override string Description
        {
            get
            {
                return "클라이언트 사전패치를 실행합니다.";
            }
        }

        public override List<string> HelpMessage
        {
            get
            {
                List<string> list = new List<string>();
                list.Add("서버가 점검중인 경우 사전패치를 위한 런쳐를 실행합니다.");
                return list;
            }
        }

        public override string Label
        {
            get
            {
                return "PrePatch";
            }
        }
    }
}

