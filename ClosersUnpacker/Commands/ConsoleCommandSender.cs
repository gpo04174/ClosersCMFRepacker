﻿namespace Closers.Repacker.Commands
{
    using System;

    public class ConsoleCommandSender : CommandSender
    {
        public override string ReceiveMessage()
        {
            Console.Write("명령어 입력>");
            return Console.ReadLine();
        }

        public override void SendMessage()
        {
            Console.WriteLine();
        }

        public override void SendMessage(string message)
        {
            Console.WriteLine(" " + message);
        }
    }
}

