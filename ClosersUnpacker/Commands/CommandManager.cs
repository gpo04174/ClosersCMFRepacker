﻿namespace Closers.Repacker.Commands
{
    using System;
    using System.Collections.Generic;

    public class CommandManager
    {
        private readonly Command _Help = null;
        private readonly List<Command> _RegisterList = new List<Command>();
        public const char ArgSeparator = ' ';

        public CommandManager()
        {
            this.RegisterList.Add(this._Help = new CommandHelp());
            this.RegisterList.Add(new CommandClientPath());
            this.RegisterList.Add(new CommandPrePatch());
            this.RegisterList.Add(new CommandChanges());
            this.RegisterList.Add(new CommandUnpack());
        }

        public bool ExecuteCommandLine(CommandSender sender, string line)
        {
            char[] separator = new char[] { ' ' };
            string[] sourceArray = line.Split(separator);
            string alias = sourceArray[0];
            string[] destinationArray = new string[sourceArray.Length - 1];
            Array.Copy(sourceArray, 1, destinationArray, 0, destinationArray.Length);
            Command command = this.GetCommand(alias);
            if (command != null)
            {
                command.Execute(sender, alias, destinationArray);
                return true;
            }
            return false;
        }

        public Command GetCommand(string alias)
        {
            Predicate<string> tmp = null;
            return this.RegisterList.Find(c => c.GetCommandLabelAndAliases().Find(tmp ?? (tmp = s => s.Equals(alias, StringComparison.OrdinalIgnoreCase))) != null);
        }

        public Command Help
        {
            get
            {
                return this._Help;
            }
        }

        public List<Command> RegisterList
        {
            get
            {
                return this._RegisterList;
            }
        }
    }
}

