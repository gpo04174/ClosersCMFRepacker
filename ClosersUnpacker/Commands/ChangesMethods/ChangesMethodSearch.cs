﻿namespace Closers.Repacker.Commands.ChangesMethods
{
    using Closers.Repacker;
    using Closers.Repacker.CMF;
    using Closers.Repacker.Commands;
    using System;
    using System.IO;

    public class ChangesMethodSearch : ChangesMethod
    {
        protected override void Init(CommandSender sender)
        {
            sender.SendMessage("변경, 생성된 파일을 탐색합니다.");
        }

        protected override void Search(CMFFileInfoManager cmfFileInfoManager, CommandSender sender, string fileName, FileInfo fileInfo, DateTime recordTime)
        {
            if (cmfFileInfoManager.HasChanged(fileName, fileInfo))
            {
                sender.SendMessage(fileName + "파일이 변경되었습니다.");
                cmfFileInfoManager[fileName] = new CMFFileInfo(fileInfo, recordTime);
                string changesDirectory = ClosersRepacker.Instance.DirectoryManager.GetChangesDirectory(recordTime);
                ClosersRepacker.Instance.CMFManager.Unpack(changesDirectory, fileInfo.FullName);
            }
        }
    }
}

