﻿namespace Closers.Repacker.Commands.ChangesMethods
{
    using Closers.Repacker.CMF;
    using Closers.Repacker.Commands;
    using System;
    using System.IO;

    public class ChangesMethodUpdate : ChangesMethod
    {
        protected override void Init(CommandSender sender)
        {
            sender.SendMessage("DAT폴더를 탐색한 기록이 없습니다.");
            sender.SendMessage("변경, 생성된 파일을 찾기 위해서 파일 목록을 생성합니다.");
            sender.SendMessage("게임이 패치되거나 게임 파일이 변경되는 경우, 이 명령어를 다시 실행하시면 됩니다.");
        }

        protected override void Search(CMFFileInfoManager cmfFileInfoManager, CommandSender sender, string fileName, FileInfo fileInfo, DateTime recordTime)
        {
            cmfFileInfoManager[fileName] = new CMFFileInfo(fileInfo, recordTime);
        }
    }
}

