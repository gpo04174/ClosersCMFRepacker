﻿namespace Closers.Repacker.Commands.ChangesMethods
{
    using Closers.Repacker.CMF;
    using Closers.Repacker.Commands;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public abstract class ChangesMethod
    {
        protected abstract void Init(CommandSender sender);
        public void Run(CMFFileInfoManager cmfFileInfoManager, CommandSender sender, string directory, List<FileInfo> fileInfoList)
        {
            int num2;
            this.Init(sender);
            DateTime now = DateTime.Now;
            for (int i = 0; i < fileInfoList.Count; i = num2)
            {
                FileInfo fileInfo = fileInfoList[i];
                string fileName = fileInfo.FullName.Replace(directory, "");
                this.Search(cmfFileInfoManager, sender, fileName, fileInfo, now);
                num2 = i + 1;
            }
        }

        protected abstract void Search(CMFFileInfoManager cmfFileInfoManager, CommandSender sender, string fileName, FileInfo fileInfo, DateTime recordTime);
    }
}

