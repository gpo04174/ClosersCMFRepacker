﻿namespace Closers.Repacker.Commands
{
    using System;
    using System.Collections.Generic;

    public abstract class Command
    {
        public abstract void Execute(CommandSender sender, string alias, string[] args);
        public List<string> GetCommandLabelAndAliases()
        {
            List<string> list = new List<string>();
            list.Add(this.Label);
            list.AddRange(this.AliasList);
            return list;
        }

        public virtual List<string> AliasList
        {
            get
            {
                return new List<string>();
            }
        }

        public abstract string Description { get; }

        public abstract List<string> HelpMessage { get; }

        public abstract string Label { get; }
    }
}

