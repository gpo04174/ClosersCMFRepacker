﻿namespace Closers.Repacker.Commands
{
    using Closers.Repacker;
    using Closers.Repacker.CMF;
    using Closers.Repacker.Commands.ChangesMethods;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class CommandChanges : Command
    {
        private readonly ChangesMethodSearch MethodSearch = null;
        private readonly ChangesMethodUpdate MethodUpdate = null;

        public CommandChanges()
        {
            this.MethodUpdate = new ChangesMethodUpdate();
            this.MethodSearch = new ChangesMethodSearch();
        }

        public override void Execute(CommandSender sender, string alias, string[] args)
        {
            ClosersRepacker instance = ClosersRepacker.Instance;
            Configuration configuration = instance.Configuration;
            CMFFileInfoManager cMFFileInfoManager = instance.CMFFileInfoManager;
            string clientDATPath = configuration.ClientDATPath;
            if (clientDATPath == null)
            {
                sender.SendMessage("클로저스 설치 경로가 지정되지 않았습니다.");
                sender.SendMessage("명령어 동작에는 클로저스 설치 경로 지정이 필요합니다.");
            }
            else if (!Directory.Exists(clientDATPath))
            {
                sender.SendMessage("클로저스 설치 폴더에 접근할 수 없습니다.");
                sender.SendMessage("현재 지정된 클로저스 설치 경로는 \"" + configuration.ClientPath + "\" 입니다.");
                sender.SendMessage("명령어를 통해서 클로저스 설치 경로를 다시 지정해주세요.");
            }
            else
            {
                ChangesMethod methodUpdate = null;
                if (!cMFFileInfoManager.Searched)
                {
                    methodUpdate = this.MethodUpdate;
                }
                else
                {
                    methodUpdate = this.MethodSearch;
                }
                List<FileInfo> dATFiles = configuration.GetDATFiles();
                methodUpdate.Run(cMFFileInfoManager, sender, configuration.ClientDATPath, dATFiles);
                cMFFileInfoManager.Searched = true;
                cMFFileInfoManager.Save();
                sender.SendMessage("탐색을 마쳤습니다.");
            }
        }

        public override string Description
        {
            get
            {
                return "클로저스 설치 경로의 DAT폴더를 탐색하여 변경 및 생성된 파일을 자동으로 분해합니다.";
            }
        }

        public override List<string> HelpMessage
        {
            get
            {
                List<string> list = new List<string>();
                list.Add("최초 실행 시 클로저스 설치 경로의 DAT폴더를 탐색하여 파일 목록을 생성합니다.");
                list.Add("다음 실행 부터 DAT폴더를 탐색하여 변경, 생성된 파일을 분해합니다.");
                return list;
            }
        }

        public override string Label
        {
            get
            {
                return "Changes";
            }
        }
    }
}

