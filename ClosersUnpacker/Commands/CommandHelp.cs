﻿namespace Closers.Repacker.Commands
{
    using Closers.Repacker;
    using System;
    using System.Collections.Generic;

    public class CommandHelp : Command
    {
        public override void Execute(CommandSender sender, string alias, string[] args)
        {
            if (args.Length == 0)
            {
                int num3;
                int num = 0;
                List<Command> registerList = ClosersRepacker.Instance.CommandManager.RegisterList;
                foreach (Command command in registerList)
                {
                    num = Math.Max(num, command.Label.Length);
                }
                sender.SendMessage(this.Label + " [명령어 이름] 을 입력하면 해당 명령어에 대한 도움말을 확인할 수 있습니다.");
                sender.SendMessage();
                for (int i = 0; i < registerList.Count; i = num3)
                {
                    Command command2 = registerList[i];
                    sender.SendMessage(command2.Label.PadRight(num) + " : " + command2.Description);
                    if (((i + 1) < registerList.Count) && (((i - 2) % 3) == 0))
                    {
                        sender.SendMessage();
                    }
                    num3 = i + 1;
                }
            }
            else
            {
                Command command3 = ClosersRepacker.Instance.CommandManager.GetCommand(args[0]);
                if (command3 == null)
                {
                    sender.SendMessage("도움말을 표시하기 위한 명령어를 찾을 수 없습니다.");
                }
                else
                {
                    List<string> helpMessage = command3.HelpMessage;
                    if (this.HelpMessage != null)
                    {
                        sender.SendMessage(command3.HelpMessage);
                    }
                    else
                    {
                        sender.SendMessage("해당 명령어는 별도의 도움말이 없습니다.");
                    }
                }
            }
        }

        public override List<string> AliasList
        {
            get
            {
                List<string> aliasList = base.AliasList;
                aliasList.Add("?");
                return aliasList;
            }
        }

        public override string Description
        {
            get
            {
                return "명령어 목록을 확인합니다.";
            }
        }

        public override List<string> HelpMessage
        {
            get
            {
                List<string> list = new List<string>();
                list.Add(this.Label + " : " + this.Description);
                list.Add(this.Label + " [명령어 이름] : 해당 명령어에 대한 도움말을 봅니다.");
                return list;
            }
        }

        public override string Label
        {
            get
            {
                return "Help";
            }
        }
    }
}

