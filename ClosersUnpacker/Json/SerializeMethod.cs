﻿namespace Closers.Repacker.Json
{
    using Closers.Repacker.NBT;
    using Newtonsoft.Json.Linq;
    using System;

    public abstract class SerializeMethod
    {
        protected SerializeMethod()
        {
        }

        public abstract NBTValue TryDeserialize(JToken token);
        public abstract JToken TrySerialize(NBTValue value);
    }
}

