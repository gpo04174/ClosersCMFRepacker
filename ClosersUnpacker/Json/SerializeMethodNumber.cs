﻿namespace Closers.Repacker.Json
{
    using Closers.Repacker.NBT;
    using Newtonsoft.Json.Linq;

    public class SerializeMethodNumber : SerializeMethod
    {
        public override NBTValue TryDeserialize(JToken token)
        {
            if (token is JValue)
            {
                return NBTNumber.Create(((JValue) token).Value);
            }
            return null;
        }

        public override JToken TrySerialize(NBTValue value)
        {
            if (value is NBTNumber)
            {
                return new JValue(((NBTNumber) value).GetAsObject());
            }
            return null;
        }
    }
}

