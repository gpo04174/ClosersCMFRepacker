﻿namespace Closers.Repacker.Json
{
    using Closers.Repacker.NBT;
    using Newtonsoft.Json.Linq;
    using System;
    using System.IO;

    public static class JsonUtil
    {
        public static NBTCompound Read(string path)
        {
            if (File.Exists(path))
            {
                string json = File.ReadAllText(path);
                try
                {
                    return (NBTCompound) Json2NBTSerializer.Deserialize(JObject.Parse(json));
                }
                catch
                {
                }
            }
            return new NBTCompound();
        }

        public static void Write(string path, NBTCompound compound)
        {
            string contents = Json2NBTSerializer.Serialize(compound).ToString();
            File.WriteAllText(path, contents);
        }
    }
}

