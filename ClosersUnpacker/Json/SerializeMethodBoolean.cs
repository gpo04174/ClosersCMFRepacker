﻿namespace Closers.Repacker.Json
{
    using Closers.Repacker.NBT;
    using Newtonsoft.Json.Linq;
    using System;

    public class SerializeMethodBoolean : SerializeMethod
    {
        public override NBTValue TryDeserialize(JToken token)
        {
            if (token is JValue)
            {
                object obj2 = ((JValue) token).Value;
                if (obj2 is bool)
                {
                    return new NBTBoolean((bool) obj2);
                }
            }
            return null;
        }

        public override JToken TrySerialize(NBTValue value)
        {
            if (value is NBTBoolean)
            {
                return new JValue(((NBTBoolean) value).Value);
            }
            return null;
        }
    }
}

