﻿namespace Closers.Repacker.Json
{
    using Closers.Repacker.NBT;
    using Newtonsoft.Json.Linq;
    using System;

    public class SerializeMethodString : SerializeMethod
    {
        public override NBTValue TryDeserialize(JToken token)
        {
            if (token is JValue)
            {
                object obj2 = ((JValue) token).Value;
                if (obj2 is string)
                {
                    return new NBTString((string) obj2);
                }
            }
            return null;
        }

        public override JToken TrySerialize(NBTValue value)
        {
            if (value is NBTString)
            {
                return new JValue(((NBTString) value).Value);
            }
            return null;
        }
    }
}

