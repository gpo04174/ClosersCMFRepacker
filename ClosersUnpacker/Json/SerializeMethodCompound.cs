﻿namespace Closers.Repacker.Json
{
    using Closers.Repacker.NBT;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;

    public class SerializeMethodCompound : SerializeMethod
    {
        public override NBTValue TryDeserialize(JToken token)
        {
            if (token is JObject)
            {
                JObject obj2 = (JObject) token;
                NBTCompound compound = new NBTCompound();
                foreach (JProperty property in obj2.Properties())
                {
                    compound.SetTag(property.Name, Json2NBTSerializer.Deserialize(property.Value));
                }
                return compound;
            }
            return null;
        }

        public override JToken TrySerialize(NBTValue value)
        {
            if (value is NBTCompound)
            {
                JObject obj2 = new JObject();
                NBTCompound compound = (NBTCompound) value;
                foreach (KeyValuePair<string, NBTValue> pair in compound.Map)
                {
                    obj2.Add(pair.Key, Json2NBTSerializer.Serialize(pair.Value));
                }
                return obj2;
            }
            return null;
        }
    }
}

