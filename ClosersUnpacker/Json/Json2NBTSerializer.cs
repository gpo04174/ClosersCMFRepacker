﻿namespace Closers.Repacker.Json
{
    using Closers.Repacker.NBT;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class Json2NBTSerializer
    {
        private static readonly List<SerializeMethod> MethodList = new List<SerializeMethod>();

        static Json2NBTSerializer()
        {
            MethodList.Add(new SerializeMethodBoolean());
            MethodList.Add(new SerializeMethodNumber());
            MethodList.Add(new SerializeMethodString());
            MethodList.Add(new SerializeMethodCompound());
        }

        public static NBTValue Deserialize(JToken token)
        {
            NBTValue value = null;
            MethodList.Any<SerializeMethod>(sm => (value = sm.TryDeserialize(token)) != null);
            return null;
        }

        public static JToken Serialize(NBTValue value)
        {
            JToken token = null;
            MethodList.Any<SerializeMethod>(sm => (token = sm.TrySerialize(value)) != null);
            return null;
        }
    }
}

