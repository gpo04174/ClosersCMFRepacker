﻿namespace Closers.Repacker
{
    using System;
    using System.Text;

    public static class Util
    {
        public static string MergeToString<E>(E[] array, Func<E, string> toStringer, string merger)
        {
            int num2;
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < array.Length; i = num2)
            {
                E local = array[i];
                if (toStringer != null)
                {
                    builder.Append(toStringer.Invoke(local));
                }
                else
                {
                    builder.Append(local);
                }
                if ((i + 1) < array.Length)
                {
                    builder.Append(merger);
                }
                num2 = i + 1;
            }
            return builder.ToString();
        }

        public static string Suffix(string text, string suffix)
        {
            if (!text.EndsWith(suffix))
            {
                text = text + suffix;
            }
            return text;
        }

        public static string ToString(params object[] args)
        {
            return ToString("=", ", ", args);
        }

        public static string ToString(string mergerNameValue, string mergerItem, params object[] args)
        {
            StringBuilder builder = new StringBuilder();
            int num = args.Length / 2;
            for (int i = 0; i < num; i += 2)
            {
                object obj2 = args[i + 0];
                object obj3 = args[i + 1];
                builder.Append(obj2);
                builder.Append(mergerNameValue);
                builder.Append(obj3);
                if ((i + 1) < num)
                {
                    builder.Append(mergerItem);
                }
            }
            return builder.ToString();
        }
    }
}

