﻿namespace Closers.Repacker
{
    using Closers.Repacker.Json;
    using Closers.Repacker.NBT;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class Configuration
    {
        private string _ClientPath = null;

        public string[] GetDATDirectories()
        {
            return Directory.GetDirectories(this.ClientDATPath, "DAT*", SearchOption.TopDirectoryOnly);
        }

        public List<FileInfo> GetDATFiles()
        {
            int num3;
            List<FileInfo> list = new List<FileInfo>();
            string[] dATDirectories = this.GetDATDirectories();
            for (int i = 0; i < dATDirectories.Length; i = num3)
            {
                string[] strArray2 = Directory.GetFiles(dATDirectories[i], "*.CMF", SearchOption.AllDirectories);
                for (int j = 0; j < strArray2.Length; j = num3)
                {
                    list.Add(new FileInfo(strArray2[j]));
                    num3 = j + 1;
                }
                num3 = i + 1;
            }
            return list;
        }

        private string GetFilePath()
        {
            return (ClosersRepacker.Instance.StartPath + "Config.json");
        }

        public void Load()
        {
            this.ClientPath = JsonUtil.Read(this.GetFilePath()).GetString("ClientPath", null);
        }

        public void Save()
        {
            NBTCompound compound = new NBTCompound();
            compound.SetString("ClientPath", this.ClientPath);
            JsonUtil.Write(this.GetFilePath(), compound);
        }

        public string ClientDATPath
        {
            get
            {
                if (this.ClientPath == null)
                {
                    return null;
                }
                return (this.ClientPath + "DAT");
            }
        }

        public string ClientPath
        {
            get
            {
                if (this._ClientPath == null)
                {
                    return null;
                }
                return Util.Suffix(this._ClientPath.Replace("/", @"\"), @"\");
            }
            set
            {
                this._ClientPath = value;
            }
        }
    }
}

