﻿namespace Closers.Repacker.CMF
{
    using Closers.Repacker;
    using Closers.Repacker.IO;
    using Closers.Repacker.NBT;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    public class CMFFileInfoManager
    {
        private bool _Searched = false;
        private readonly Dictionary<string, CMFFileInfo> Map = new Dictionary<string, CMFFileInfo>();

        private string GetFilePath()
        {
            return (ClosersRepacker.Instance.StartPath + "FileChangeInfo.dat");
        }

        public bool HasChanged(string name, FileInfo fileInfo)
        {
            CMFFileInfo info = this[name];
            return ((info == null) || info.HasChanged(fileInfo));
        }

        public void Load()
        {
            Stream stream = null;
            NBTCompound compound = null;
            int num3;
            try
            {
                stream = new FileStream(this.GetFilePath(), FileMode.OpenOrCreate);
                compound = (NBTCompound) NBTValue.ReadCompressionBinary(stream);
            }
            catch
            {
            }
            IOUtil.CloseAndDisposeQuitly(stream);
            if (compound == null)
            {
                compound = new NBTCompound();
            }
            this.Map.Clear();
            int integer = compound.GetInteger("Map.Count");
            for (int i = 0; i < integer; i = num3)
            {
                string str = "Map." + i;
                string str2 = compound.GetString(str + ".Key");
                CMFFileInfo info = CMFFileInfo.Read(compound.GetCompound(str + ".Value"));
                this.Map[str2] = info;
                num3 = i + 1;
            }
            this.Searched = compound.GetBoolean("Searched");
        }

        public void Save()
        {
            int num2;
            NBTCompound compound = new NBTCompound();
            List<KeyValuePair<string, CMFFileInfo>> list = Enumerable.ToList<KeyValuePair<string, CMFFileInfo>>(this.Map);
            compound.SetInteger("Map.Count", list.Count);
            for (int i = 0; i < list.Count; i = num2)
            {
                string str = "Map." + i;
                KeyValuePair<string, CMFFileInfo> pair = list[i];
                compound.SetString(str + ".Key", pair.Key);
                compound.SetCompound(str + ".Value", CMFFileInfo.Write(pair.Value));
                num2 = i + 1;
            }
            compound.SetBoolean("Searched", this.Searched);
            Stream stream = null;
            try
            {
                stream = new FileStream(this.GetFilePath(), FileMode.Create);
                NBTValue.WriteCompressionBinary(stream, compound);
            }
            catch
            {
            }
            IOUtil.CloseAndDisposeQuitly(stream);
        }

        public CMFFileInfo this[string name]
        {
            get
            {
                CMFFileInfo info = null;
                this.Map.TryGetValue(name, out info);
                return info;
            }
            set
            {
                this.Map[name] = value;
            }
        }

        public Dictionary<string, CMFFileInfo>.KeyCollection Keys
        {
            get
            {
                return this.Map.Keys;
            }
        }

        public List<KeyValuePair<string, CMFFileInfo>> PairList
        {
            get
            {
                return Enumerable.ToList<KeyValuePair<string, CMFFileInfo>>(this.Map);
            }
        }

        public bool Searched
        {
            get
            {
                return this._Searched;
            }
            set
            {
                this._Searched = value;
            }
        }
    }
}

