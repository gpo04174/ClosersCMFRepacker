﻿namespace Closers.Repacker.CMF
{
    using Closers.Repacker.IO;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class CMFFile
    {
        private readonly List<CMFData> _DataList = new List<CMFData>();
        private readonly bool _OnlyHeader = false;
        private FileVersion _Version = null;

        public CMFFile(bool onlyHeader)
        {
            this.Version = new FileVersion();
        }

        private static CMFFile Read(BinaryReader reader, bool onlyHeader)
        {
            CMFFile file = new CMFFile(onlyHeader);
            file.ReadFromCMF(reader);
            return file;
        }

        public void ReadFromCMF(BinaryReader reader)
        {
            int num4;
            this.Version.Read(reader);
            int num = new CMFEncryptReader(reader).ReadInteger();
            this.DataList.Clear();
            for (int i = 0; i < num; i = num4)
            {
                CMFData item = new CMFData();
                item.ReadHeaderFromCMF(new CMFEncryptReader(reader));
                this.DataList.Add(item);
                num4 = i + 1;
            }
            int num2 = (int) reader.BaseStream.Position;
            if (this.OnlyHeader)
            {
                int num5 = reader.ReadInt32();
                int num6 = reader.ReadInt32();
            }
            else
            {
                for (int j = 0; j < num; j = num4)
                {
                    int position = ((int) reader.BaseStream.Position) - num2;
                    this.DataList.Find(delegate (CMFData d) {
                        return d.Offset == position;
                    }).ReadDataFrmCMF(reader);
                    num4 = j + 1;
                }
            }
        }

        public static List<CMFFile> ReadFromFile(string path)
        {
            Exception exception = null;
            List<CMFFile> list = new List<CMFFile>();
            string fileName = Path.GetFileName(path);
            FileStream input = null;
            BinaryReader reader = null;
            try
            {
                input = new FileStream(path, FileMode.Open);
                reader = new BinaryReader(input);
                if (!fileName.Equals("HEADER.CMF", StringComparison.OrdinalIgnoreCase) && !fileName.Equals("SCRIPT_PACK.CMF", StringComparison.OrdinalIgnoreCase))
                {
                    list.Add(Read(reader, false));
                }
            }
            catch (Exception exception2)
            {
                exception = new Exception("", exception2);
            }
            IOUtil.CloseAndDisposeQuitly(reader);
            IOUtil.CloseAndDisposeQuitly(input);
            if (exception != null)
            {
                throw exception;
            }
            return list;
        }

        public void WriteToCMF(BinaryWriter writer)
        {
            this.Version.Write(writer);
        }

        public List<CMFData> DataList
        {
            get
            {
                return this._DataList;
            }
        }

        public bool OnlyHeader
        {
            get
            {
                return this._OnlyHeader;
            }
        }

        public FileVersion Version
        {
            get
            {
                return this._Version;
            }
            set
            {
                this._Version = value;
            }
        }
    }
}

