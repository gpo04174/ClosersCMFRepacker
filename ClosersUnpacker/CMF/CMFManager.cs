﻿namespace Closers.Repacker.CMF
{
    using Closers.Repacker.IO;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class CMFManager
    {
        public void Unpack(string destination, string path)
        {
            int num2;
            List<CMFFile> list = CMFFile.ReadFromFile(path);
            for (int i = 0; i < list.Count; i = num2)
            {
                CMFFile file = list[i];
                foreach (CMFData data in file.DataList)
                {
                    FileStream stream = null;
                    try
                    {
                        stream = new FileStream(destination + data.Name, FileMode.Create);
                    }
                    catch
                    {
                        stream = new FileStream(destination + "WrongName" + data.Offset, FileMode.Create);
                    }
                    try
                    {
                        stream.Write(data.Data, 0, data.Data.Length);
                    }
                    catch
                    {
                    }
                    IOUtil.CloseAndDisposeQuitly(stream);
                }
                num2 = i + 1;
            }
        }
    }
}

