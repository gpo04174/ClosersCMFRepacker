﻿namespace Closers.Repacker.CMF
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;

    public static class CMFString
    {
        public static string ReadString(byte[] bytes)
        {
            return //Encoding.Default.GetString(Enumerable.ToArray<byte>(Enumerable.TakeWhile<byte>(Enumerable.Where<byte>(bytes, CS.CS_9__0_0 ?? (CS.CS_9__0_0 = new Func<byte, int, bool>(CS.CSS, (IntPtr)this.ReadString_b__0_0))), CS.CS_9__0_1 ?? (CS.CS_9__0_1 = new Func<byte, bool>(CS.CSS, (IntPtr)CS.ReadString_b__0_1)))));
                Encoding.Default.GetString(bytes.Where<byte>(((Func<byte, int, bool>)((value, index) => ((index % 2) == 0)))).TakeWhile<byte>(((Func<byte, bool>)(value => (value > 0)))).ToArray<byte>());
        }

        public static string ReadString(BinaryReader reader, int size)
        {
            return ReadString(reader.ReadBytes(size));
        }

        public static void WriteString(BinaryWriter writer, string value, int size)
        {
            int num2;
            byte[] buffer = new byte[size];
            char[] chArray = value.ToCharArray();
            for (int i = 0; i < chArray.Length; i = num2)
            {
                buffer[i * 2] = (byte) chArray[i];
                num2 = i + 1;
            }
            writer.Write(buffer);
        }

        [Serializable, CompilerGenerated]
        private sealed class CS
        {
            public static readonly CMFString.CS CSS = new CMFString.CS();
            public static Func<byte, int, bool> CS_9__0_0;
            public static Func<byte, bool> CS_9__0_1;

            internal bool ReadString_b__0_0(byte value, int index)
            {
                return ((index % 2) == 0);
            }

            internal bool ReadString_b__0_1(byte value)
            {
                return (value > 0);
            }
        }
    }
}

