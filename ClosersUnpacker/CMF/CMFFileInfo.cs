﻿namespace Closers.Repacker.CMF
{
    using Closers.Repacker;
    using Closers.Repacker.NBT;
    using System;
    using System.IO;

    public class CMFFileInfo
    {
        private DateTime _LastWriteTime;
        private long _Legnth;
        private DateTime _RecordTime;

        public CMFFileInfo()
        {
            this._Legnth = 0L;
        }

        public CMFFileInfo(FileInfo fileInfo, DateTime recordTime)
        {
            this._Legnth = 0L;
            this.LastWriteTime = fileInfo.LastWriteTime;
            this.RecordTime = recordTime;
            this.Length = fileInfo.Length;
        }

        public bool HasChanged(FileInfo info)
        {
            return ((this.LastWriteTime != info.LastWriteTime) || (this.Length != info.Length));
        }

        public static CMFFileInfo Read(NBTCompound compound)
        {
            CMFFileInfo info = new CMFFileInfo();
            info.RecordTime = new DateTime(compound.GetLong("RecordTime"));
            info.LastWriteTime = new DateTime(compound.GetLong("LastWriteTime"));
            info.Length = compound.GetLong("Length");
            return info;
        }

        public override string ToString()
        {
            object[] args = new object[] { "RecordTime", this.RecordTime, "LastWriteTime", this.LastWriteTime, "Length", this.Length };
            return Util.ToString(args);
        }

        public static NBTCompound Write(CMFFileInfo info)
        {
            NBTCompound compound = new NBTCompound();
            compound.SetLong("RecordTime", info.RecordTime.Ticks);
            compound.SetLong("LastWriteTime", info.LastWriteTime.Ticks);
            compound.SetLong("Length", info.Length);
            return compound;
        }

        public DateTime LastWriteTime
        {
            get
            {
                return this._LastWriteTime;
            }
            set
            {
                this._LastWriteTime = value;
            }
        }

        public long Length
        {
            get
            {
                return this._Legnth;
            }
            set
            {
                this._Legnth = value;
            }
        }

        public DateTime RecordTime
        {
            get
            {
                return this._RecordTime;
            }
            set
            {
                this._RecordTime = value;
            }
        }
    }
}

