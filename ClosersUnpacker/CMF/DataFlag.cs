﻿namespace Closers.Repacker.CMF
{
    using System;

    public enum DataFlag
    {
        Normal,
        Compression,
        Unknown
    }
}

