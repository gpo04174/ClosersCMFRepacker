﻿namespace Closers.Repacker.CMF
{
    using System;
    using System.IO;

    public class CMFEncryptReader
    {
        private int KeyIndex = 0;
        private readonly BinaryReader Reader = null;

        public CMFEncryptReader(BinaryReader reader)
        {
            this.Reader = reader;
        }

        private byte[] DecodeBytes(int read)
        {
            byte[] bytes = BitConverter.GetBytes((int) (read ^ this.GetKey()));
            byte num = bytes[0];
            bytes[0] = bytes[3];
            bytes[3] = num;
            return bytes;
        }

        private int DecodeInteger(int read)
        {
            return BitConverter.ToInt32(this.DecodeBytes(read), 0);
        }

        private int GetKey()
        {
            int[] entryKeys = CMFEncryptor.EntryKeys;
            int index = this.KeyIndex % entryKeys.Length;
            this.KeyIndex = (index + 1) % entryKeys.Length;
            return entryKeys[index];
        }

        public byte[] ReadBytes(int byteCount)
        {
            int num4;
            byte[] destinationArray = new byte[byteCount];
            int num = byteCount / 4;
            for (int i = 0; i < num; i = num4)
            {
                int read = this.Reader.ReadInt32();
                byte[] sourceArray = this.DecodeBytes(read);
                Array.Copy(sourceArray, 0, destinationArray, i * 4, sourceArray.Length);
                num4 = i + 1;
            }
            return destinationArray;
        }

        public int ReadInteger()
        {
            return this.DecodeInteger(this.Reader.ReadInt32());
        }

        public string ReadString(int byteCount)
        {
            return CMFString.ReadString(this.ReadBytes(byteCount));
        }
    }
}

