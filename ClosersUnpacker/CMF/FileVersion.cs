﻿namespace Closers.Repacker.CMF
{
    using System;
    using System.IO;

    public class FileVersion
    {
        private string _Value = "";
        public const int Size = 100;

        public void Read(BinaryReader reader)
        {
            this.Value = CMFString.ReadString(reader, 100);
        }

        public void Write(BinaryWriter writer)
        {
            CMFString.WriteString(writer, this.Value, 100);
        }

        public string Value
        {
            get
            {
                return this._Value;
            }
            set
            {
                this._Value = value;
            }
        }
    }
}

