﻿namespace Closers.Repacker.CMF
{
    using Closers.Repacker.IO;
    using Ionic.Zlib;
    using System;
    using System.IO;

    public class CMFData
    {
        private int _CompressionSize = 0;
        private byte[] _Data = null;
        private DataFlag _Flags = DataFlag.Normal;
        private string _Name = "";
        private int _Offset = 0;
        private int _OriginalSize = 0;

        public void ReadDataFrmCMF(BinaryReader reader)
        {
            byte[] buffer = reader.ReadBytes(this.ReadSize);
            bool flag = this.Flags == DataFlag.Compression;
            int num = this.Name.LastIndexOf('.');
            string str = this.Name.Substring(num + 1);
            if (((str.Equals("FX", StringComparison.OrdinalIgnoreCase) || str.Equals("LUA", StringComparison.OrdinalIgnoreCase)) || str.Equals("TET", StringComparison.OrdinalIgnoreCase)) || str.Equals("XET", StringComparison.OrdinalIgnoreCase))
            {
                flag = false;
            }
            if (flag)
            {
                MemoryStream stream = new MemoryStream(buffer);
                ZlibStream stream2 = null;
                MemoryStream stream3 = null;
                try
                {
                    stream2 = new ZlibStream(stream, CompressionMode.Decompress);
                    stream3 = new MemoryStream();
                    stream2.CopyTo(stream3);
                    this.Data = stream3.ToArray();
                }
                catch
                {
                    this.Data = buffer;
                }
                IOUtil.CloseAndDisposeQuitly(stream2);
                IOUtil.CloseAndDisposeQuitly(stream);
                IOUtil.CloseAndDisposeQuitly(stream3);
            }
            else
            {
                this.Data = buffer;
            }
        }

        public void ReadHeaderFromCMF(CMFEncryptReader reader)
        {
            this.Name = reader.ReadString(0x200);
            this.OriginalSize = reader.ReadInteger();
            this.CompressionSize = reader.ReadInteger();
            this.Offset = reader.ReadInteger();
            this.Flags = (DataFlag) reader.ReadInteger();
        }

        public int CompressionSize
        {
            get
            {
                return this._CompressionSize;
            }
            set
            {
                this._CompressionSize = value;
            }
        }

        public byte[] Data
        {
            get
            {
                return this._Data;
            }
            set
            {
                this._Data = value;
            }
        }

        public DataFlag Flags
        {
            get
            {
                return this._Flags;
            }
            set
            {
                this._Flags = value;
            }
        }

        public string Name
        {
            get
            {
                return this._Name;
            }
            set
            {
                this._Name = value;
            }
        }

        public int Offset
        {
            get
            {
                return this._Offset;
            }
            set
            {
                this._Offset = value;
            }
        }

        public int OriginalSize
        {
            get
            {
                return this._OriginalSize;
            }
            set
            {
                this._OriginalSize = value;
            }
        }

        public int ReadSize
        {
            get
            {
                return ((this.Flags == DataFlag.Compression) ? this.CompressionSize : this.OriginalSize);
            }
        }
    }
}

