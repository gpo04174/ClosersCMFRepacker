﻿namespace Closers.Repacker.CMF
{
    using System;

    public static class CMFEncryptor
    {
        private static readonly int[] _EntryKeys = new int[] { -560762196, 0x169af84, -1892064548 };
        public const int EntryKey1 = -560762196;
        public const int EntryKey2 = 0x169af84;
        public const int EntryKey3 = -1892064548;

        public static byte[] Decode(int read, int key)
        {
            byte[] bytes = BitConverter.GetBytes((int) (read ^ key));
            byte num = bytes[0];
            bytes[0] = bytes[3];
            bytes[3] = num;
            return bytes;
        }

        public static int[] EntryKeys
        {
            get
            {
                return _EntryKeys;
            }
        }
    }
}

