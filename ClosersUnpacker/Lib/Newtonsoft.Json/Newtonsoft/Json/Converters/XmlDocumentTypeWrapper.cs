﻿namespace Newtonsoft.Json.Converters
{
    using System;
    using System.Xml;

    internal class XmlDocumentTypeWrapper : XmlNodeWrapper, IXmlDocumentType, IXmlNode
    {
        private readonly XmlDocumentType _documentType;

        public XmlDocumentTypeWrapper(XmlDocumentType documentType) : base(documentType)
        {
            this._documentType = documentType;
        }

        public string InternalSubset
        {
            get
            {
                return this._documentType.InternalSubset;
            }
        }

        public override string LocalName
        {
            get
            {
                return "DOCTYPE";
            }
        }

        public string Name
        {
            get
            {
                return this._documentType.Name;
            }
        }

        public string Public
        {
            get
            {
                return this._documentType.PublicId;
            }
        }

        public string System
        {
            get
            {
                return this._documentType.SystemId;
            }
        }
    }
}

