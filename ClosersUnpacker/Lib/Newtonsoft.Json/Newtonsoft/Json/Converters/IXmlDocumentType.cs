﻿namespace Newtonsoft.Json.Converters
{
    using System;

    internal interface IXmlDocumentType : IXmlNode
    {
        string InternalSubset { get; }

        string Name { get; }

        string Public { get; }

        string System { get; }
    }
}

