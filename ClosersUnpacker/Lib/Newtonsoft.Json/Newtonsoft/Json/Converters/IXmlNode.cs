﻿namespace Newtonsoft.Json.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Xml;

    internal interface IXmlNode
    {
        IXmlNode AppendChild(IXmlNode newChild);

        List<IXmlNode> Attributes { get; }

        List<IXmlNode> ChildNodes { get; }

        string LocalName { get; }

        string NamespaceUri { get; }

        XmlNodeType NodeType { get; }

        IXmlNode ParentNode { get; }

        string Value { get; set; }

        object WrappedNode { get; }
    }
}

