﻿namespace Newtonsoft.Json.Converters
{
    using System;
    using System.Xml.Linq;

    internal class XProcessingInstructionWrapper : XObjectWrapper
    {
        public XProcessingInstructionWrapper(XProcessingInstruction processingInstruction) : base(processingInstruction)
        {
        }

        public override string LocalName
        {
            get
            {
                return this.ProcessingInstruction.Target;
            }
        }

        private XProcessingInstruction ProcessingInstruction
        {
            get
            {
                return (XProcessingInstruction) base.WrappedNode;
            }
        }

        public override string Value
        {
            get
            {
                return this.ProcessingInstruction.Data;
            }
            set
            {
                this.ProcessingInstruction.Data = value;
            }
        }
    }
}

