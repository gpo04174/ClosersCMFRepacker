﻿namespace Newtonsoft.Json.Linq.JsonPath
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Runtime.CompilerServices;
    using System.Threading;

    internal class FieldFilter : PathFilter
    {
        public override IEnumerable<JToken> ExecuteFilter(IEnumerable<JToken> current, bool errorWhenNoMatch)
        {
            using (this.<>7__wrap1 = current.GetEnumerator())
            {
                while (this.<>7__wrap1.MoveNext())
                {
                    this.<t>5__2 = this.<>7__wrap1.Current;
                    this.<o>5__1 = this.<t>5__2 as JObject;
                    if (this.<o>5__1 != null)
                    {
                        if (this.Name != null)
                        {
                            JToken token = this.<o>5__1[this.Name];
                            if (token != null)
                            {
                                yield return token;
                            }
                            else if (errorWhenNoMatch)
                            {
                                throw new JsonException("Property '{0}' does not exist on JObject.".FormatWith(CultureInfo.InvariantCulture, this.Name));
                            }
                        }
                        else
                        {
                            using (this.<>7__wrap2 = this.<o>5__1.GetEnumerator())
                            {
                                while (this.<>7__wrap2.MoveNext())
                                {
                                    KeyValuePair<string, JToken> current = this.<>7__wrap2.Current;
                                    yield return current.Value;
                                }
                            }
                            this.<>7__wrap2 = null;
                        }
                    }
                    else if (errorWhenNoMatch)
                    {
                        throw new JsonException("Property '{0}' not valid on {1}.".FormatWith(CultureInfo.InvariantCulture, this.Name ?? "*", this.<t>5__2.GetType().Name));
                    }
                    this.<o>5__1 = null;
                    this.<t>5__2 = null;
                }
            }
            this.<>7__wrap1 = null;
        }

        public string Name { get; set; }

    }
}

