﻿namespace Newtonsoft.Json.Linq.JsonPath
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Runtime.CompilerServices;
    using System.Threading;

    internal class ArrayIndexFilter : PathFilter
    {
        public override IEnumerable<JToken> ExecuteFilter(IEnumerable<JToken> current, bool errorWhenNoMatch)
        {
            using (this.<>7__wrap1 = current.GetEnumerator())
            {
                while (this.<>7__wrap1.MoveNext())
                {
                    this.<t>5__1 = this.<>7__wrap1.Current;
                    if (this.Index.HasValue)
                    {
                        JToken token = PathFilter.GetTokenIndex(this.<t>5__1, errorWhenNoMatch, this.Index.GetValueOrDefault());
                        if (token != null)
                        {
                            yield return token;
                        }
                    }
                    else if ((this.<t>5__1 is JArray) || (this.<t>5__1 is JConstructor))
                    {
                        using (this.<>7__wrap2 = ((IEnumerable<JToken>) this.<t>5__1).GetEnumerator())
                        {
                            while (this.<>7__wrap2.MoveNext())
                            {
                                JToken current = this.<>7__wrap2.Current;
                                yield return current;
                            }
                        }
                        this.<>7__wrap2 = null;
                    }
                    else if (errorWhenNoMatch)
                    {
                        throw new JsonException("Index * not valid on {0}.".FormatWith(CultureInfo.InvariantCulture, this.<t>5__1.GetType().Name));
                    }
                    this.<t>5__1 = null;
                }
            }
            this.<>7__wrap1 = null;
        }

        public int? Index { get; set; }

    }
}

