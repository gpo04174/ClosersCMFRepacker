﻿namespace Newtonsoft.Json.Linq.JsonPath
{
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Threading;

    internal class ScanFilter : PathFilter
    {
        public override IEnumerable<JToken> ExecuteFilter(IEnumerable<JToken> current, bool errorWhenNoMatch)
        {
            using (this.<>7__wrap1 = current.GetEnumerator())
            {
                while (this.<>7__wrap1.MoveNext())
                {
                    JProperty property;
                    this.<root>5__1 = this.<>7__wrap1.Current;
                    if (this.Name == null)
                    {
                        yield return this.<root>5__1;
                    }
                    this.<value>5__2 = this.<root>5__1;
                    JToken token = this.<root>5__1;
                Label_009C:
                    if ((token == null) || !token.HasValues)
                    {
                        goto Label_00C6;
                    }
                    this.<value>5__2 = token.First;
                    goto Label_0121;
                Label_00B5:
                    this.<value>5__2 = this.<value>5__2.Parent;
                Label_00C6:
                    if (((this.<value>5__2 != null) && (this.<value>5__2 != this.<root>5__1)) && (this.<value>5__2 == this.<value>5__2.Parent.Last))
                    {
                        goto Label_00B5;
                    }
                    if ((this.<value>5__2 == null) || (this.<value>5__2 == this.<root>5__1))
                    {
                        goto Label_01A6;
                    }
                    this.<value>5__2 = this.<value>5__2.Next;
                Label_0121:
                    property = this.<value>5__2 as JProperty;
                    if (property != null)
                    {
                        if (property.Name == this.Name)
                        {
                            yield return property.Value;
                        }
                    }
                    else if (this.Name == null)
                    {
                        yield return this.<value>5__2;
                    }
                    token = this.<value>5__2 as JContainer;
                    goto Label_009C;
                Label_01A6:
                    this.<value>5__2 = null;
                    this.<root>5__1 = null;
                }
            }
            this.<>7__wrap1 = null;
        }

        public string Name { get; set; }

    }
}

