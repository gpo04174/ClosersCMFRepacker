﻿namespace Newtonsoft.Json.Serialization
{
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    public class JsonDictionaryContract : JsonContainerContract
    {
        private readonly Type _genericCollectionDefinitionType;
        private Func<object> _genericTemporaryDictionaryCreator;
        private ObjectConstructor<object> _genericWrapperCreator;
        private Type _genericWrapperType;
        private ObjectConstructor<object> _overrideCreator;
        private readonly ConstructorInfo _parameterizedConstructor;
        private ObjectConstructor<object> _parameterizedCreator;

        public JsonDictionaryContract(Type underlyingType) : base(underlyingType)
        {
            Type type;
            Type type2;
            base.ContractType = JsonContractType.Dictionary;
            if (ReflectionUtils.ImplementsGenericDefinition(underlyingType, typeof(IDictionary<,>), out this._genericCollectionDefinitionType))
            {
                type = this._genericCollectionDefinitionType.GetGenericArguments()[0];
                type2 = this._genericCollectionDefinitionType.GetGenericArguments()[1];
                if (ReflectionUtils.IsGenericDefinition(base.UnderlyingType, typeof(IDictionary<,>)))
                {
                    Type[] typeArguments = new Type[] { type, type2 };
                    base.CreatedType = typeof(Dictionary<,>).MakeGenericType(typeArguments);
                }
            }
            else
            {
                ReflectionUtils.GetDictionaryKeyValueTypes(base.UnderlyingType, out type, out type2);
                if (base.UnderlyingType == typeof(IDictionary))
                {
                    base.CreatedType = typeof(Dictionary<object, object>);
                }
            }
            if ((type != null) && (type2 != null))
            {
                Type[] typeArray2 = new Type[] { type, type2 };
                Type[] typeArray3 = new Type[] { type, type2 };
                this._parameterizedConstructor = CollectionUtils.ResolveEnumerableCollectionConstructor(base.CreatedType, typeof(KeyValuePair<,>).MakeGenericType(typeArray2), typeof(IDictionary<,>).MakeGenericType(typeArray3));
                if (!this.HasParameterizedCreatorInternal && (underlyingType.Name == "FSharpMap`2"))
                {
                    FSharpUtils.EnsureInitialized(underlyingType.Assembly());
                    this._parameterizedCreator = FSharpUtils.CreateMap(type, type2);
                }
            }
            this.ShouldCreateWrapper = !typeof(IDictionary).IsAssignableFrom(base.CreatedType);
            this.DictionaryKeyType = type;
            this.DictionaryValueType = type2;
        }

        internal IDictionary CreateTemporaryDictionary()
        {
            if (this._genericTemporaryDictionaryCreator == null)
            {
                Type[] typeArguments = new Type[] { this.DictionaryKeyType ?? typeof(object), this.DictionaryValueType ?? typeof(object) };
                Type type = typeof(Dictionary<,>).MakeGenericType(typeArguments);
                this._genericTemporaryDictionaryCreator = JsonTypeReflector.ReflectionDelegateFactory.CreateDefaultConstructor<object>(type);
            }
            return (IDictionary) this._genericTemporaryDictionaryCreator();
        }

        internal IWrappedDictionary CreateWrapper(object dictionary)
        {
            if (this._genericWrapperCreator == null)
            {
                Type[] typeArguments = new Type[] { this.DictionaryKeyType, this.DictionaryValueType };
                this._genericWrapperType = typeof(DictionaryWrapper<,>).MakeGenericType(typeArguments);
                Type[] types = new Type[] { this._genericCollectionDefinitionType };
                ConstructorInfo constructor = this._genericWrapperType.GetConstructor(types);
                this._genericWrapperCreator = JsonTypeReflector.ReflectionDelegateFactory.CreateParameterizedConstructor(constructor);
            }
            object[] args = new object[] { dictionary };
            return (IWrappedDictionary) this._genericWrapperCreator(args);
        }

        public Func<string, string> DictionaryKeyResolver { get; set; }

        public Type DictionaryKeyType { get; private set; }

        public Type DictionaryValueType { get; private set; }

        public bool HasParameterizedCreator { get; set; }

        internal bool HasParameterizedCreatorInternal
        {
            get
            {
                if (!this.HasParameterizedCreator && (this._parameterizedCreator == null))
                {
                    return (this._parameterizedConstructor != null);
                }
                return true;
            }
        }

        internal JsonContract KeyContract { get; set; }

        public ObjectConstructor<object> OverrideCreator
        {
            get
            {
                return this._overrideCreator;
            }
            set
            {
                this._overrideCreator = value;
            }
        }

        internal ObjectConstructor<object> ParameterizedCreator
        {
            get
            {
                if (this._parameterizedCreator == null)
                {
                    this._parameterizedCreator = JsonTypeReflector.ReflectionDelegateFactory.CreateParameterizedConstructor(this._parameterizedConstructor);
                }
                return this._parameterizedCreator;
            }
        }

        [Obsolete("PropertyNameResolver is obsolete. Use DictionaryKeyResolver instead.")]
        public Func<string, string> PropertyNameResolver
        {
            get
            {
                return this.DictionaryKeyResolver;
            }
            set
            {
                this.DictionaryKeyResolver = value;
            }
        }

        internal bool ShouldCreateWrapper { get; private set; }
    }
}

