﻿namespace Newtonsoft.Json.Serialization
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Globalization;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.Serialization;
    using System.Security;

    public class JsonObjectContract : JsonContainerContract
    {
        private JsonPropertyCollection _creatorParameters;
        private Type _extensionDataValueType;
        private bool? _hasRequiredOrDefaultValueProperties;
        private ConstructorInfo _overrideConstructor;
        private ObjectConstructor<object> _overrideCreator;
        private ObjectConstructor<object> _parameterizedCreator;
        private ConstructorInfo _parametrizedConstructor;
        internal bool ExtensionDataIsJToken;

        public JsonObjectContract(Type underlyingType) : base(underlyingType)
        {
            base.ContractType = JsonContractType.Object;
            this.Properties = new JsonPropertyCollection(base.UnderlyingType);
        }

        [SecuritySafeCritical]
        internal object GetUninitializedObject()
        {
            if (!JsonTypeReflector.FullyTrusted)
            {
                throw new JsonException("Insufficient permissions. Creating an uninitialized '{0}' type requires full trust.".FormatWith(CultureInfo.InvariantCulture, base.NonNullableUnderlyingType));
            }
            return FormatterServices.GetUninitializedObject(base.NonNullableUnderlyingType);
        }

        [Obsolete("ConstructorParameters is obsolete. Use CreatorParameters instead.")]
        public JsonPropertyCollection ConstructorParameters
        {
            get
            {
                return this.CreatorParameters;
            }
        }

        public JsonPropertyCollection CreatorParameters
        {
            get
            {
                if (this._creatorParameters == null)
                {
                    this._creatorParameters = new JsonPropertyCollection(base.UnderlyingType);
                }
                return this._creatorParameters;
            }
        }

        public Newtonsoft.Json.Serialization.ExtensionDataGetter ExtensionDataGetter { get; set; }

        public Newtonsoft.Json.Serialization.ExtensionDataSetter ExtensionDataSetter { get; set; }

        public Type ExtensionDataValueType
        {
            get
            {
                return this._extensionDataValueType;
            }
            set
            {
                this._extensionDataValueType = value;
                this.ExtensionDataIsJToken = (value != null) && typeof(JToken).IsAssignableFrom(value);
            }
        }

        internal bool HasRequiredOrDefaultValueProperties
        {
            get
            {
                if (!this._hasRequiredOrDefaultValueProperties.HasValue)
                {
                    this._hasRequiredOrDefaultValueProperties = false;
                    if (((Required) this.ItemRequired.GetValueOrDefault(Required.Default)) != Required.Default)
                    {
                        this._hasRequiredOrDefaultValueProperties = true;
                    }
                    else
                    {
                        foreach (JsonProperty property in this.Properties)
                        {
                            if (property.Required == Required.Default)
                            {
                                DefaultValueHandling? nullable2 = ((DefaultValueHandling) property.DefaultValueHandling) & DefaultValueHandling.Populate;
                                DefaultValueHandling populate = DefaultValueHandling.Populate;
                                if (!((((DefaultValueHandling) nullable2.GetValueOrDefault()) == populate) ? nullable2.HasValue : false))
                                {
                                    continue;
                                }
                            }
                            this._hasRequiredOrDefaultValueProperties = true;
                            break;
                        }
                    }
                }
                return this._hasRequiredOrDefaultValueProperties.GetValueOrDefault();
            }
        }

        public Required? ItemRequired { get; set; }

        public Newtonsoft.Json.MemberSerialization MemberSerialization { get; set; }

        [Obsolete("OverrideConstructor is obsolete. Use OverrideCreator instead.")]
        public ConstructorInfo OverrideConstructor
        {
            get
            {
                return this._overrideConstructor;
            }
            set
            {
                this._overrideConstructor = value;
                this._overrideCreator = (value != null) ? JsonTypeReflector.ReflectionDelegateFactory.CreateParameterizedConstructor(value) : null;
            }
        }

        public ObjectConstructor<object> OverrideCreator
        {
            get
            {
                return this._overrideCreator;
            }
            set
            {
                this._overrideCreator = value;
                this._overrideConstructor = null;
            }
        }

        internal ObjectConstructor<object> ParameterizedCreator
        {
            get
            {
                return this._parameterizedCreator;
            }
        }

        [Obsolete("ParametrizedConstructor is obsolete. Use OverrideCreator instead.")]
        public ConstructorInfo ParametrizedConstructor
        {
            get
            {
                return this._parametrizedConstructor;
            }
            set
            {
                this._parametrizedConstructor = value;
                this._parameterizedCreator = (value != null) ? JsonTypeReflector.ReflectionDelegateFactory.CreateParameterizedConstructor(value) : null;
            }
        }

        public JsonPropertyCollection Properties { get; private set; }
    }
}

