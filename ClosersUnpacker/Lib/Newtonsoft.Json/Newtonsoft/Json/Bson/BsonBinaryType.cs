﻿namespace Newtonsoft.Json.Bson
{
    using System;

    internal enum BsonBinaryType : byte
    {
        Binary = 0,
        [Obsolete("This type has been deprecated in the BSON specification. Use Binary instead.")]
        BinaryOld = 2,
        Function = 1,
        Md5 = 5,
        UserDefined = 0x80,
        Uuid = 4,
        [Obsolete("This type has been deprecated in the BSON specification. Use Uuid instead.")]
        UuidOld = 3
    }
}

