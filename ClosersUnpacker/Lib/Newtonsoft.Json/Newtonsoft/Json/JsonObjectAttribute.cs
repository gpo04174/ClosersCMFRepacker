﻿namespace Newtonsoft.Json
{
    using System;

    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Struct | AttributeTargets.Class, AllowMultiple=false)]
    public sealed class JsonObjectAttribute : JsonContainerAttribute
    {
        internal Required? _itemRequired;
        private Newtonsoft.Json.MemberSerialization _memberSerialization;

        public JsonObjectAttribute()
        {
        }

        public JsonObjectAttribute(Newtonsoft.Json.MemberSerialization memberSerialization)
        {
            this.MemberSerialization = memberSerialization;
        }

        public JsonObjectAttribute(string id) : base(id)
        {
        }

        public Required ItemRequired
        {
            get
            {
                Required? nullable = this._itemRequired;
                if (!nullable.HasValue)
                {
                    return Required.Default;
                }
                return nullable.GetValueOrDefault();
            }
            set
            {
                this._itemRequired = new Required?(value);
            }
        }

        public Newtonsoft.Json.MemberSerialization MemberSerialization
        {
            get
            {
                return this._memberSerialization;
            }
            set
            {
                this._memberSerialization = value;
            }
        }
    }
}

