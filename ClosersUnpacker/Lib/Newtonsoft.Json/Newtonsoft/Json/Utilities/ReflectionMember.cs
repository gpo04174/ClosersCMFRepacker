﻿namespace Newtonsoft.Json.Utilities
{
    using System;
    using System.Runtime.CompilerServices;

    internal class ReflectionMember
    {
        public Func<object, object> Getter { get; set; }

        public Type MemberType { get; set; }

        public Action<object, object> Setter { get; set; }
    }
}

