﻿namespace Ionic.Zlib
{
    using System;

    internal enum DeflateFlavor
    {
        Store,
        Fast,
        Slow
    }
}

