﻿namespace Ionic.Zlib
{
    using Ionic.Crc;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Threading;

    public class ParallelDeflateOutputStream : Stream
    {
        private int _bufferSize;
        private CompressionLevel _compressLevel;
        private int _Crc32;
        private int _currentlyFilling;
        private TraceBits _DesiredTrace;
        private object _eLock;
        private bool _firstWriteDone;
        private bool _handlingException;
        private bool _isClosed;
        private int _lastFilled;
        private int _lastWritten;
        private int _latestCompressed;
        private object _latestLock;
        private bool _leaveOpen;
        private int _maxBufferPairs;
        private AutoResetEvent _newlyCompressedBlob;
        private object _outputLock;
        private Stream _outStream;
        private volatile Exception _pendingException;
        private List<WorkItem> _pool;
        private CRC32 _runningCrc;
        private Queue<int> _toFill;
        private long _totalBytesProcessed;
        private Queue<int> _toWrite;
        private static readonly int BufferPairsPerCore = 4;
        private bool emitting;
        private static readonly int IO_BUFFER_SIZE_DEFAULT = 0x10000;

        public ParallelDeflateOutputStream(Stream stream) : this(stream, CompressionLevel.Default, CompressionStrategy.Default, false)
        {
        }

        public ParallelDeflateOutputStream(Stream stream, CompressionLevel level) : this(stream, level, CompressionStrategy.Default, false)
        {
        }

        public ParallelDeflateOutputStream(Stream stream, bool leaveOpen) : this(stream, CompressionLevel.Default, CompressionStrategy.Default, leaveOpen)
        {
        }

        public ParallelDeflateOutputStream(Stream stream, CompressionLevel level, bool leaveOpen) : this(stream, CompressionLevel.Default, CompressionStrategy.Default, leaveOpen)
        {
        }

        public ParallelDeflateOutputStream(Stream stream, CompressionLevel level, CompressionStrategy strategy, bool leaveOpen)
        {
            this._bufferSize = IO_BUFFER_SIZE_DEFAULT;
            this._outputLock = new object();
            this._latestLock = new object();
            this._eLock = new object();
            this._DesiredTrace = TraceBits.Compress | TraceBits.EmitAll | TraceBits.EmitEnter | TraceBits.Session | TraceBits.WriteEnter | TraceBits.WriteTake;
            this._outStream = stream;
            this._compressLevel = level;
            this.Strategy = strategy;
            this._leaveOpen = leaveOpen;
            this.MaxBufferPairs = 0x10;
        }

        private void _DeflateOne(object wi)
        {
            object obj2;
            WorkItem workitem = (WorkItem) wi;
            try
            {
                int index = workitem.index;
                CRC32 crc = new CRC32();
                crc.SlurpBlock(workitem.buffer, 0, workitem.inputBytesAvailable);
                this.DeflateOneSegment(workitem);
                workitem.crc = crc.Crc32Result;
                lock ((obj2 = this._latestLock))
                {
                    if (workitem.ordinal > this._latestCompressed)
                    {
                        this._latestCompressed = workitem.ordinal;
                    }
                }
                lock (this._toWrite)
                {
                    this._toWrite.Enqueue(workitem.index);
                }
                this._newlyCompressedBlob.Set();
            }
            catch (Exception exception)
            {
                lock ((obj2 = this._eLock))
                {
                    if (this._pendingException != null)
                    {
                        this._pendingException = exception;
                    }
                }
            }
        }

        private void _Flush(bool lastInput)
        {
            if (this._isClosed)
            {
                throw new InvalidOperationException();
            }
            if (!this.emitting)
            {
                if (this._currentlyFilling >= 0)
                {
                    WorkItem wi = this._pool[this._currentlyFilling];
                    this._DeflateOne(wi);
                    this._currentlyFilling = -1;
                }
                if (lastInput)
                {
                    this.EmitPendingBuffers(true, false);
                    this._FlushFinish();
                }
                else
                {
                    this.EmitPendingBuffers(false, false);
                }
            }
        }

        private void _FlushFinish()
        {
            byte[] buffer = new byte[0x80];
            ZlibCodec codec = new ZlibCodec();
            int num = codec.InitializeDeflate(this._compressLevel, false);
            codec.InputBuffer = null;
            codec.NextIn = 0;
            codec.AvailableBytesIn = 0;
            codec.OutputBuffer = buffer;
            codec.NextOut = 0;
            codec.AvailableBytesOut = buffer.Length;
            num = codec.Deflate(FlushType.Finish);
            if ((num != 1) && (num != 0))
            {
                throw new Exception("deflating: " + codec.Message);
            }
            if ((buffer.Length - codec.AvailableBytesOut) > 0)
            {
                this._outStream.Write(buffer, 0, buffer.Length - codec.AvailableBytesOut);
            }
            codec.EndDeflate();
            this._Crc32 = this._runningCrc.Crc32Result;
        }

        private void _InitializePoolOfWorkItems()
        {
            this._toWrite = new Queue<int>();
            this._toFill = new Queue<int>();
            this._pool = new List<WorkItem>();
            int num = BufferPairsPerCore * Environment.ProcessorCount;
            num = Math.Min(num, this._maxBufferPairs);
            for (int i = 0; i < num; i++)
            {
                this._pool.Add(new WorkItem(this._bufferSize, this._compressLevel, this.Strategy, i));
                this._toFill.Enqueue(i);
            }
            this._newlyCompressedBlob = new AutoResetEvent(false);
            this._runningCrc = new CRC32();
            this._currentlyFilling = -1;
            this._lastFilled = -1;
            this._lastWritten = -1;
            this._latestCompressed = -1;
        }

        public override void Close()
        {
            if (this._pendingException != null)
            {
                this._handlingException = true;
                Exception exception = this._pendingException;
                this._pendingException = null;
                throw exception;
            }
            if (!this._handlingException && !this._isClosed)
            {
                this._Flush(true);
                if (!this._leaveOpen)
                {
                    this._outStream.Close();
                }
                this._isClosed = true;
            }
        }

        private bool DeflateOneSegment(WorkItem workitem)
        {
            ZlibCodec compressor = workitem.compressor;
            int num = 0;
            compressor.ResetDeflate();
            compressor.NextIn = 0;
            compressor.AvailableBytesIn = workitem.inputBytesAvailable;
            compressor.NextOut = 0;
            compressor.AvailableBytesOut = workitem.compressed.Length;
            do
            {
                compressor.Deflate(FlushType.None);
            }
            while ((compressor.AvailableBytesIn > 0) || (compressor.AvailableBytesOut == 0));
            num = compressor.Deflate(FlushType.Sync);
            workitem.compressedBytesAvailable = (int) compressor.TotalBytesOut;
            return true;
        }

        public void Dispose()
        {
            this.Close();
            this._pool = null;
            this.Dispose(true);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        private void EmitPendingBuffers(bool doAll, bool mustWait)
        {
            if (!this.emitting)
            {
                this.emitting = true;
                if (doAll || mustWait)
                {
                    this._newlyCompressedBlob.WaitOne();
                }
                do
                {
                    int num = -1;
                    int millisecondsTimeout = doAll ? 200 : (mustWait ? -1 : 0);
                    int num3 = -1;
                    do
                    {
                        if (Monitor.TryEnter(this._toWrite, millisecondsTimeout))
                        {
                            num3 = -1;
                            try
                            {
                                if (this._toWrite.Count > 0)
                                {
                                    num3 = this._toWrite.Dequeue();
                                }
                            }
                            finally
                            {
                                Monitor.Exit(this._toWrite);
                            }
                            if (num3 >= 0)
                            {
                                WorkItem item = this._pool[num3];
                                if (item.ordinal != (this._lastWritten + 1))
                                {
                                    lock (this._toWrite)
                                    {
                                        this._toWrite.Enqueue(num3);
                                    }
                                    if (num == num3)
                                    {
                                        this._newlyCompressedBlob.WaitOne();
                                        num = -1;
                                    }
                                    else if (num == -1)
                                    {
                                        num = num3;
                                    }
                                }
                                else
                                {
                                    num = -1;
                                    this._outStream.Write(item.compressed, 0, item.compressedBytesAvailable);
                                    this._runningCrc.Combine(item.crc, item.inputBytesAvailable);
                                    this._totalBytesProcessed += item.inputBytesAvailable;
                                    item.inputBytesAvailable = 0;
                                    this._lastWritten = item.ordinal;
                                    this._toFill.Enqueue(item.index);
                                    if (millisecondsTimeout == -1)
                                    {
                                        millisecondsTimeout = 0;
                                    }
                                }
                            }
                        }
                        else
                        {
                            num3 = -1;
                        }
                    }
                    while (num3 >= 0);
                }
                while (doAll && (this._lastWritten != this._latestCompressed));
                this.emitting = false;
            }
        }

        public override void Flush()
        {
            if (this._pendingException != null)
            {
                this._handlingException = true;
                Exception exception = this._pendingException;
                this._pendingException = null;
                throw exception;
            }
            if (!this._handlingException)
            {
                this._Flush(false);
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new NotSupportedException();
        }

        public void Reset(Stream stream)
        {
            if (this._firstWriteDone)
            {
                this._toWrite.Clear();
                this._toFill.Clear();
                foreach (WorkItem item in this._pool)
                {
                    this._toFill.Enqueue(item.index);
                    item.ordinal = -1;
                }
                this._firstWriteDone = false;
                this._totalBytesProcessed = 0L;
                this._runningCrc = new CRC32();
                this._isClosed = false;
                this._currentlyFilling = -1;
                this._lastFilled = -1;
                this._lastWritten = -1;
                this._latestCompressed = -1;
                this._outStream = stream;
            }
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotSupportedException();
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }

        [Conditional("Trace")]
        private void TraceOutput(TraceBits bits, string format, params object[] varParams)
        {
            if ((bits & this._DesiredTrace) != TraceBits.None)
            {
                lock (this._outputLock)
                {
                    int hashCode = Thread.CurrentThread.GetHashCode();
                    Console.ForegroundColor = (ConsoleColor) ((hashCode % 8) + 8);
                    Console.Write("{0:000} PDOS ", hashCode);
                    Console.WriteLine(format, varParams);
                    Console.ResetColor();
                }
            }
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            bool mustWait = false;
            if (this._isClosed)
            {
                throw new InvalidOperationException();
            }
            if (this._pendingException != null)
            {
                this._handlingException = true;
                Exception exception = this._pendingException;
                this._pendingException = null;
                throw exception;
            }
            if (count == 0)
            {
                return;
            }
            if (!this._firstWriteDone)
            {
                this._InitializePoolOfWorkItems();
                this._firstWriteDone = true;
            }
        Label_0073:
            this.EmitPendingBuffers(false, mustWait);
            mustWait = false;
            int num = -1;
            if (this._currentlyFilling >= 0)
            {
                num = this._currentlyFilling;
            }
            else
            {
                if (this._toFill.Count == 0)
                {
                    mustWait = true;
                    goto Label_01A2;
                }
                num = this._toFill.Dequeue();
                this._lastFilled++;
            }
            WorkItem state = this._pool[num];
            int num2 = ((state.buffer.Length - state.inputBytesAvailable) > count) ? count : (state.buffer.Length - state.inputBytesAvailable);
            state.ordinal = this._lastFilled;
            Buffer.BlockCopy(buffer, offset, state.buffer, state.inputBytesAvailable, num2);
            count -= num2;
            offset += num2;
            state.inputBytesAvailable += num2;
            if (state.inputBytesAvailable == state.buffer.Length)
            {
                if (!ThreadPool.QueueUserWorkItem(new WaitCallback(this._DeflateOne), state))
                {
                    throw new Exception("Cannot enqueue workitem");
                }
                this._currentlyFilling = -1;
            }
            else
            {
                this._currentlyFilling = num;
            }
            if (count > 0)
            {
            }
        Label_01A2:
            if (count > 0)
            {
                goto Label_0073;
            }
        }

        public int BufferSize
        {
            get
            {
                return this._bufferSize;
            }
            set
            {
                if (value < 0x400)
                {
                    throw new ArgumentOutOfRangeException("BufferSize", "BufferSize must be greater than 1024 bytes");
                }
                this._bufferSize = value;
            }
        }

        public long BytesProcessed
        {
            get
            {
                return this._totalBytesProcessed;
            }
        }

        public override bool CanRead
        {
            get
            {
                return false;
            }
        }

        public override bool CanSeek
        {
            get
            {
                return false;
            }
        }

        public override bool CanWrite
        {
            get
            {
                return this._outStream.CanWrite;
            }
        }

        public int Crc32
        {
            get
            {
                return this._Crc32;
            }
        }

        public override long Length
        {
            get
            {
                throw new NotSupportedException();
            }
        }

        public int MaxBufferPairs
        {
            get
            {
                return this._maxBufferPairs;
            }
            set
            {
                if (value < 4)
                {
                    throw new ArgumentException("MaxBufferPairs", "Value must be 4 or greater.");
                }
                this._maxBufferPairs = value;
            }
        }

        public override long Position
        {
            get
            {
                return this._outStream.Position;
            }
            set
            {
                throw new NotSupportedException();
            }
        }

        public CompressionStrategy Strategy { get; private set; }

        [Flags]
        private enum TraceBits : uint
        {
            All = 0xffffffff,
            Compress = 0x800,
            EmitAll = 0x3a,
            EmitBegin = 8,
            EmitDone = 0x10,
            EmitEnter = 4,
            EmitLock = 2,
            EmitSkip = 0x20,
            Flush = 0x40,
            Instance = 0x400,
            Lifecycle = 0x80,
            None = 0,
            NotUsed1 = 1,
            Session = 0x100,
            Synch = 0x200,
            Write = 0x1000,
            WriteEnter = 0x2000,
            WriteTake = 0x4000
        }
    }
}

