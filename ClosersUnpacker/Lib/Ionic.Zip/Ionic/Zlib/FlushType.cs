﻿namespace Ionic.Zlib
{
    using System;

    public enum FlushType
    {
        None,
        Partial,
        Sync,
        Full,
        Finish
    }
}

