﻿namespace Ionic.Zlib
{
    using System;

    public enum CompressionStrategy
    {
        Default,
        Filtered,
        HuffmanOnly
    }
}

