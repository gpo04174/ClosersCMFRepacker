﻿namespace Ionic
{
    using System;
    using System.ComponentModel;

    internal sealed class EnumUtil
    {
        private EnumUtil()
        {
        }

        internal static string GetDescription(Enum value)
        {
            DescriptionAttribute[] customAttributes = (DescriptionAttribute[]) value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (customAttributes.Length > 0)
            {
                return customAttributes[0].Description;
            }
            return value.ToString();
        }

        internal static object Parse(Type enumType, string stringRepresentation)
        {
            return Parse(enumType, stringRepresentation, false);
        }

        internal static object Parse(Type enumType, string stringRepresentation, bool ignoreCase)
        {
            if (ignoreCase)
            {
                stringRepresentation = stringRepresentation.ToLower();
            }
            foreach (Enum enum2 in Enum.GetValues(enumType))
            {
                string description = GetDescription(enum2);
                if (ignoreCase)
                {
                    description = description.ToLower();
                }
                if (description == stringRepresentation)
                {
                    return enum2;
                }
            }
            return Enum.Parse(enumType, stringRepresentation, ignoreCase);
        }
    }
}

