﻿namespace Ionic.Crc
{
    using System;
    using System.IO;

    public class CrcCalculatorStream : Stream, IDisposable
    {
        private CRC32 _Crc32;
        internal Stream _innerStream;
        private bool _leaveOpen;
        private long _lengthLimit;
        private static readonly long UnsetLengthLimit = -99L;

        public CrcCalculatorStream(Stream stream) : this(true, UnsetLengthLimit, stream, null)
        {
        }

        public CrcCalculatorStream(Stream stream, bool leaveOpen) : this(leaveOpen, UnsetLengthLimit, stream, null)
        {
        }

        public CrcCalculatorStream(Stream stream, long length) : this(true, length, stream, null)
        {
            if (length < 0L)
            {
                throw new ArgumentException("length");
            }
        }

        public CrcCalculatorStream(Stream stream, long length, bool leaveOpen) : this(leaveOpen, length, stream, null)
        {
            if (length < 0L)
            {
                throw new ArgumentException("length");
            }
        }

        private CrcCalculatorStream(bool leaveOpen, long length, Stream stream, CRC32 crc32)
        {
            this._lengthLimit = -99L;
            this._innerStream = stream;
            this._Crc32 = crc32 ?? new CRC32();
            this._lengthLimit = length;
            this._leaveOpen = leaveOpen;
        }

        public CrcCalculatorStream(Stream stream, long length, bool leaveOpen, CRC32 crc32) : this(leaveOpen, length, stream, crc32)
        {
            if (length < 0L)
            {
                throw new ArgumentException("length");
            }
        }

        public override void Close()
        {
            base.Close();
            if (!this._leaveOpen)
            {
                this._innerStream.Close();
            }
        }

        public override void Flush()
        {
            this._innerStream.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int num = count;
            if (this._lengthLimit != UnsetLengthLimit)
            {
                if (this._Crc32.TotalBytesRead >= this._lengthLimit)
                {
                    return 0;
                }
                long num2 = this._lengthLimit - this._Crc32.TotalBytesRead;
                if (num2 < count)
                {
                    num = (int) num2;
                }
            }
            int num3 = this._innerStream.Read(buffer, offset, num);
            if (num3 > 0)
            {
                this._Crc32.SlurpBlock(buffer, offset, num3);
            }
            return num3;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotSupportedException();
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }

        void IDisposable.Dispose()
        {
            this.Close();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            if (count > 0)
            {
                this._Crc32.SlurpBlock(buffer, offset, count);
            }
            this._innerStream.Write(buffer, offset, count);
        }

        public override bool CanRead
        {
            get
            {
                return this._innerStream.CanRead;
            }
        }

        public override bool CanSeek
        {
            get
            {
                return false;
            }
        }

        public override bool CanWrite
        {
            get
            {
                return this._innerStream.CanWrite;
            }
        }

        public int Crc
        {
            get
            {
                return this._Crc32.Crc32Result;
            }
        }

        public bool LeaveOpen
        {
            get
            {
                return this._leaveOpen;
            }
            set
            {
                this._leaveOpen = value;
            }
        }

        public override long Length
        {
            get
            {
                if (this._lengthLimit == UnsetLengthLimit)
                {
                    return this._innerStream.Length;
                }
                return this._lengthLimit;
            }
        }

        public override long Position
        {
            get
            {
                return this._Crc32.TotalBytesRead;
            }
            set
            {
                throw new NotSupportedException();
            }
        }

        public long TotalBytesSlurped
        {
            get
            {
                return this._Crc32.TotalBytesRead;
            }
        }
    }
}

