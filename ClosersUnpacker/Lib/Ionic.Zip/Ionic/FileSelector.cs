﻿namespace Ionic
{
    using Ionic.Zip;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Text.RegularExpressions;

    public class FileSelector
    {
        internal SelectionCriterion _Criterion;

        public FileSelector(string selectionCriteria) : this(selectionCriteria, true)
        {
        }

        public FileSelector(string selectionCriteria, bool traverseDirectoryReparsePoints)
        {
            if (!string.IsNullOrEmpty(selectionCriteria))
            {
                this._Criterion = _ParseCriterion(selectionCriteria);
            }
            this.TraverseReparsePoints = traverseDirectoryReparsePoints;
        }

        private static SelectionCriterion _ParseCriterion(string s)
        {
            if (s == null)
            {
                return null;
            }
            s = NormalizeCriteriaExpression(s);
            if (s.IndexOf(" ") == -1)
            {
                s = "name = " + s;
            }
            string[] strArray = s.Trim().Split(new char[] { ' ', '\t' });
            if (strArray.Length < 3)
            {
                throw new ArgumentException(s);
            }
            SelectionCriterion item = null;
            LogicalConjunction nONE = LogicalConjunction.NONE;
            Stack<ParseState> stack = new Stack<ParseState>();
            Stack<SelectionCriterion> stack2 = new Stack<SelectionCriterion>();
            stack.Push(ParseState.Start);
            for (int i = 0; i < strArray.Length; i++)
            {
                ParseState state;
                DateTime time;
                long num2;
                ComparisonOperator @operator;
                string str = strArray[i].ToLower();
                switch (str)
                {
                    case "and":
                    case "xor":
                    case "or":
                        state = stack.Peek();
                        if (state != ParseState.CriterionDone)
                        {
                            throw new ArgumentException(string.Join(" ", strArray, i, strArray.Length - i));
                        }
                        break;

                    case "(":
                        state = stack.Peek();
                        if (((state != ParseState.Start) && (state != ParseState.ConjunctionPending)) && (state != ParseState.OpenParen))
                        {
                            throw new ArgumentException(string.Join(" ", strArray, i, strArray.Length - i));
                        }
                        goto Label_02E0;

                    case ")":
                        state = stack.Pop();
                        if (((ParseState) stack.Peek()) != ParseState.OpenParen)
                        {
                            throw new ArgumentException(string.Join(" ", strArray, i, strArray.Length - i));
                        }
                        goto Label_0347;

                    case "atime":
                    case "ctime":
                    case "mtime":
                        if (strArray.Length <= (i + 2))
                        {
                            throw new ArgumentException(string.Join(" ", strArray, i, strArray.Length - i));
                        }
                        goto Label_0385;

                    case "length":
                    case "size":
                        if (strArray.Length <= (i + 2))
                        {
                            throw new ArgumentException(string.Join(" ", strArray, i, strArray.Length - i));
                        }
                        goto Label_04AF;

                    case "filename":
                    case "name":
                        if (strArray.Length <= (i + 2))
                        {
                            throw new ArgumentException(string.Join(" ", strArray, i, strArray.Length - i));
                        }
                        goto Label_06D1;

                    case "attrs":
                    case "attributes":
                    case "type":
                        if (strArray.Length <= (i + 2))
                        {
                            throw new ArgumentException(string.Join(" ", strArray, i, strArray.Length - i));
                        }
                        goto Label_07C6;

                    case "":
                        stack.Push(ParseState.Whitespace);
                        goto Label_0895;

                    default:
                        throw new ArgumentException("'" + strArray[i] + "'");
                }
                if (strArray.Length <= (i + 3))
                {
                    throw new ArgumentException(string.Join(" ", strArray, i, strArray.Length - i));
                }
                nONE = (LogicalConjunction) Enum.Parse(typeof(LogicalConjunction), strArray[i].ToUpper(), true);
                CompoundCriterion criterion5 = new CompoundCriterion {
                    Left = item,
                    Right = null,
                    Conjunction = nONE
                };
                item = criterion5;
                stack.Push(state);
                stack.Push(ParseState.ConjunctionPending);
                stack2.Push(item);
                goto Label_0895;
            Label_02E0:
                if (strArray.Length <= (i + 4))
                {
                    throw new ArgumentException(string.Join(" ", strArray, i, strArray.Length - i));
                }
                stack.Push(ParseState.OpenParen);
                goto Label_0895;
            Label_0347:
                stack.Pop();
                stack.Push(ParseState.CriterionDone);
                goto Label_0895;
            Label_0385:
                try
                {
                    time = DateTime.ParseExact(strArray[i + 2], "yyyy-MM-dd-HH:mm:ss", null);
                }
                catch (FormatException)
                {
                    try
                    {
                        time = DateTime.ParseExact(strArray[i + 2], "yyyy/MM/dd-HH:mm:ss", null);
                    }
                    catch (FormatException)
                    {
                        try
                        {
                            time = DateTime.ParseExact(strArray[i + 2], "yyyy/MM/dd", null);
                        }
                        catch (FormatException)
                        {
                            try
                            {
                                time = DateTime.ParseExact(strArray[i + 2], "MM/dd/yyyy", null);
                            }
                            catch (FormatException)
                            {
                                time = DateTime.ParseExact(strArray[i + 2], "yyyy-MM-dd", null);
                            }
                        }
                    }
                }
                time = DateTime.SpecifyKind(time, DateTimeKind.Local).ToUniversalTime();
                TimeCriterion criterion6 = new TimeCriterion {
                    Which = (WhichTime) Enum.Parse(typeof(WhichTime), strArray[i], true),
                    Operator = (ComparisonOperator) EnumUtil.Parse(typeof(ComparisonOperator), strArray[i + 1]),
                    Time = time
                };
                item = criterion6;
                i += 2;
                stack.Push(ParseState.CriterionDone);
                goto Label_0895;
            Label_04AF:
                num2 = 0L;
                string str2 = strArray[i + 2];
                if (str2.ToUpper().EndsWith("K"))
                {
                    num2 = long.Parse(str2.Substring(0, str2.Length - 1)) * 0x400L;
                }
                else if (str2.ToUpper().EndsWith("KB"))
                {
                    num2 = long.Parse(str2.Substring(0, str2.Length - 2)) * 0x400L;
                }
                else if (str2.ToUpper().EndsWith("M"))
                {
                    num2 = (long.Parse(str2.Substring(0, str2.Length - 1)) * 0x400L) * 0x400L;
                }
                else if (str2.ToUpper().EndsWith("MB"))
                {
                    num2 = (long.Parse(str2.Substring(0, str2.Length - 2)) * 0x400L) * 0x400L;
                }
                else if (str2.ToUpper().EndsWith("G"))
                {
                    num2 = ((long.Parse(str2.Substring(0, str2.Length - 1)) * 0x400L) * 0x400L) * 0x400L;
                }
                else if (str2.ToUpper().EndsWith("GB"))
                {
                    num2 = ((long.Parse(str2.Substring(0, str2.Length - 2)) * 0x400L) * 0x400L) * 0x400L;
                }
                else
                {
                    num2 = long.Parse(strArray[i + 2]);
                }
                SizeCriterion criterion7 = new SizeCriterion {
                    Size = num2,
                    Operator = (ComparisonOperator) EnumUtil.Parse(typeof(ComparisonOperator), strArray[i + 1])
                };
                item = criterion7;
                i += 2;
                stack.Push(ParseState.CriterionDone);
                goto Label_0895;
            Label_06D1:
                @operator = (ComparisonOperator) EnumUtil.Parse(typeof(ComparisonOperator), strArray[i + 1]);
                if ((@operator != ComparisonOperator.NotEqualTo) && (@operator != ComparisonOperator.EqualTo))
                {
                    throw new ArgumentException(string.Join(" ", strArray, i, strArray.Length - i));
                }
                string str3 = strArray[i + 2];
                if (str3.StartsWith("'") && str3.EndsWith("'"))
                {
                    str3 = str3.Substring(1, str3.Length - 2).Replace("\x0006", " ");
                }
                NameCriterion criterion2 = new NameCriterion {
                    MatchingFileSpec = str3,
                    Operator = @operator
                };
                item = criterion2;
                i += 2;
                stack.Push(ParseState.CriterionDone);
                goto Label_0895;
            Label_07C6:
                @operator = (ComparisonOperator) EnumUtil.Parse(typeof(ComparisonOperator), strArray[i + 1]);
                if ((@operator != ComparisonOperator.NotEqualTo) && (@operator != ComparisonOperator.EqualTo))
                {
                    throw new ArgumentException(string.Join(" ", strArray, i, strArray.Length - i));
                }
                item = (str == "type") ? ((SelectionCriterion) new TypeCriterion()) : ((SelectionCriterion) new AttributesCriterion());
                i += 2;
                stack.Push(ParseState.CriterionDone);
            Label_0895:
                state = stack.Peek();
                switch (state)
                {
                    case ParseState.CriterionDone:
                        stack.Pop();
                        if (((ParseState) stack.Peek()) == ParseState.ConjunctionPending)
                        {
                            while (((ParseState) stack.Peek()) == ParseState.ConjunctionPending)
                            {
                                CompoundCriterion criterion8 = stack2.Pop() as CompoundCriterion;
                                criterion8.Right = item;
                                item = criterion8;
                                stack.Pop();
                                state = stack.Pop();
                                if (state != ParseState.CriterionDone)
                                {
                                    throw new ArgumentException("??");
                                }
                            }
                        }
                        else
                        {
                            stack.Push(ParseState.CriterionDone);
                        }
                        break;

                    case ParseState.Whitespace:
                        stack.Pop();
                        break;
                }
            }
            return item;
        }

        private bool Evaluate(ZipEntry entry)
        {
            return this._Criterion.Evaluate(entry);
        }

        private bool Evaluate(string filename)
        {
            return this._Criterion.Evaluate(filename);
        }

        private static string NormalizeCriteriaExpression(string source)
        {
            string[][] strArray = new string[][] { new string[] { @"([^']*)\(\(([^']+)", "$1( ($2" }, new string[] { @"(.)\)\)", "$1) )" }, new string[] { @"\((\S)", "( $1" }, new string[] { @"(\S)\)", "$1 )" }, new string[] { @"^\)", " )" }, new string[] { @"(\S)\(", "$1 (" }, new string[] { @"\)(\S)", ") $1" }, new string[] { "(=)('[^']*')", "$1 $2" }, new string[] { "([^ !><])(>|<|!=|=)", "$1 $2" }, new string[] { "(>|<|!=|=)([^ =])", "$1 $2" }, new string[] { "/", @"\" } };
            string input = source;
            for (int i = 0; i < strArray.Length; i++)
            {
                string str2 = RegexAssertions.PrecededByEvenNumberOfSingleQuotes + strArray[i][0] + RegexAssertions.FollowedByEvenNumberOfSingleQuotesAndLineEnd;
                input = Regex.Replace(input, str2, strArray[i][1]);
            }
            string pattern = "/" + RegexAssertions.FollowedByOddNumberOfSingleQuotesAndLineEnd;
            input = Regex.Replace(input, pattern, @"\");
            pattern = " " + RegexAssertions.FollowedByOddNumberOfSingleQuotesAndLineEnd;
            return Regex.Replace(input, pattern, "\x0006");
        }

        public ICollection<ZipEntry> SelectEntries(ZipFile zip)
        {
            if (zip == null)
            {
                throw new ArgumentNullException("zip");
            }
            List<ZipEntry> list = new List<ZipEntry>();
            foreach (ZipEntry entry in zip)
            {
                if (this.Evaluate(entry))
                {
                    list.Add(entry);
                }
            }
            return list;
        }

        public ICollection<ZipEntry> SelectEntries(ZipFile zip, string directoryPathInArchive)
        {
            if (zip == null)
            {
                throw new ArgumentNullException("zip");
            }
            List<ZipEntry> list = new List<ZipEntry>();
            string str = (directoryPathInArchive == null) ? null : directoryPathInArchive.Replace("/", @"\");
            if (str != null)
            {
                while (str.EndsWith(@"\"))
                {
                    str = str.Substring(0, str.Length - 1);
                }
            }
            foreach (ZipEntry entry in zip)
            {
                if ((((directoryPathInArchive == null) || (Path.GetDirectoryName(entry.FileName) == directoryPathInArchive)) || (Path.GetDirectoryName(entry.FileName) == str)) && this.Evaluate(entry))
                {
                    list.Add(entry);
                }
            }
            return list;
        }

        public ICollection<string> SelectFiles(string directory)
        {
            return this.SelectFiles(directory, false);
        }

        public ReadOnlyCollection<string> SelectFiles(string directory, bool recurseDirectories)
        {
            if (this._Criterion == null)
            {
                throw new ArgumentException("SelectionCriteria has not been set");
            }
            List<string> list = new List<string>();
            try
            {
                if (Directory.Exists(directory))
                {
                    string[] files = Directory.GetFiles(directory);
                    foreach (string str in files)
                    {
                        if (this.Evaluate(str))
                        {
                            list.Add(str);
                        }
                    }
                    if (recurseDirectories)
                    {
                        string[] directories = Directory.GetDirectories(directory);
                        foreach (string str2 in directories)
                        {
                            if (this.TraverseReparsePoints || ((File.GetAttributes(str2) & FileAttributes.ReparsePoint) == 0))
                            {
                                if (this.Evaluate(str2))
                                {
                                    list.Add(str2);
                                }
                                list.AddRange(this.SelectFiles(str2, recurseDirectories));
                            }
                        }
                    }
                }
            }
            catch (UnauthorizedAccessException)
            {
            }
            catch (IOException)
            {
            }
            return list.AsReadOnly();
        }

        [Conditional("SelectorTrace")]
        private void SelectorTrace(string format, params object[] args)
        {
            if ((this._Criterion != null) && this._Criterion.Verbose)
            {
                Console.WriteLine(format, args);
            }
        }

        public override string ToString()
        {
            return ("FileSelector(" + this._Criterion.ToString() + ")");
        }

        public string SelectionCriteria
        {
            get
            {
                if (this._Criterion == null)
                {
                    return null;
                }
                return this._Criterion.ToString();
            }
            set
            {
                if (value == null)
                {
                    this._Criterion = null;
                }
                else if (value.Trim() == "")
                {
                    this._Criterion = null;
                }
                else
                {
                    this._Criterion = _ParseCriterion(value);
                }
            }
        }

        public bool TraverseReparsePoints { get; set; }

        private enum ParseState
        {
            Start,
            OpenParen,
            CriterionDone,
            ConjunctionPending,
            Whitespace
        }

        private static class RegexAssertions
        {
            public static readonly string FollowedByEvenNumberOfSingleQuotesAndLineEnd = "(?=(?:[^']*'[^']*')*[^']*$)";
            public static readonly string FollowedByOddNumberOfSingleQuotesAndLineEnd = "(?=[^']*'(?:[^']*'[^']*')*[^']*$)";
            public static readonly string PrecededByEvenNumberOfSingleQuotes = "(?<=(?:[^']*'[^']*')*[^']*)";
            public static readonly string PrecededByOddNumberOfSingleQuotes = "(?<=(?:[^']*'[^']*')*'[^']*)";
        }
    }
}

