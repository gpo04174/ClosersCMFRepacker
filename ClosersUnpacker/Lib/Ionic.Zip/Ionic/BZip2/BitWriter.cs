﻿namespace Ionic.BZip2
{
    using System;
    using System.IO;

    internal class BitWriter
    {
        private uint accumulator;
        private int nAccumulatedBits;
        private Stream output;
        private int totalBytesWrittenOut;

        public BitWriter(Stream s)
        {
            this.output = s;
        }

        public void FinishAndPad()
        {
            this.Flush();
            if (this.NumRemainingBits > 0)
            {
                byte num = (byte) ((this.accumulator >> 0x18) & 0xff);
                this.output.WriteByte(num);
                this.totalBytesWrittenOut++;
            }
        }

        public void Flush()
        {
            this.WriteBits(0, 0);
        }

        public void Reset()
        {
            this.accumulator = 0;
            this.nAccumulatedBits = 0;
            this.totalBytesWrittenOut = 0;
            this.output.Seek(0L, SeekOrigin.Begin);
            this.output.SetLength(0L);
        }

        public void WriteBits(int nbits, uint value)
        {
            int nAccumulatedBits = this.nAccumulatedBits;
            uint accumulator = this.accumulator;
            while (nAccumulatedBits >= 8)
            {
                this.output.WriteByte((byte) ((accumulator >> 0x18) & 0xff));
                this.totalBytesWrittenOut++;
                accumulator = accumulator << 8;
                nAccumulatedBits -= 8;
            }
            this.accumulator = accumulator | (value << ((0x20 - nAccumulatedBits) - nbits));
            this.nAccumulatedBits = nAccumulatedBits + nbits;
        }

        public void WriteByte(byte b)
        {
            this.WriteBits(8, b);
        }

        public void WriteInt(uint u)
        {
            this.WriteBits(8, (u >> 0x18) & 0xff);
            this.WriteBits(8, (u >> 0x10) & 0xff);
            this.WriteBits(8, (u >> 8) & 0xff);
            this.WriteBits(8, u & 0xff);
        }

        public int NumRemainingBits
        {
            get
            {
                return this.nAccumulatedBits;
            }
        }

        public byte RemainingBits
        {
            get
            {
                return (byte) ((this.accumulator >> (0x20 - this.nAccumulatedBits)) & 0xff);
            }
        }

        public int TotalBytesWrittenOut
        {
            get
            {
                return this.totalBytesWrittenOut;
            }
        }
    }
}

