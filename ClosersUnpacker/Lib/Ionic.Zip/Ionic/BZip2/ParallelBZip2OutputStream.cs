﻿namespace Ionic.BZip2
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Threading;

    public class ParallelBZip2OutputStream : Stream
    {
        private int _maxWorkers;
        private int blockSize100k;
        private static readonly int BufferPairsPerCore = 4;
        private BitWriter bw;
        private uint combinedCRC;
        private int currentlyFilling;
        private TraceBits desiredTrace;
        private object eLock;
        private bool emitting;
        private bool firstWriteDone;
        private bool handlingException;
        private int lastFilled;
        private int lastWritten;
        private int latestCompressed;
        private object latestLock;
        private bool leaveOpen;
        private AutoResetEvent newlyCompressedBlob;
        private Stream output;
        private object outputLock;
        private volatile Exception pendingException;
        private List<WorkItem> pool;
        private Queue<int> toFill;
        private long totalBytesWrittenIn;
        private long totalBytesWrittenOut;
        private Queue<int> toWrite;

        public ParallelBZip2OutputStream(Stream output) : this(output, Ionic.BZip2.BZip2.MaxBlockSize, false)
        {
        }

        public ParallelBZip2OutputStream(Stream output, bool leaveOpen) : this(output, Ionic.BZip2.BZip2.MaxBlockSize, leaveOpen)
        {
        }

        public ParallelBZip2OutputStream(Stream output, int blockSize) : this(output, blockSize, false)
        {
        }

        public ParallelBZip2OutputStream(Stream output, int blockSize, bool leaveOpen)
        {
            this.latestLock = new object();
            this.eLock = new object();
            this.outputLock = new object();
            this.desiredTrace = TraceBits.Crc | TraceBits.Write;
            if ((blockSize < Ionic.BZip2.BZip2.MinBlockSize) || (blockSize > Ionic.BZip2.BZip2.MaxBlockSize))
            {
                throw new ArgumentException(string.Format("blockSize={0} is out of range; must be between {1} and {2}", blockSize, Ionic.BZip2.BZip2.MinBlockSize, Ionic.BZip2.BZip2.MaxBlockSize), "blockSize");
            }
            this.output = output;
            if (!this.output.CanWrite)
            {
                throw new ArgumentException("The stream is not writable.", "output");
            }
            this.bw = new BitWriter(this.output);
            this.blockSize100k = blockSize;
            this.leaveOpen = leaveOpen;
            this.combinedCRC = 0;
            this.MaxWorkers = 0x10;
            this.EmitHeader();
        }

        public override void Close()
        {
            if (this.pendingException != null)
            {
                this.handlingException = true;
                Exception pendingException = this.pendingException;
                this.pendingException = null;
                throw pendingException;
            }
            if (!this.handlingException && (this.output != null))
            {
                Stream output = this.output;
                try
                {
                    this.FlushOutput(true);
                }
                finally
                {
                    this.output = null;
                    this.bw = null;
                }
                if (!this.leaveOpen)
                {
                    output.Close();
                }
            }
        }

        private void CompressOne(object wi)
        {
            object obj2;
            WorkItem item = (WorkItem) wi;
            try
            {
                item.Compressor.CompressAndWrite();
                lock ((obj2 = this.latestLock))
                {
                    if (item.ordinal > this.latestCompressed)
                    {
                        this.latestCompressed = item.ordinal;
                    }
                }
                lock (this.toWrite)
                {
                    this.toWrite.Enqueue(item.index);
                }
                this.newlyCompressedBlob.Set();
            }
            catch (Exception exception)
            {
                lock ((obj2 = this.eLock))
                {
                    if (this.pendingException != null)
                    {
                        this.pendingException = exception;
                    }
                }
            }
        }

        private void EmitHeader()
        {
            byte[] buffer2 = new byte[] { 0x42, 90, 0x68, 0 };
            buffer2[3] = (byte) (0x30 + this.blockSize100k);
            byte[] buffer = buffer2;
            this.output.Write(buffer, 0, buffer.Length);
        }

        private void EmitPendingBuffers(bool doAll, bool mustWait)
        {
            if (!this.emitting)
            {
                this.emitting = true;
                if (doAll || mustWait)
                {
                    this.newlyCompressedBlob.WaitOne();
                }
                do
                {
                    int num = -1;
                    int millisecondsTimeout = doAll ? 200 : (mustWait ? -1 : 0);
                    int num3 = -1;
                    do
                    {
                        if (Monitor.TryEnter(this.toWrite, millisecondsTimeout))
                        {
                            num3 = -1;
                            try
                            {
                                if (this.toWrite.Count > 0)
                                {
                                    num3 = this.toWrite.Dequeue();
                                }
                            }
                            finally
                            {
                                Monitor.Exit(this.toWrite);
                            }
                            if (num3 >= 0)
                            {
                                WorkItem item = this.pool[num3];
                                if (item.ordinal != (this.lastWritten + 1))
                                {
                                    lock (this.toWrite)
                                    {
                                        this.toWrite.Enqueue(num3);
                                    }
                                    if (num == num3)
                                    {
                                        this.newlyCompressedBlob.WaitOne();
                                        num = -1;
                                    }
                                    else if (num == -1)
                                    {
                                        num = num3;
                                    }
                                }
                                else
                                {
                                    int num4;
                                    num = -1;
                                    BitWriter bw = item.bw;
                                    bw.Flush();
                                    MemoryStream ms = item.ms;
                                    ms.Seek(0L, SeekOrigin.Begin);
                                    int num5 = -1;
                                    long num6 = 0L;
                                    byte[] buffer = new byte[0x400];
                                    while ((num4 = ms.Read(buffer, 0, buffer.Length)) > 0)
                                    {
                                        num5 = num4;
                                        for (int i = 0; i < num4; i++)
                                        {
                                            this.bw.WriteByte(buffer[i]);
                                        }
                                        num6 += num4;
                                    }
                                    if (bw.NumRemainingBits > 0)
                                    {
                                        this.bw.WriteBits(bw.NumRemainingBits, bw.RemainingBits);
                                    }
                                    this.combinedCRC = (this.combinedCRC << 1) | (this.combinedCRC >> 0x1f);
                                    this.combinedCRC ^= item.Compressor.Crc32;
                                    this.totalBytesWrittenOut += num6;
                                    bw.Reset();
                                    this.lastWritten = item.ordinal;
                                    item.ordinal = -1;
                                    this.toFill.Enqueue(item.index);
                                    if (millisecondsTimeout == -1)
                                    {
                                        millisecondsTimeout = 0;
                                    }
                                }
                            }
                        }
                        else
                        {
                            num3 = -1;
                        }
                    }
                    while (num3 >= 0);
                }
                while (doAll && (this.lastWritten != this.latestCompressed));
                if (doAll)
                {
                }
                this.emitting = false;
            }
        }

        private void EmitTrailer()
        {
            this.bw.WriteByte(0x17);
            this.bw.WriteByte(0x72);
            this.bw.WriteByte(0x45);
            this.bw.WriteByte(0x38);
            this.bw.WriteByte(80);
            this.bw.WriteByte(0x90);
            this.bw.WriteInt(this.combinedCRC);
            this.bw.FinishAndPad();
        }

        public override void Flush()
        {
            if (this.output != null)
            {
                this.FlushOutput(false);
                this.bw.Flush();
                this.output.Flush();
            }
        }

        private void FlushOutput(bool lastInput)
        {
            if (!this.emitting)
            {
                if (this.currentlyFilling >= 0)
                {
                    WorkItem wi = this.pool[this.currentlyFilling];
                    this.CompressOne(wi);
                    this.currentlyFilling = -1;
                }
                if (lastInput)
                {
                    this.EmitPendingBuffers(true, false);
                    this.EmitTrailer();
                }
                else
                {
                    this.EmitPendingBuffers(false, false);
                }
            }
        }

        private void InitializePoolOfWorkItems()
        {
            this.toWrite = new Queue<int>();
            this.toFill = new Queue<int>();
            this.pool = new List<WorkItem>();
            int num = BufferPairsPerCore * Environment.ProcessorCount;
            num = Math.Min(num, this.MaxWorkers);
            for (int i = 0; i < num; i++)
            {
                this.pool.Add(new WorkItem(i, this.blockSize100k));
                this.toFill.Enqueue(i);
            }
            this.newlyCompressedBlob = new AutoResetEvent(false);
            this.currentlyFilling = -1;
            this.lastFilled = -1;
            this.lastWritten = -1;
            this.latestCompressed = -1;
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        [Conditional("Trace")]
        private void TraceOutput(TraceBits bits, string format, params object[] varParams)
        {
            if ((bits & this.desiredTrace) != TraceBits.None)
            {
                lock (this.outputLock)
                {
                    int hashCode = Thread.CurrentThread.GetHashCode();
                    Console.ForegroundColor = (ConsoleColor) ((hashCode % 8) + 10);
                    Console.Write("{0:000} PBOS ", hashCode);
                    Console.WriteLine(format, varParams);
                    Console.ResetColor();
                }
            }
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            bool mustWait = false;
            if (this.output == null)
            {
                throw new IOException("the stream is not open");
            }
            if (this.pendingException != null)
            {
                this.handlingException = true;
                Exception pendingException = this.pendingException;
                this.pendingException = null;
                throw pendingException;
            }
            if (offset < 0)
            {
                throw new IndexOutOfRangeException(string.Format("offset ({0}) must be > 0", offset));
            }
            if (count < 0)
            {
                throw new IndexOutOfRangeException(string.Format("count ({0}) must be > 0", count));
            }
            if ((offset + count) > buffer.Length)
            {
                throw new IndexOutOfRangeException(string.Format("offset({0}) count({1}) bLength({2})", offset, count, buffer.Length));
            }
            if (count == 0)
            {
                return;
            }
            if (!this.firstWriteDone)
            {
                this.InitializePoolOfWorkItems();
                this.firstWriteDone = true;
            }
            int num = 0;
            int num2 = count;
        Label_00FA:
            this.EmitPendingBuffers(false, mustWait);
            mustWait = false;
            int currentlyFilling = -1;
            if (this.currentlyFilling >= 0)
            {
                currentlyFilling = this.currentlyFilling;
            }
            else
            {
                if (this.toFill.Count == 0)
                {
                    mustWait = true;
                    goto Label_01E0;
                }
                currentlyFilling = this.toFill.Dequeue();
                this.lastFilled++;
            }
            WorkItem state = this.pool[currentlyFilling];
            state.ordinal = this.lastFilled;
            int num4 = state.Compressor.Fill(buffer, offset, num2);
            if (num4 != num2)
            {
                if (!ThreadPool.QueueUserWorkItem(new WaitCallback(this.CompressOne), state))
                {
                    throw new Exception("Cannot enqueue workitem");
                }
                this.currentlyFilling = -1;
                offset += num4;
            }
            else
            {
                this.currentlyFilling = currentlyFilling;
            }
            num2 -= num4;
            num += num4;
        Label_01E0:
            if (num2 > 0)
            {
                goto Label_00FA;
            }
            this.totalBytesWrittenIn += num;
        }

        public int BlockSize
        {
            get
            {
                return this.blockSize100k;
            }
        }

        public long BytesWrittenOut
        {
            get
            {
                return this.totalBytesWrittenOut;
            }
        }

        public override bool CanRead
        {
            get
            {
                return false;
            }
        }

        public override bool CanSeek
        {
            get
            {
                return false;
            }
        }

        public override bool CanWrite
        {
            get
            {
                if (this.output == null)
                {
                    throw new ObjectDisposedException("BZip2Stream");
                }
                return this.output.CanWrite;
            }
        }

        public override long Length
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public int MaxWorkers
        {
            get
            {
                return this._maxWorkers;
            }
            set
            {
                if (value < 4)
                {
                    throw new ArgumentException("MaxWorkers", "Value must be 4 or greater.");
                }
                this._maxWorkers = value;
            }
        }

        public override long Position
        {
            get
            {
                return this.totalBytesWrittenIn;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        [Flags]
        private enum TraceBits : uint
        {
            All = 0xffffffff,
            Crc = 1,
            None = 0,
            Write = 2
        }
    }
}

