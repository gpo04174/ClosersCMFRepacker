﻿namespace Ionic.BZip2
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Threading;

    public class BZip2OutputStream : Stream
    {
        private int blockSize100k;
        private BitWriter bw;
        private uint combinedCRC;
        private BZip2Compressor compressor;
        private TraceBits desiredTrace;
        private bool leaveOpen;
        private Stream output;
        private int totalBytesWrittenIn;

        public BZip2OutputStream(Stream output) : this(output, Ionic.BZip2.BZip2.MaxBlockSize, false)
        {
        }

        public BZip2OutputStream(Stream output, bool leaveOpen) : this(output, Ionic.BZip2.BZip2.MaxBlockSize, leaveOpen)
        {
        }

        public BZip2OutputStream(Stream output, int blockSize) : this(output, blockSize, false)
        {
        }

        public BZip2OutputStream(Stream output, int blockSize, bool leaveOpen)
        {
            this.desiredTrace = TraceBits.Crc | TraceBits.Write;
            if ((blockSize < Ionic.BZip2.BZip2.MinBlockSize) || (blockSize > Ionic.BZip2.BZip2.MaxBlockSize))
            {
                throw new ArgumentException(string.Format("blockSize={0} is out of range; must be between {1} and {2}", blockSize, Ionic.BZip2.BZip2.MinBlockSize, Ionic.BZip2.BZip2.MaxBlockSize), "blockSize");
            }
            this.output = output;
            if (!this.output.CanWrite)
            {
                throw new ArgumentException("The stream is not writable.", "output");
            }
            this.bw = new BitWriter(this.output);
            this.blockSize100k = blockSize;
            this.compressor = new BZip2Compressor(this.bw, blockSize);
            this.leaveOpen = leaveOpen;
            this.combinedCRC = 0;
            this.EmitHeader();
        }

        public override void Close()
        {
            if (this.output != null)
            {
                Stream output = this.output;
                this.Finish();
                if (!this.leaveOpen)
                {
                    output.Close();
                }
            }
        }

        private void EmitHeader()
        {
            byte[] buffer2 = new byte[] { 0x42, 90, 0x68, 0 };
            buffer2[3] = (byte) (0x30 + this.blockSize100k);
            byte[] buffer = buffer2;
            this.output.Write(buffer, 0, buffer.Length);
        }

        private void EmitTrailer()
        {
            this.bw.WriteByte(0x17);
            this.bw.WriteByte(0x72);
            this.bw.WriteByte(0x45);
            this.bw.WriteByte(0x38);
            this.bw.WriteByte(80);
            this.bw.WriteByte(0x90);
            this.bw.WriteInt(this.combinedCRC);
            this.bw.FinishAndPad();
        }

        private void Finish()
        {
            try
            {
                int totalBytesWrittenOut = this.bw.TotalBytesWrittenOut;
                this.compressor.CompressAndWrite();
                this.combinedCRC = (this.combinedCRC << 1) | (this.combinedCRC >> 0x1f);
                this.combinedCRC ^= this.compressor.Crc32;
                this.EmitTrailer();
            }
            finally
            {
                this.output = null;
                this.compressor = null;
                this.bw = null;
            }
        }

        public override void Flush()
        {
            if (this.output != null)
            {
                this.bw.Flush();
                this.output.Flush();
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        [Conditional("Trace")]
        private void TraceOutput(TraceBits bits, string format, params object[] varParams)
        {
            if ((bits & this.desiredTrace) != TraceBits.None)
            {
                int hashCode = Thread.CurrentThread.GetHashCode();
                Console.ForegroundColor = (ConsoleColor) ((hashCode % 8) + 10);
                Console.Write("{0:000} PBOS ", hashCode);
                Console.WriteLine(format, varParams);
                Console.ResetColor();
            }
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            if (offset < 0)
            {
                throw new IndexOutOfRangeException(string.Format("offset ({0}) must be > 0", offset));
            }
            if (count < 0)
            {
                throw new IndexOutOfRangeException(string.Format("count ({0}) must be > 0", count));
            }
            if ((offset + count) > buffer.Length)
            {
                throw new IndexOutOfRangeException(string.Format("offset({0}) count({1}) bLength({2})", offset, count, buffer.Length));
            }
            if (this.output == null)
            {
                throw new IOException("the stream is not open");
            }
            if (count != 0)
            {
                int num = 0;
                int num2 = count;
                do
                {
                    int num3 = this.compressor.Fill(buffer, offset, num2);
                    if (num3 != num2)
                    {
                        int totalBytesWrittenOut = this.bw.TotalBytesWrittenOut;
                        this.compressor.CompressAndWrite();
                        this.combinedCRC = (this.combinedCRC << 1) | (this.combinedCRC >> 0x1f);
                        this.combinedCRC ^= this.compressor.Crc32;
                        offset += num3;
                    }
                    num2 -= num3;
                    num += num3;
                }
                while (num2 > 0);
                this.totalBytesWrittenIn += num;
            }
        }

        public int BlockSize
        {
            get
            {
                return this.blockSize100k;
            }
        }

        public override bool CanRead
        {
            get
            {
                return false;
            }
        }

        public override bool CanSeek
        {
            get
            {
                return false;
            }
        }

        public override bool CanWrite
        {
            get
            {
                if (this.output == null)
                {
                    throw new ObjectDisposedException("BZip2Stream");
                }
                return this.output.CanWrite;
            }
        }

        public override long Length
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override long Position
        {
            get
            {
                return (long) this.totalBytesWrittenIn;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        [Flags]
        private enum TraceBits : uint
        {
            All = 0xffffffff,
            Crc = 1,
            None = 0,
            Write = 2
        }
    }
}

