﻿namespace Ionic.BZip2
{
    using System;

    internal static class BZip2
    {
        public static readonly int BlockSizeMultiple = 0x186a0;
        public static readonly int G_SIZE = 50;
        public static readonly int MaxAlphaSize = 0x102;
        public static readonly int MaxBlockSize = 9;
        public static readonly int MaxCodeLength = 0x17;
        public static readonly int MaxSelectors = (2 + (0xdbba0 / G_SIZE));
        public static readonly int MinBlockSize = 1;
        public static readonly int N_ITERS = 4;
        public static readonly int NGroups = 6;
        public static readonly int NUM_OVERSHOOT_BYTES = 20;
        internal static readonly int QSORT_STACK_SIZE = 0x3e8;
        public static readonly char RUNA = '\0';
        public static readonly char RUNB = '\x0001';

        internal static T[][] InitRectangularArray<T>(int d1, int d2)
        {
            T[][] localArray = new T[d1][];
            for (int i = 0; i < d1; i++)
            {
                localArray[i] = new T[d2];
            }
            return localArray;
        }
    }
}

