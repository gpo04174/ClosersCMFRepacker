﻿namespace Ionic
{
    using System;

    internal enum LogicalConjunction
    {
        NONE,
        AND,
        OR,
        XOR
    }
}

