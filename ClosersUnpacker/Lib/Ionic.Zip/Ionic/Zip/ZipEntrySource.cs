﻿namespace Ionic.Zip
{
    using System;

    public enum ZipEntrySource
    {
        None,
        FileSystem,
        Stream,
        ZipFile,
        WriteDelegate,
        JitStream,
        ZipOutputStream
    }
}

