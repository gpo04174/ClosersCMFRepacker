﻿namespace Ionic.Zip
{
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;

    public delegate void WriteDelegate(string entryName, Stream stream);
}

