﻿namespace Ionic.Zip
{
    using System;

    public enum ZipErrorAction
    {
        Throw,
        Skip,
        Retry,
        InvokeErrorEvent
    }
}

