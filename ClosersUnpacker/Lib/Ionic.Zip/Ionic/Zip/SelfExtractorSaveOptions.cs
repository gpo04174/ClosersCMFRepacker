﻿namespace Ionic.Zip
{
    using System;
    using System.Runtime.CompilerServices;

    public class SelfExtractorSaveOptions
    {
        public string AdditionalCompilerSwitches { get; set; }

        public string Copyright { get; set; }

        public string DefaultExtractDirectory { get; set; }

        public string Description { get; set; }

        public ExtractExistingFileAction ExtractExistingFile { get; set; }

        public Version FileVersion { get; set; }

        public SelfExtractorFlavor Flavor { get; set; }

        public string IconFile { get; set; }

        public string PostExtractCommandLine { get; set; }

        public string ProductName { get; set; }

        public string ProductVersion { get; set; }

        public bool Quiet { get; set; }

        public bool RemoveUnpackedFilesAfterExecute { get; set; }

        public string SfxExeWindowTitle { get; set; }
    }
}

