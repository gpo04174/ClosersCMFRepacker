﻿namespace Ionic.Zip
{
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Text;

    public class ReadOptions
    {
        public System.Text.Encoding Encoding { get; set; }

        public EventHandler<ReadProgressEventArgs> ReadProgress { get; set; }

        public TextWriter StatusMessageWriter { get; set; }
    }
}

