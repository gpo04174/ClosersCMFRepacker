﻿namespace Ionic.Zip
{
    using System;

    public enum ExtractExistingFileAction
    {
        Throw,
        OverwriteSilently,
        DoNotOverwrite,
        InvokeExtractProgressEvent
    }
}

