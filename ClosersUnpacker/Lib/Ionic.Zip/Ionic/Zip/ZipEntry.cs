﻿namespace Ionic.Zip
{
    using Ionic.BZip2;
    using Ionic.Crc;
    using Ionic.Zlib;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;

    [ComVisible(true), Guid("ebc25cf6-9120-4283-b972-0e5520d00004"), ClassInterface(ClassInterfaceType.AutoDispatch)]
    public class ZipEntry
    {
        private long __FileDataPosition = -1L;
        private Encoding _actualEncoding;
        private WinZipAesCrypto _aesCrypto_forExtract;
        private WinZipAesCrypto _aesCrypto_forWrite;
        internal Stream _archiveStream;
        private DateTime _Atime;
        internal short _BitField;
        private CloseDelegate _CloseDelegate;
        internal string _Comment;
        private byte[] _CommentBytes;
        private short _commentLength;
        internal long _CompressedFileDataSize;
        internal long _CompressedSize;
        private Ionic.Zlib.CompressionLevel _CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
        internal short _CompressionMethod = 8;
        private short _CompressionMethod_FromZipFile;
        internal ZipContainer _container;
        internal int _Crc32;
        private bool _crcCalculated;
        private DateTime _Ctime;
        private uint _diskNumber;
        private bool _emitNtfsTimes = true;
        private bool _emitUnixTimes;
        internal EncryptionAlgorithm _Encryption = EncryptionAlgorithm.None;
        internal EncryptionAlgorithm _Encryption_FromZipFile;
        private byte[] _EntryHeader;
        private bool? _entryRequiresZip64;
        private int _ExternalFileAttrs;
        internal byte[] _Extra;
        private short _extraFieldLength;
        private string _FileNameInArchive;
        private short _filenameLength;
        private long _future_ROLH;
        private Stream _inputDecryptorStream;
        internal bool _InputUsesZip64;
        private short _InternalFileAttrs;
        private bool _ioOperationCanceled;
        private bool _IsDirectory;
        private bool _IsText;
        internal DateTime _LastModified;
        private int _LengthOfHeader;
        private int _LengthOfTrailer;
        internal string _LocalFileName;
        private bool _metadataChanged;
        private DateTime _Mtime;
        private bool _ntfsTimesAreSet;
        private OpenDelegate _OpenDelegate;
        private object _outputLock = new object();
        private bool? _OutputUsesZip64;
        internal string _Password;
        private bool _presumeZip64;
        private int _readExtraDepth;
        internal long _RelativeOffsetOfLocalHeader;
        private bool _restreamRequiredOnSave;
        private bool _skippedDuringSave;
        internal ZipEntrySource _Source = ZipEntrySource.None;
        private bool _sourceIsEncrypted;
        private Stream _sourceStream;
        private long? _sourceStreamOriginalPosition;
        private bool _sourceWasJitProvided;
        internal int _TimeBlob;
        private ZipEntryTimestamp _timestamp;
        private long _TotalEntrySize;
        private bool _TrimVolumeFromFullyQualifiedPaths = true;
        internal long _UncompressedSize;
        private static DateTime _unixEpoch = new DateTime(0x7b2, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private uint _UnsupportedAlgorithmId;
        private short _VersionMadeBy;
        internal short _VersionNeeded;
        internal byte[] _WeakEncryptionHeader;
        private static DateTime _win32Epoch = DateTime.FromFileTimeUtc(0L);
        private short _WinZipAesMethod;
        private WriteDelegate _WriteDelegate;
        private static DateTime _zeroHour = new DateTime(1, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private ZipCrypto _zipCrypto_forExtract;
        private ZipCrypto _zipCrypto_forWrite;
        private static Encoding ibm437 = Encoding.GetEncoding("IBM437");

        public ZipEntry()
        {
            this.AlternateEncoding = Encoding.GetEncoding("IBM437");
            this.AlternateEncodingUsage = ZipOption.Default;
        }

        private void _CheckRead(int nbytes)
        {
            if (nbytes == 0)
            {
                throw new BadReadException(string.Format("bad read of entry {0} from compressed archive.", this.FileName));
            }
        }

        internal void _SetTimes(string fileOrDirectory, bool isFile)
        {
            try
            {
                if (this._ntfsTimesAreSet)
                {
                    if (isFile)
                    {
                        if (File.Exists(fileOrDirectory))
                        {
                            File.SetCreationTimeUtc(fileOrDirectory, this._Ctime);
                            File.SetLastAccessTimeUtc(fileOrDirectory, this._Atime);
                            File.SetLastWriteTimeUtc(fileOrDirectory, this._Mtime);
                        }
                    }
                    else if (Directory.Exists(fileOrDirectory))
                    {
                        Directory.SetCreationTimeUtc(fileOrDirectory, this._Ctime);
                        Directory.SetLastAccessTimeUtc(fileOrDirectory, this._Atime);
                        Directory.SetLastWriteTimeUtc(fileOrDirectory, this._Mtime);
                    }
                }
                else
                {
                    DateTime lastWriteTime = SharedUtilities.AdjustTime_Reverse(this.LastModified);
                    if (isFile)
                    {
                        File.SetLastWriteTime(fileOrDirectory, lastWriteTime);
                    }
                    else
                    {
                        Directory.SetLastWriteTime(fileOrDirectory, lastWriteTime);
                    }
                }
            }
            catch (IOException exception)
            {
                this.WriteStatus("failed to set time on {0}: {1}", new object[] { fileOrDirectory, exception.Message });
            }
        }

        private void _WriteEntryData(Stream s)
        {
            Stream input = null;
            long position = -1L;
            try
            {
                position = s.Position;
            }
            catch (Exception)
            {
            }
            try
            {
                Stream stream3;
                Stream stream4;
                long streamLength = this.SetInputAndFigureFileLength(ref input);
                CountingStream stream2 = new CountingStream(s);
                if (streamLength != 0L)
                {
                    stream3 = this.MaybeApplyEncryption(stream2);
                    stream4 = this.MaybeApplyCompression(stream3, streamLength);
                }
                else
                {
                    stream3 = stream4 = stream2;
                }
                CrcCalculatorStream stream = new CrcCalculatorStream(stream4, true);
                if (this._Source == ZipEntrySource.WriteDelegate)
                {
                    this._WriteDelegate(this.FileName, stream);
                }
                else
                {
                    int num3;
                    byte[] buffer = new byte[this.BufferSize];
                    while ((num3 = SharedUtilities.ReadWithRetry(input, buffer, 0, buffer.Length, this.FileName)) != 0)
                    {
                        stream.Write(buffer, 0, num3);
                        this.OnWriteBlock(stream.TotalBytesSlurped, streamLength);
                        if (this._ioOperationCanceled)
                        {
                            break;
                        }
                    }
                }
                this.FinishOutputStream(s, stream2, stream3, stream4, stream);
            }
            finally
            {
                if (this._Source == ZipEntrySource.JitStream)
                {
                    if (this._CloseDelegate != null)
                    {
                        this._CloseDelegate(this.FileName, input);
                    }
                }
                else if (input is FileStream)
                {
                    input.Dispose();
                }
            }
            if (!this._ioOperationCanceled)
            {
                this.__FileDataPosition = position;
                this.PostProcessOutput(s);
            }
        }

        private int CheckExtractExistingFile(string baseDir, string targetFileName)
        {
            int num = 0;
        Label_0003:
            switch (this.ExtractExistingFile)
            {
                case ExtractExistingFileAction.OverwriteSilently:
                    this.WriteStatus("the file {0} exists; will overwrite it...", new object[] { targetFileName });
                    return 0;

                case ExtractExistingFileAction.DoNotOverwrite:
                    this.WriteStatus("the file {0} exists; not extracting entry...", new object[] { this.FileName });
                    this.OnAfterExtract(baseDir);
                    return 1;

                case ExtractExistingFileAction.InvokeExtractProgressEvent:
                    if (num > 0)
                    {
                        throw new ZipException(string.Format("The file {0} already exists.", targetFileName));
                    }
                    this.OnExtractExisting(baseDir);
                    if (!this._ioOperationCanceled)
                    {
                        num++;
                        goto Label_0003;
                    }
                    return 2;
            }
            throw new ZipException(string.Format("The file {0} already exists.", targetFileName));
        }

        private byte[] ConstructExtraField(bool forCentralDirectory)
        {
            byte[] buffer;
            int num2;
            List<byte[]> list = new List<byte[]>();
            if (!((this._container.Zip64 == Zip64Option.Always) ? false : ((this._container.Zip64 != Zip64Option.AsNecessary) ? true : (!forCentralDirectory ? false : !this._entryRequiresZip64.Value))))
            {
                int num = 4 + (forCentralDirectory ? 0x1c : 0x10);
                buffer = new byte[num];
                num2 = 0;
                if (this._presumeZip64 || forCentralDirectory)
                {
                    buffer[num2++] = 1;
                    buffer[num2++] = 0;
                }
                else
                {
                    buffer[num2++] = 0x99;
                    buffer[num2++] = 0x99;
                }
                buffer[num2++] = (byte) (num - 4);
                buffer[num2++] = 0;
                Array.Copy(BitConverter.GetBytes(this._UncompressedSize), 0, buffer, num2, 8);
                num2 += 8;
                Array.Copy(BitConverter.GetBytes(this._CompressedSize), 0, buffer, num2, 8);
                num2 += 8;
                if (forCentralDirectory)
                {
                    Array.Copy(BitConverter.GetBytes(this._RelativeOffsetOfLocalHeader), 0, buffer, num2, 8);
                    num2 += 8;
                    Array.Copy(BitConverter.GetBytes(0), 0, buffer, num2, 4);
                }
                list.Add(buffer);
            }
            if ((this.Encryption == EncryptionAlgorithm.WinZipAes128) || (this.Encryption == EncryptionAlgorithm.WinZipAes256))
            {
                buffer = new byte[11];
                num2 = 0;
                buffer[num2++] = 1;
                buffer[num2++] = 0x99;
                buffer[num2++] = 7;
                buffer[num2++] = 0;
                buffer[num2++] = 1;
                buffer[num2++] = 0;
                buffer[num2++] = 0x41;
                buffer[num2++] = 0x45;
                int keyStrengthInBits = GetKeyStrengthInBits(this.Encryption);
                if (keyStrengthInBits == 0x80)
                {
                    buffer[num2] = 1;
                }
                else if (keyStrengthInBits == 0x100)
                {
                    buffer[num2] = 3;
                }
                else
                {
                    buffer[num2] = 0xff;
                }
                num2++;
                buffer[num2++] = (byte) (this._CompressionMethod & 0xff);
                buffer[num2++] = (byte) (this._CompressionMethod & 0xff00);
                list.Add(buffer);
            }
            if (this._ntfsTimesAreSet && this._emitNtfsTimes)
            {
                buffer = new byte[0x24];
                num2 = 0;
                buffer[num2++] = 10;
                buffer[num2++] = 0;
                buffer[num2++] = 0x20;
                buffer[num2++] = 0;
                num2 += 4;
                buffer[num2++] = 1;
                buffer[num2++] = 0;
                buffer[num2++] = 0x18;
                buffer[num2++] = 0;
                Array.Copy(BitConverter.GetBytes(this._Mtime.ToFileTime()), 0, buffer, num2, 8);
                num2 += 8;
                Array.Copy(BitConverter.GetBytes(this._Atime.ToFileTime()), 0, buffer, num2, 8);
                num2 += 8;
                Array.Copy(BitConverter.GetBytes(this._Ctime.ToFileTime()), 0, buffer, num2, 8);
                num2 += 8;
                list.Add(buffer);
            }
            if (this._ntfsTimesAreSet && this._emitUnixTimes)
            {
                int num5 = 9;
                if (!forCentralDirectory)
                {
                    num5 += 8;
                }
                buffer = new byte[num5];
                num2 = 0;
                buffer[num2++] = 0x55;
                buffer[num2++] = 0x54;
                buffer[num2++] = (byte) (num5 - 4);
                buffer[num2++] = 0;
                buffer[num2++] = 7;
                TimeSpan span = (TimeSpan) (this._Mtime - _unixEpoch);
                int totalSeconds = (int) span.TotalSeconds;
                Array.Copy(BitConverter.GetBytes(totalSeconds), 0, buffer, num2, 4);
                num2 += 4;
                if (!forCentralDirectory)
                {
                    span = (TimeSpan) (this._Atime - _unixEpoch);
                    totalSeconds = (int) span.TotalSeconds;
                    Array.Copy(BitConverter.GetBytes(totalSeconds), 0, buffer, num2, 4);
                    num2 += 4;
                    span = (TimeSpan) (this._Ctime - _unixEpoch);
                    totalSeconds = (int) span.TotalSeconds;
                    Array.Copy(BitConverter.GetBytes(totalSeconds), 0, buffer, num2, 4);
                    num2 += 4;
                }
                list.Add(buffer);
            }
            byte[] destinationArray = null;
            if (list.Count > 0)
            {
                int num7 = 0;
                int destinationIndex = 0;
                for (num2 = 0; num2 < list.Count; num2++)
                {
                    num7 += list[num2].Length;
                }
                destinationArray = new byte[num7];
                for (num2 = 0; num2 < list.Count; num2++)
                {
                    Array.Copy(list[num2], 0, destinationArray, destinationIndex, list[num2].Length);
                    destinationIndex += list[num2].Length;
                }
            }
            return destinationArray;
        }

        internal void CopyMetaData(ZipEntry source)
        {
            this.__FileDataPosition = source.__FileDataPosition;
            this.CompressionMethod = source.CompressionMethod;
            this._CompressionMethod_FromZipFile = source._CompressionMethod_FromZipFile;
            this._CompressedFileDataSize = source._CompressedFileDataSize;
            this._UncompressedSize = source._UncompressedSize;
            this._BitField = source._BitField;
            this._Source = source._Source;
            this._LastModified = source._LastModified;
            this._Mtime = source._Mtime;
            this._Atime = source._Atime;
            this._Ctime = source._Ctime;
            this._ntfsTimesAreSet = source._ntfsTimesAreSet;
            this._emitUnixTimes = source._emitUnixTimes;
            this._emitNtfsTimes = source._emitNtfsTimes;
        }

        private void CopyThroughOneEntry(Stream outStream)
        {
            if (this.LengthOfHeader == 0)
            {
                throw new BadStateException("Bad header length.");
            }
            if ((((this._metadataChanged || (this.ArchiveStream is ZipSegmentedStream)) || (outStream is ZipSegmentedStream)) || (this._InputUsesZip64 && (this._container.UseZip64WhenSaving == Zip64Option.Default))) || (!this._InputUsesZip64 && (this._container.UseZip64WhenSaving == Zip64Option.Always)))
            {
                this.CopyThroughWithRecompute(outStream);
            }
            else
            {
                this.CopyThroughWithNoChange(outStream);
            }
            this._entryRequiresZip64 = new bool?(((this._CompressedSize >= 0xffffffffL) || (this._UncompressedSize >= 0xffffffffL)) || (this._RelativeOffsetOfLocalHeader >= 0xffffffffL));
            this._OutputUsesZip64 = new bool?((this._container.Zip64 == Zip64Option.Always) ? true : this._entryRequiresZip64.Value);
        }

        private void CopyThroughWithNoChange(Stream outstream)
        {
            byte[] buffer = new byte[this.BufferSize];
            CountingStream stream = new CountingStream(this.ArchiveStream);
            stream.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
            if (this._TotalEntrySize == 0L)
            {
                this._TotalEntrySize = (this._LengthOfHeader + this._CompressedFileDataSize) + this._LengthOfTrailer;
            }
            CountingStream stream2 = outstream as CountingStream;
            this._RelativeOffsetOfLocalHeader = (stream2 != null) ? stream2.ComputedPosition : outstream.Position;
            long num2 = this._TotalEntrySize;
            while (num2 > 0L)
            {
                int count = (num2 > buffer.Length) ? buffer.Length : ((int) num2);
                int num = stream.Read(buffer, 0, count);
                outstream.Write(buffer, 0, num);
                num2 -= num;
                this.OnWriteBlock(stream.BytesRead, this._TotalEntrySize);
                if (this._ioOperationCanceled)
                {
                    break;
                }
            }
        }

        private void CopyThroughWithRecompute(Stream outstream)
        {
            byte[] buffer = new byte[this.BufferSize];
            CountingStream stream = new CountingStream(this.ArchiveStream);
            long num2 = this._RelativeOffsetOfLocalHeader;
            int lengthOfHeader = this.LengthOfHeader;
            this.WriteHeader(outstream, 0);
            this.StoreRelativeOffset();
            if (!this.FileName.EndsWith("/"))
            {
                long offset = num2 + lengthOfHeader;
                int lengthOfCryptoHeaderBytes = GetLengthOfCryptoHeaderBytes(this._Encryption_FromZipFile);
                offset -= lengthOfCryptoHeaderBytes;
                this._LengthOfHeader += lengthOfCryptoHeaderBytes;
                stream.Seek(offset, SeekOrigin.Begin);
                long num6 = this._CompressedSize;
                while (num6 > 0L)
                {
                    lengthOfCryptoHeaderBytes = (num6 > buffer.Length) ? buffer.Length : ((int) num6);
                    int count = stream.Read(buffer, 0, lengthOfCryptoHeaderBytes);
                    outstream.Write(buffer, 0, count);
                    num6 -= count;
                    this.OnWriteBlock(stream.BytesRead, this._CompressedSize);
                    if (this._ioOperationCanceled)
                    {
                        break;
                    }
                }
                if ((this._BitField & 8) == 8)
                {
                    int num7 = 0x10;
                    if (this._InputUsesZip64)
                    {
                        num7 += 8;
                    }
                    byte[] buffer2 = new byte[num7];
                    stream.Read(buffer2, 0, num7);
                    if (this._InputUsesZip64 && (this._container.UseZip64WhenSaving == Zip64Option.Default))
                    {
                        outstream.Write(buffer2, 0, 8);
                        if (this._CompressedSize > 0xffffffffL)
                        {
                            throw new InvalidOperationException("ZIP64 is required");
                        }
                        outstream.Write(buffer2, 8, 4);
                        if (this._UncompressedSize > 0xffffffffL)
                        {
                            throw new InvalidOperationException("ZIP64 is required");
                        }
                        outstream.Write(buffer2, 0x10, 4);
                        this._LengthOfTrailer -= 8;
                    }
                    else if (!(this._InputUsesZip64 || (this._container.UseZip64WhenSaving != Zip64Option.Always)))
                    {
                        byte[] buffer3 = new byte[4];
                        outstream.Write(buffer2, 0, 8);
                        outstream.Write(buffer2, 8, 4);
                        outstream.Write(buffer3, 0, 4);
                        outstream.Write(buffer2, 12, 4);
                        outstream.Write(buffer3, 0, 4);
                        this._LengthOfTrailer += 8;
                    }
                    else
                    {
                        outstream.Write(buffer2, 0, num7);
                    }
                }
            }
            this._TotalEntrySize = (this._LengthOfHeader + this._CompressedFileDataSize) + this._LengthOfTrailer;
        }

        private static ZipEntry Create(string nameInArchive, ZipEntrySource source, object arg1, object arg2)
        {
            if (string.IsNullOrEmpty(nameInArchive))
            {
                throw new ZipException("The entry name must be non-null and non-empty.");
            }
            ZipEntry entry = new ZipEntry {
                _VersionMadeBy = 0x2d,
                _Source = source
            };
            entry._Mtime = entry._Atime = entry._Ctime = DateTime.UtcNow;
            if (source == ZipEntrySource.Stream)
            {
                entry._sourceStream = arg1 as Stream;
            }
            else if (source == ZipEntrySource.WriteDelegate)
            {
                entry._WriteDelegate = arg1 as WriteDelegate;
            }
            else if (source == ZipEntrySource.JitStream)
            {
                entry._OpenDelegate = arg1 as OpenDelegate;
                entry._CloseDelegate = arg2 as CloseDelegate;
            }
            else if (source != ZipEntrySource.ZipOutputStream)
            {
                if (source == ZipEntrySource.None)
                {
                    entry._Source = ZipEntrySource.FileSystem;
                }
                else
                {
                    string str = arg1 as string;
                    if (string.IsNullOrEmpty(str))
                    {
                        throw new ZipException("The filename must be non-null and non-empty.");
                    }
                    try
                    {
                        entry._Mtime = File.GetLastWriteTime(str).ToUniversalTime();
                        entry._Ctime = File.GetCreationTime(str).ToUniversalTime();
                        entry._Atime = File.GetLastAccessTime(str).ToUniversalTime();
                        if (File.Exists(str) || Directory.Exists(str))
                        {
                            entry._ExternalFileAttrs = (int) File.GetAttributes(str);
                        }
                        entry._ntfsTimesAreSet = true;
                        entry._LocalFileName = Path.GetFullPath(str);
                    }
                    catch (PathTooLongException exception)
                    {
                        throw new ZipException(string.Format("The path is too long, filename={0}", str), exception);
                    }
                }
            }
            entry._LastModified = entry._Mtime;
            entry._FileNameInArchive = SharedUtilities.NormalizePathForUseInZipFile(nameInArchive);
            return entry;
        }

        internal static ZipEntry CreateForJitStreamProvider(string nameInArchive, OpenDelegate opener, CloseDelegate closer)
        {
            return Create(nameInArchive, ZipEntrySource.JitStream, opener, closer);
        }

        internal static ZipEntry CreateForStream(string entryName, Stream s)
        {
            return Create(entryName, ZipEntrySource.Stream, s, null);
        }

        internal static ZipEntry CreateForWriter(string entryName, WriteDelegate d)
        {
            return Create(entryName, ZipEntrySource.WriteDelegate, d, null);
        }

        internal static ZipEntry CreateForZipOutputStream(string nameInArchive)
        {
            return Create(nameInArchive, ZipEntrySource.ZipOutputStream, null, null);
        }

        internal static ZipEntry CreateFromFile(string filename, string nameInArchive)
        {
            return Create(nameInArchive, ZipEntrySource.FileSystem, filename, null);
        }

        internal static ZipEntry CreateFromNothing(string nameInArchive)
        {
            return Create(nameInArchive, ZipEntrySource.None, null, null);
        }

        public void Extract()
        {
            this.InternalExtract(".", null, null);
        }

        public void Extract(ExtractExistingFileAction extractExistingFile)
        {
            this.ExtractExistingFile = extractExistingFile;
            this.InternalExtract(".", null, null);
        }

        public void Extract(Stream stream)
        {
            this.InternalExtract(null, stream, null);
        }

        public void Extract(string baseDirectory)
        {
            this.InternalExtract(baseDirectory, null, null);
        }

        public void Extract(string baseDirectory, ExtractExistingFileAction extractExistingFile)
        {
            this.ExtractExistingFile = extractExistingFile;
            this.InternalExtract(baseDirectory, null, null);
        }

        private int ExtractOne(Stream output)
        {
            int crc = 0;
            Stream archiveStream = this.ArchiveStream;
            try
            {
                archiveStream.Seek(this.FileDataPosition, SeekOrigin.Begin);
                byte[] buffer = new byte[this.BufferSize];
                long num2 = (this._CompressionMethod_FromZipFile != 0) ? this.UncompressedSize : this._CompressedFileDataSize;
                this._inputDecryptorStream = this.GetExtractDecryptor(archiveStream);
                Stream extractDecompressor = this.GetExtractDecompressor(this._inputDecryptorStream);
                long bytesWritten = 0L;
                using (CrcCalculatorStream stream3 = new CrcCalculatorStream(extractDecompressor))
                {
                    while (num2 > 0L)
                    {
                        int count = (num2 > buffer.Length) ? buffer.Length : ((int) num2);
                        int nbytes = stream3.Read(buffer, 0, count);
                        this._CheckRead(nbytes);
                        output.Write(buffer, 0, nbytes);
                        num2 -= nbytes;
                        bytesWritten += nbytes;
                        this.OnExtractProgress(bytesWritten, this.UncompressedSize);
                        if (this._ioOperationCanceled)
                        {
                            break;
                        }
                    }
                    crc = stream3.Crc;
                }
            }
            finally
            {
                ZipSegmentedStream stream4 = archiveStream as ZipSegmentedStream;
                if (stream4 != null)
                {
                    stream4.Dispose();
                    this._archiveStream = null;
                }
            }
            return crc;
        }

        public void ExtractWithPassword(string password)
        {
            this.InternalExtract(".", null, password);
        }

        public void ExtractWithPassword(ExtractExistingFileAction extractExistingFile, string password)
        {
            this.ExtractExistingFile = extractExistingFile;
            this.InternalExtract(".", null, password);
        }

        public void ExtractWithPassword(Stream stream, string password)
        {
            this.InternalExtract(null, stream, password);
        }

        public void ExtractWithPassword(string baseDirectory, string password)
        {
            this.InternalExtract(baseDirectory, null, password);
        }

        public void ExtractWithPassword(string baseDirectory, ExtractExistingFileAction extractExistingFile, string password)
        {
            this.ExtractExistingFile = extractExistingFile;
            this.InternalExtract(baseDirectory, null, password);
        }

        private int FigureCrc32()
        {
            if (!this._crcCalculated)
            {
                Stream input = null;
                if (this._Source == ZipEntrySource.WriteDelegate)
                {
                    CrcCalculatorStream stream = new CrcCalculatorStream(Stream.Null);
                    this._WriteDelegate(this.FileName, stream);
                    this._Crc32 = stream.Crc;
                }
                else if (this._Source != ZipEntrySource.ZipFile)
                {
                    if (this._Source == ZipEntrySource.Stream)
                    {
                        this.PrepSourceStream();
                        input = this._sourceStream;
                    }
                    else if (this._Source == ZipEntrySource.JitStream)
                    {
                        if (this._sourceStream == null)
                        {
                            this._sourceStream = this._OpenDelegate(this.FileName);
                        }
                        this.PrepSourceStream();
                        input = this._sourceStream;
                    }
                    else if (this._Source != ZipEntrySource.ZipOutputStream)
                    {
                        input = File.Open(this.LocalFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    }
                    this._Crc32 = new CRC32().GetCrc32(input);
                    if (this._sourceStream == null)
                    {
                        input.Dispose();
                    }
                }
                this._crcCalculated = true;
            }
            return this._Crc32;
        }

        internal static int FindExtraFieldSegment(byte[] extra, int offx, ushort targetHeaderId)
        {
            short num3;
            for (int i = offx; (i + 3) < extra.Length; i += num3)
            {
                ushort num2 = (ushort) (extra[i++] + (extra[i++] * 0x100));
                if (num2 == targetHeaderId)
                {
                    return (i - 2);
                }
                num3 = (short) (extra[i++] + (extra[i++] * 0x100));
            }
            return -1;
        }

        internal void FinishOutputStream(Stream s, CountingStream entryCounter, Stream encryptor, Stream compressor, CrcCalculatorStream output)
        {
            if (output != null)
            {
                output.Close();
                if (compressor is DeflateStream)
                {
                    compressor.Close();
                }
                else if (compressor is BZip2OutputStream)
                {
                    compressor.Close();
                }
                else if (compressor is ParallelBZip2OutputStream)
                {
                    compressor.Close();
                }
                else if (compressor is ParallelDeflateOutputStream)
                {
                    compressor.Close();
                }
                encryptor.Flush();
                encryptor.Close();
                this._LengthOfTrailer = 0;
                this._UncompressedSize = output.TotalBytesSlurped;
                WinZipAesCipherStream stream = encryptor as WinZipAesCipherStream;
                if ((stream != null) && (this._UncompressedSize > 0L))
                {
                    s.Write(stream.FinalAuthentication, 0, 10);
                    this._LengthOfTrailer += 10;
                }
                this._CompressedFileDataSize = entryCounter.BytesWritten;
                this._CompressedSize = this._CompressedFileDataSize;
                this._Crc32 = output.Crc;
                this.StoreRelativeOffset();
            }
        }

        private byte[] GetEncodedFileNameBytes()
        {
            string s = this.NormalizeFileName();
            switch (this.AlternateEncodingUsage)
            {
                case ZipOption.Default:
                    if ((this._Comment != null) && (this._Comment.Length != 0))
                    {
                        this._CommentBytes = ibm437.GetBytes(this._Comment);
                    }
                    this._actualEncoding = ibm437;
                    return ibm437.GetBytes(s);

                case ZipOption.Always:
                    if ((this._Comment != null) && (this._Comment.Length != 0))
                    {
                        this._CommentBytes = this.AlternateEncoding.GetBytes(this._Comment);
                    }
                    this._actualEncoding = this.AlternateEncoding;
                    return this.AlternateEncoding.GetBytes(s);
            }
            byte[] bytes = ibm437.GetBytes(s);
            string str2 = ibm437.GetString(bytes, 0, bytes.Length);
            this._CommentBytes = null;
            if (str2 != s)
            {
                bytes = this.AlternateEncoding.GetBytes(s);
                if ((this._Comment != null) && (this._Comment.Length != 0))
                {
                    this._CommentBytes = this.AlternateEncoding.GetBytes(this._Comment);
                }
                this._actualEncoding = this.AlternateEncoding;
                return bytes;
            }
            this._actualEncoding = ibm437;
            if ((this._Comment != null) && (this._Comment.Length != 0))
            {
                byte[] buffer2 = ibm437.GetBytes(this._Comment);
                if (ibm437.GetString(buffer2, 0, buffer2.Length) != this.Comment)
                {
                    bytes = this.AlternateEncoding.GetBytes(s);
                    this._CommentBytes = this.AlternateEncoding.GetBytes(this._Comment);
                    this._actualEncoding = this.AlternateEncoding;
                    return bytes;
                }
                this._CommentBytes = buffer2;
            }
            return bytes;
        }

        internal Stream GetExtractDecompressor(Stream input2)
        {
            switch (this._CompressionMethod_FromZipFile)
            {
                case 0:
                    return input2;

                case 8:
                    return new DeflateStream(input2, CompressionMode.Decompress, true);

                case 12:
                    return new BZip2InputStream(input2, true);
            }
            return null;
        }

        internal Stream GetExtractDecryptor(Stream input)
        {
            if (this._Encryption_FromZipFile == EncryptionAlgorithm.PkzipWeak)
            {
                return new ZipCipherStream(input, this._zipCrypto_forExtract, CryptoMode.Decrypt);
            }
            if ((this._Encryption_FromZipFile == EncryptionAlgorithm.WinZipAes128) || (this._Encryption_FromZipFile == EncryptionAlgorithm.WinZipAes256))
            {
                return new WinZipAesCipherStream(input, this._aesCrypto_forExtract, this._CompressedFileDataSize, CryptoMode.Decrypt);
            }
            return input;
        }

        private static int GetKeyStrengthInBits(EncryptionAlgorithm a)
        {
            if (a == EncryptionAlgorithm.WinZipAes256)
            {
                return 0x100;
            }
            if (a == EncryptionAlgorithm.WinZipAes128)
            {
                return 0x80;
            }
            return -1;
        }

        internal static int GetLengthOfCryptoHeaderBytes(EncryptionAlgorithm a)
        {
            if (a == EncryptionAlgorithm.None)
            {
                return 0;
            }
            if ((a == EncryptionAlgorithm.WinZipAes128) || (a == EncryptionAlgorithm.WinZipAes256))
            {
                return (((GetKeyStrengthInBits(a) / 8) / 2) + 2);
            }
            if (a != EncryptionAlgorithm.PkzipWeak)
            {
                throw new ZipException("internal error");
            }
            return 12;
        }

        internal static void HandlePK00Prefix(Stream s)
        {
            if (SharedUtilities.ReadInt(s) != 0x30304b50)
            {
                s.Seek(-4L, SeekOrigin.Current);
            }
        }

        private static void HandleUnexpectedDataDescriptor(ZipEntry entry)
        {
            Stream archiveStream = entry.ArchiveStream;
            if (((ulong) SharedUtilities.ReadInt(archiveStream)) == (ulong)entry._Crc32)
            {
                if (SharedUtilities.ReadInt(archiveStream) == entry._CompressedSize)
                {
                    if (SharedUtilities.ReadInt(archiveStream) != entry._UncompressedSize)
                    {
                        archiveStream.Seek(-12L, SeekOrigin.Current);
                    }
                }
                else
                {
                    archiveStream.Seek(-8L, SeekOrigin.Current);
                }
            }
            else
            {
                archiveStream.Seek(-4L, SeekOrigin.Current);
            }
        }

        private void InternalExtract(string baseDir, Stream outstream, string password)
        {
            if (this._container == null)
            {
                throw new BadStateException("This entry is an orphan");
            }
            if (this._container.ZipFile == null)
            {
                throw new InvalidOperationException("Use Extract() only with ZipFile.");
            }
            this._container.ZipFile.Reset(false);
            if (this._Source != ZipEntrySource.ZipFile)
            {
                throw new BadStateException("You must call ZipFile.Save before calling any Extract method");
            }
            this.OnBeforeExtract(baseDir);
            this._ioOperationCanceled = false;
            string outFileName = null;
            Stream output = null;
            bool flag = false;
            bool flag2 = false;
            try
            {
                this.ValidateCompression();
                this.ValidateEncryption();
                if (this.ValidateOutput(baseDir, outstream, out outFileName))
                {
                    this.WriteStatus("extract dir {0}...", new object[] { outFileName });
                    this.OnAfterExtract(baseDir);
                }
                else
                {
                    if ((outFileName != null) && File.Exists(outFileName))
                    {
                        flag = true;
                        switch (this.CheckExtractExistingFile(baseDir, outFileName))
                        {
                            case 2:
                            case 1:
                                return;
                        }
                    }
                    string str2 = password ?? (this._Password ?? this._container.Password);
                    if (this._Encryption_FromZipFile != EncryptionAlgorithm.None)
                    {
                        if (str2 == null)
                        {
                            throw new BadPasswordException();
                        }
                        this.SetupCryptoForExtract(str2);
                    }
                    if (outFileName != null)
                    {
                        this.WriteStatus("extract file {0}...", new object[] { outFileName });
                        outFileName = outFileName + ".tmp";
                        string directoryName = Path.GetDirectoryName(outFileName);
                        if (!Directory.Exists(directoryName))
                        {
                            Directory.CreateDirectory(directoryName);
                        }
                        else if (this._container.ZipFile != null)
                        {
                            flag2 = this._container.ZipFile._inExtractAll;
                        }
                        output = new FileStream(outFileName, FileMode.CreateNew);
                    }
                    else
                    {
                        this.WriteStatus("extract entry {0} to stream...", new object[] { this.FileName });
                        output = outstream;
                    }
                    if (!this._ioOperationCanceled)
                    {
                        int num2 = this.ExtractOne(output);
                        if (!this._ioOperationCanceled)
                        {
                            this.VerifyCrcAfterExtract(num2);
                            if (outFileName != null)
                            {
                                output.Close();
                                output = null;
                                string sourceFileName = outFileName;
                                string destFileName = null;
                                outFileName = sourceFileName.Substring(0, sourceFileName.Length - 4);
                                if (flag)
                                {
                                    destFileName = outFileName + ".PendingOverwrite";
                                    File.Move(outFileName, destFileName);
                                }
                                File.Move(sourceFileName, outFileName);
                                this._SetTimes(outFileName, true);
                                if ((destFileName != null) && File.Exists(destFileName))
                                {
                                    ReallyDelete(destFileName);
                                }
                                if (flag2 && (this.FileName.IndexOf('/') != -1))
                                {
                                    string str6 = Path.GetDirectoryName(this.FileName);
                                    if (this._container.ZipFile[str6] == null)
                                    {
                                        this._SetTimes(Path.GetDirectoryName(outFileName), false);
                                    }
                                }
                                if (((this._VersionMadeBy & 0xff00) == 0xa00) || ((this._VersionMadeBy & 0xff00) == 0))
                                {
                                    File.SetAttributes(outFileName, (FileAttributes) this._ExternalFileAttrs);
                                }
                            }
                            this.OnAfterExtract(baseDir);
                        }
                    }
                }
            }
            catch (Exception)
            {
                this._ioOperationCanceled = true;
                throw;
            }
            finally
            {
                if (this._ioOperationCanceled && (outFileName != null))
                {
                    try
                    {
                        if (output != null)
                        {
                            output.Close();
                        }
                        if (!(!File.Exists(outFileName) || flag))
                        {
                            File.Delete(outFileName);
                        }
                    }
                    finally
                    {
                    }
                }
            }
        }

        internal CrcCalculatorStream InternalOpenReader(string password)
        {
            this.ValidateCompression();
            this.ValidateEncryption();
            this.SetupCryptoForExtract(password);
            if (this._Source != ZipEntrySource.ZipFile)
            {
                throw new BadStateException("You must call ZipFile.Save before calling OpenReader");
            }
            long length = (this._CompressionMethod_FromZipFile == 0) ? this._CompressedFileDataSize : this.UncompressedSize;
            Stream archiveStream = this.ArchiveStream;
            this.ArchiveStream.Seek(this.FileDataPosition, SeekOrigin.Begin);
            this._inputDecryptorStream = this.GetExtractDecryptor(archiveStream);
            return new CrcCalculatorStream(this.GetExtractDecompressor(this._inputDecryptorStream), length);
        }

        private static bool IsNotValidSig(int signature)
        {
            return (signature != 0x4034b50);
        }

        internal static bool IsNotValidZipDirEntrySig(int signature)
        {
            return (signature != 0x2014b50);
        }

        internal void MarkAsDirectory()
        {
            this._IsDirectory = true;
            if (!this._FileNameInArchive.EndsWith("/"))
            {
                this._FileNameInArchive = this._FileNameInArchive + "/";
            }
        }

        private Stream MaybeApplyCompression(Stream s, long streamLength)
        {
            if ((this._CompressionMethod == 8) && (this.CompressionLevel != Ionic.Zlib.CompressionLevel.None))
            {
                if ((this._container.ParallelDeflateThreshold == 0L) || ((streamLength > this._container.ParallelDeflateThreshold) && (this._container.ParallelDeflateThreshold > 0L)))
                {
                    if (this._container.ParallelDeflater == null)
                    {
                        this._container.ParallelDeflater = new ParallelDeflateOutputStream(s, this.CompressionLevel, this._container.Strategy, true);
                        if (this._container.CodecBufferSize > 0)
                        {
                            this._container.ParallelDeflater.BufferSize = this._container.CodecBufferSize;
                        }
                        if (this._container.ParallelDeflateMaxBufferPairs > 0)
                        {
                            this._container.ParallelDeflater.MaxBufferPairs = this._container.ParallelDeflateMaxBufferPairs;
                        }
                    }
                    ParallelDeflateOutputStream parallelDeflater = this._container.ParallelDeflater;
                    parallelDeflater.Reset(s);
                    return parallelDeflater;
                }
                DeflateStream stream2 = new DeflateStream(s, CompressionMode.Compress, this.CompressionLevel, true);
                if (this._container.CodecBufferSize > 0)
                {
                    stream2.BufferSize = this._container.CodecBufferSize;
                }
                stream2.Strategy = this._container.Strategy;
                return stream2;
            }
            if (this._CompressionMethod == 12)
            {
                if ((this._container.ParallelDeflateThreshold == 0L) || ((streamLength > this._container.ParallelDeflateThreshold) && (this._container.ParallelDeflateThreshold > 0L)))
                {
                    return new ParallelBZip2OutputStream(s, true);
                }
                return new BZip2OutputStream(s, true);
            }
            return s;
        }

        private Stream MaybeApplyEncryption(Stream s)
        {
            if (this.Encryption == EncryptionAlgorithm.PkzipWeak)
            {
                return new ZipCipherStream(s, this._zipCrypto_forWrite, CryptoMode.Encrypt);
            }
            if ((this.Encryption == EncryptionAlgorithm.WinZipAes128) || (this.Encryption == EncryptionAlgorithm.WinZipAes256))
            {
                return new WinZipAesCipherStream(s, this._aesCrypto_forWrite, CryptoMode.Encrypt);
            }
            return s;
        }

        private void MaybeUnsetCompressionMethodForWriting(int cycle)
        {
            if (cycle > 1)
            {
                this._CompressionMethod = 0;
            }
            else if (this.IsDirectory)
            {
                this._CompressionMethod = 0;
            }
            else if (this._Source != ZipEntrySource.ZipFile)
            {
                if (this._Source == ZipEntrySource.Stream)
                {
                    if (((this._sourceStream != null) && this._sourceStream.CanSeek) && (this._sourceStream.Length == 0L))
                    {
                        this._CompressionMethod = 0;
                        return;
                    }
                }
                else if ((this._Source == ZipEntrySource.FileSystem) && (SharedUtilities.GetFileLength(this.LocalFileName) == 0L))
                {
                    this._CompressionMethod = 0;
                    return;
                }
                if (this.SetCompression != null)
                {
                    this.CompressionLevel = this.SetCompression(this.LocalFileName, this._FileNameInArchive);
                }
                if ((this.CompressionLevel == Ionic.Zlib.CompressionLevel.None) && (this.CompressionMethod == Ionic.Zip.CompressionMethod.Deflate))
                {
                    this._CompressionMethod = 0;
                }
            }
        }

        internal static string NameInArchive(string filename, string directoryPathInArchive)
        {
            string pathName = null;
            if (directoryPathInArchive == null)
            {
                pathName = filename;
            }
            else if (string.IsNullOrEmpty(directoryPathInArchive))
            {
                pathName = Path.GetFileName(filename);
            }
            else
            {
                pathName = Path.Combine(directoryPathInArchive, Path.GetFileName(filename));
            }
            return SharedUtilities.NormalizePathForUseInZipFile(pathName);
        }

        private string NormalizeFileName()
        {
            string str = this.FileName.Replace(@"\", "/");
            if (((this._TrimVolumeFromFullyQualifiedPaths && (this.FileName.Length >= 3)) && (this.FileName[1] == ':')) && (str[2] == '/'))
            {
                return str.Substring(3);
            }
            if ((this.FileName.Length >= 4) && ((str[0] == '/') && (str[1] == '/')))
            {
                int index = str.IndexOf('/', 2);
                if (index == -1)
                {
                    throw new ArgumentException("The path for that entry appears to be badly formatted");
                }
                return str.Substring(index + 1);
            }
            if ((this.FileName.Length >= 3) && ((str[0] == '.') && (str[1] == '/')))
            {
                return str.Substring(2);
            }
            return str;
        }

        internal void NotifySaveComplete()
        {
            this._Encryption_FromZipFile = this._Encryption;
            this._CompressionMethod_FromZipFile = this._CompressionMethod;
            this._restreamRequiredOnSave = false;
            this._metadataChanged = false;
            this._Source = ZipEntrySource.ZipFile;
        }

        private void OnAfterExtract(string path)
        {
            if ((this._container.ZipFile != null) && !this._container.ZipFile._inExtractAll)
            {
                this._container.ZipFile.OnSingleEntryExtract(this, path, false);
            }
        }

        private void OnBeforeExtract(string path)
        {
            if ((this._container.ZipFile != null) && !this._container.ZipFile._inExtractAll)
            {
                this._ioOperationCanceled = this._container.ZipFile.OnSingleEntryExtract(this, path, true);
            }
        }

        private void OnExtractExisting(string path)
        {
            if (this._container.ZipFile != null)
            {
                this._ioOperationCanceled = this._container.ZipFile.OnExtractExisting(this, path);
            }
        }

        private void OnExtractProgress(long bytesWritten, long totalBytesToWrite)
        {
            if (this._container.ZipFile != null)
            {
                this._ioOperationCanceled = this._container.ZipFile.OnExtractBlock(this, bytesWritten, totalBytesToWrite);
            }
        }

        private void OnWriteBlock(long bytesXferred, long totalBytesToXfer)
        {
            if (this._container.ZipFile != null)
            {
                this._ioOperationCanceled = this._container.ZipFile.OnSaveBlock(this, bytesXferred, totalBytesToXfer);
            }
        }

        private void OnZipErrorWhileSaving(Exception e)
        {
            if (this._container.ZipFile != null)
            {
                this._ioOperationCanceled = this._container.ZipFile.OnZipErrorSaving(this, e);
            }
        }

        public CrcCalculatorStream OpenReader()
        {
            if (this._container.ZipFile == null)
            {
                throw new InvalidOperationException("Use OpenReader() only with ZipFile.");
            }
            return this.InternalOpenReader(this._Password ?? this._container.Password);
        }

        public CrcCalculatorStream OpenReader(string password)
        {
            if (this._container.ZipFile == null)
            {
                throw new InvalidOperationException("Use OpenReader() only with ZipFile.");
            }
            return this.InternalOpenReader(password);
        }

        internal void PostProcessOutput(Stream s)
        {
            int num2;
            short num9;
            CountingStream stream = s as CountingStream;
            if ((this._UncompressedSize == 0L) && (this._CompressedSize == 0L))
            {
                if (this._Source == ZipEntrySource.ZipOutputStream)
                {
                    return;
                }
                if (this._Password != null)
                {
                    int num = 0;
                    if (this.Encryption == EncryptionAlgorithm.PkzipWeak)
                    {
                        num = 12;
                    }
                    else if ((this.Encryption == EncryptionAlgorithm.WinZipAes128) || (this.Encryption == EncryptionAlgorithm.WinZipAes256))
                    {
                        num = this._aesCrypto_forWrite._Salt.Length + this._aesCrypto_forWrite.GeneratedPV.Length;
                    }
                    if (!((this._Source != ZipEntrySource.ZipOutputStream) || s.CanSeek))
                    {
                        throw new ZipException("Zero bytes written, encryption in use, and non-seekable output.");
                    }
                    if (this.Encryption != EncryptionAlgorithm.None)
                    {
                        s.Seek((long) (-1 * num), SeekOrigin.Current);
                        s.SetLength(s.Position);
                        if (stream != null)
                        {
                            stream.Adjust((long) num);
                        }
                        this._LengthOfHeader -= num;
                        this.__FileDataPosition -= num;
                    }
                    this._Password = null;
                    this._BitField = (short) (this._BitField & -2);
                    num2 = 6;
                    this._EntryHeader[num2++] = (byte) (this._BitField & 0xff);
                    this._EntryHeader[num2++] = (byte) ((this._BitField & 0xff00) >> 8);
                    if ((this.Encryption == EncryptionAlgorithm.WinZipAes128) || (this.Encryption == EncryptionAlgorithm.WinZipAes256))
                    {
                        short num3 = (short) (this._EntryHeader[0x1a] + (this._EntryHeader[0x1b] * 0x100));
                        int offx = 30 + num3;
                        int num5 = FindExtraFieldSegment(this._EntryHeader, offx, 0x9901);
                        if (num5 >= 0)
                        {
                            this._EntryHeader[num5++] = 0x99;
                            this._EntryHeader[num5++] = 0x99;
                        }
                    }
                }
                this.CompressionMethod = Ionic.Zip.CompressionMethod.None;
                this.Encryption = EncryptionAlgorithm.None;
            }
            else if ((this._zipCrypto_forWrite != null) || (this._aesCrypto_forWrite != null))
            {
                if (this.Encryption == EncryptionAlgorithm.PkzipWeak)
                {
                    this._CompressedSize += 12L;
                }
                else if ((this.Encryption == EncryptionAlgorithm.WinZipAes128) || (this.Encryption == EncryptionAlgorithm.WinZipAes256))
                {
                    this._CompressedSize += this._aesCrypto_forWrite.SizeOfEncryptionMetadata;
                }
            }
            int destinationIndex = 8;
            this._EntryHeader[destinationIndex++] = (byte) (this._CompressionMethod & 0xff);
            this._EntryHeader[destinationIndex++] = (byte) ((this._CompressionMethod & 0xff00) >> 8);
            destinationIndex = 14;
            this._EntryHeader[destinationIndex++] = (byte) (this._Crc32 & 0xff);
            this._EntryHeader[destinationIndex++] = (byte) ((this._Crc32 & 0xff00) >> 8);
            this._EntryHeader[destinationIndex++] = (byte) ((this._Crc32 & 0xff0000) >> 0x10);
            this._EntryHeader[destinationIndex++] = (byte) ((this._Crc32 & 0xff000000L) >> 0x18);
            this.SetZip64Flags();
            short num7 = (short) (this._EntryHeader[0x1a] + (this._EntryHeader[0x1b] * 0x100));
            short num8 = (short) (this._EntryHeader[0x1c] + (this._EntryHeader[0x1d] * 0x100));
            if (this._OutputUsesZip64.Value)
            {
                this._EntryHeader[4] = 0x2d;
                this._EntryHeader[5] = 0;
                for (num2 = 0; num2 < 8; num2++)
                {
                    this._EntryHeader[destinationIndex++] = 0xff;
                }
                destinationIndex = 30 + num7;
                this._EntryHeader[destinationIndex++] = 1;
                this._EntryHeader[destinationIndex++] = 0;
                destinationIndex += 2;
                Array.Copy(BitConverter.GetBytes(this._UncompressedSize), 0, this._EntryHeader, destinationIndex, 8);
                destinationIndex += 8;
                Array.Copy(BitConverter.GetBytes(this._CompressedSize), 0, this._EntryHeader, destinationIndex, 8);
            }
            else
            {
                this._EntryHeader[4] = 20;
                this._EntryHeader[5] = 0;
                destinationIndex = 0x12;
                this._EntryHeader[destinationIndex++] = (byte) (this._CompressedSize & 0xffL);
                this._EntryHeader[destinationIndex++] = (byte) ((this._CompressedSize & 0xff00L) >> 8);
                this._EntryHeader[destinationIndex++] = (byte) ((this._CompressedSize & 0xff0000L) >> 0x10);
                this._EntryHeader[destinationIndex++] = (byte) ((this._CompressedSize & 0xff000000L) >> 0x18);
                this._EntryHeader[destinationIndex++] = (byte) (this._UncompressedSize & 0xffL);
                this._EntryHeader[destinationIndex++] = (byte) ((this._UncompressedSize & 0xff00L) >> 8);
                this._EntryHeader[destinationIndex++] = (byte) ((this._UncompressedSize & 0xff0000L) >> 0x10);
                this._EntryHeader[destinationIndex++] = (byte) ((this._UncompressedSize & 0xff000000L) >> 0x18);
                if (num8 != 0)
                {
                    destinationIndex = 30 + num7;
                    num9 = (short) (this._EntryHeader[destinationIndex + 2] + (this._EntryHeader[destinationIndex + 3] * 0x100));
                    if (num9 == 0x10)
                    {
                        this._EntryHeader[destinationIndex++] = 0x99;
                        this._EntryHeader[destinationIndex++] = 0x99;
                    }
                }
            }
            if ((this.Encryption == EncryptionAlgorithm.WinZipAes128) || (this.Encryption == EncryptionAlgorithm.WinZipAes256))
            {
                destinationIndex = 8;
                this._EntryHeader[destinationIndex++] = 0x63;
                this._EntryHeader[destinationIndex++] = 0;
                destinationIndex = 30 + num7;
                do
                {
                    ushort num10 = (ushort) (this._EntryHeader[destinationIndex] + (this._EntryHeader[destinationIndex + 1] * 0x100));
                    num9 = (short) (this._EntryHeader[destinationIndex + 2] + (this._EntryHeader[destinationIndex + 3] * 0x100));
                    if (num10 != 0x9901)
                    {
                        destinationIndex += num9 + 4;
                    }
                    else
                    {
                        destinationIndex += 9;
                        this._EntryHeader[destinationIndex++] = (byte) (this._CompressionMethod & 0xff);
                        this._EntryHeader[destinationIndex++] = (byte) (this._CompressionMethod & 0xff00);
                    }
                }
                while (destinationIndex < ((num8 - 30) - num7));
            }
            if (((this._BitField & 8) != 8) || ((this._Source == ZipEntrySource.ZipOutputStream) && s.CanSeek))
            {
                ZipSegmentedStream stream2 = s as ZipSegmentedStream;
                if ((stream2 != null) && (this._diskNumber != stream2.CurrentSegment))
                {
                    using (Stream stream3 = ZipSegmentedStream.ForUpdate(this._container.ZipFile.Name, this._diskNumber))
                    {
                        stream3.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
                        stream3.Write(this._EntryHeader, 0, this._EntryHeader.Length);
                    }
                }
                else
                {
                    s.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
                    s.Write(this._EntryHeader, 0, this._EntryHeader.Length);
                    if (stream != null)
                    {
                        stream.Adjust((long) this._EntryHeader.Length);
                    }
                    s.Seek(this._CompressedSize, SeekOrigin.Current);
                }
            }
            if (((this._BitField & 8) == 8) && !this.IsDirectory)
            {
                byte[] destinationArray = new byte[0x10 + (this._OutputUsesZip64.Value ? 8 : 0)];
                destinationIndex = 0;
                Array.Copy(BitConverter.GetBytes(0x8074b50), 0, destinationArray, destinationIndex, 4);
                destinationIndex += 4;
                Array.Copy(BitConverter.GetBytes(this._Crc32), 0, destinationArray, destinationIndex, 4);
                destinationIndex += 4;
                if (this._OutputUsesZip64.Value)
                {
                    Array.Copy(BitConverter.GetBytes(this._CompressedSize), 0, destinationArray, destinationIndex, 8);
                    destinationIndex += 8;
                    Array.Copy(BitConverter.GetBytes(this._UncompressedSize), 0, destinationArray, destinationIndex, 8);
                    destinationIndex += 8;
                }
                else
                {
                    destinationArray[destinationIndex++] = (byte) (this._CompressedSize & 0xffL);
                    destinationArray[destinationIndex++] = (byte) ((this._CompressedSize & 0xff00L) >> 8);
                    destinationArray[destinationIndex++] = (byte) ((this._CompressedSize & 0xff0000L) >> 0x10);
                    destinationArray[destinationIndex++] = (byte) ((this._CompressedSize & 0xff000000L) >> 0x18);
                    destinationArray[destinationIndex++] = (byte) (this._UncompressedSize & 0xffL);
                    destinationArray[destinationIndex++] = (byte) ((this._UncompressedSize & 0xff00L) >> 8);
                    destinationArray[destinationIndex++] = (byte) ((this._UncompressedSize & 0xff0000L) >> 0x10);
                    destinationArray[destinationIndex++] = (byte) ((this._UncompressedSize & 0xff000000L) >> 0x18);
                }
                s.Write(destinationArray, 0, destinationArray.Length);
                this._LengthOfTrailer += destinationArray.Length;
            }
        }

        internal void PrepOutputStream(Stream s, long streamLength, out CountingStream outputCounter, out Stream encryptor, out Stream compressor, out CrcCalculatorStream output)
        {
            outputCounter = new CountingStream(s);
            if (streamLength != 0L)
            {
                encryptor = this.MaybeApplyEncryption(outputCounter);
                compressor = this.MaybeApplyCompression(encryptor, streamLength);
            }
            else
            {
                encryptor = compressor = outputCounter;
            }
            output = new CrcCalculatorStream(compressor, true);
        }

        private void PrepSourceStream()
        {
            if (this._sourceStream == null)
            {
                throw new ZipException(string.Format("The input stream is null for entry '{0}'.", this.FileName));
            }
            if (this._sourceStreamOriginalPosition.HasValue)
            {
                this._sourceStream.Position = this._sourceStreamOriginalPosition.Value;
            }
            else if (this._sourceStream.CanSeek)
            {
                this._sourceStreamOriginalPosition = new long?(this._sourceStream.Position);
            }
            else if ((this.Encryption == EncryptionAlgorithm.PkzipWeak) && ((this._Source != ZipEntrySource.ZipFile) && ((this._BitField & 8) != 8)))
            {
                throw new ZipException("It is not possible to use PKZIP encryption on a non-seekable input stream");
            }
        }

        internal int ProcessExtraField(Stream s, short extraFieldLength)
        {
            int num = 0;
            if (extraFieldLength > 0)
            {
                int num4;
                short num6;
                byte[] buffer = this._Extra = new byte[extraFieldLength];
                num = s.Read(buffer, 0, buffer.Length);
                long posn = s.Position - num;
                for (int i = 0; (i + 3) < buffer.Length; i = (num4 + num6) + 4)
                {
                    num4 = i;
                    ushort num5 = (ushort) (buffer[i++] + (buffer[i++] * 0x100));
                    num6 = (short) (buffer[i++] + (buffer[i++] * 0x100));
                    ushort num8 = num5;
                    if (num8 <= 0x5455)
                    {
                        switch (num8)
                        {
                            case 0x17:
                                i = this.ProcessExtraFieldPkwareStrongEncryption(buffer, i);
                                break;

                            case 0x5455:
                                i = this.ProcessExtraFieldUnixTimes(buffer, i, num6, posn);
                                break;

                            case 1:
                                goto Label_0113;

                            case 10:
                                goto Label_00E5;
                        }
                    }
                    else if (num8 <= 0x7855)
                    {
                        switch (num8)
                        {
                            case 0x5855:
                                goto Label_0101;
                        }
                    }
                    else if ((num8 != 0x7875) && (num8 == 0x9901))
                    {
                        goto Label_0121;
                    }
                    continue;
                Label_00E5:
                    i = this.ProcessExtraFieldWindowsTimes(buffer, i, num6, posn);
                    continue;
                Label_0101:
                    i = this.ProcessExtraFieldInfoZipTimes(buffer, i, num6, posn);
                    continue;
                Label_0113:
                    i = this.ProcessExtraFieldZip64(buffer, i, num6, posn);
                    continue;
                Label_0121:
                    i = this.ProcessExtraFieldWinZipAes(buffer, i, num6, posn);
                }
            }
            return num;
        }

        private int ProcessExtraFieldInfoZipTimes(byte[] buffer, int j, short dataSize, long posn)
        {
            if ((dataSize != 12) && (dataSize != 8))
            {
                throw new BadReadException(string.Format("  Unexpected size (0x{0:X4}) for InfoZip v1 extra field at position 0x{1:X16}", dataSize, posn));
            }
            int num = BitConverter.ToInt32(buffer, j);
            this._Mtime = _unixEpoch.AddSeconds((double) num);
            j += 4;
            num = BitConverter.ToInt32(buffer, j);
            this._Atime = _unixEpoch.AddSeconds((double) num);
            j += 4;
            this._Ctime = DateTime.UtcNow;
            this._ntfsTimesAreSet = true;
            this._timestamp |= ZipEntryTimestamp.InfoZip1;
            return j;
        }

        private int ProcessExtraFieldPkwareStrongEncryption(byte[] Buffer, int j)
        {
            j += 2;
            this._UnsupportedAlgorithmId = (ushort) (Buffer[j++] + (Buffer[j++] * 0x100));
            this._Encryption_FromZipFile = this._Encryption = EncryptionAlgorithm.Unsupported;
            return j;
        }

        private int ProcessExtraFieldUnixTimes(byte[] buffer, int j, short dataSize, long posn)
        {
            if (((dataSize != 13) && (dataSize != 9)) && (dataSize != 5))
            {
                throw new BadReadException(string.Format("  Unexpected size (0x{0:X4}) for Extended Timestamp extra field at position 0x{1:X16}", dataSize, posn));
            }
            int remainingData = dataSize;
            Func<DateTime> func = delegate {
                int num = BitConverter.ToInt32(buffer, j);
                j += 4;
                remainingData -= 4;
                return _unixEpoch.AddSeconds((double) num);
            };
            if ((dataSize == 13) || (this._readExtraDepth > 0))
            {
                byte num = buffer[j++];
                remainingData--;
                if (((num & 1) != 0) && (remainingData >= 4))
                {
                    this._Mtime = func();
                }
                this._Atime = (((num & 2) != 0) && (remainingData >= 4)) ? func() : DateTime.UtcNow;
                this._Ctime = (((num & 4) != 0) && (remainingData >= 4)) ? func() : DateTime.UtcNow;
                this._timestamp |= ZipEntryTimestamp.Unix;
                this._ntfsTimesAreSet = true;
                this._emitUnixTimes = true;
            }
            else
            {
                this.ReadExtraField();
            }
            return j;
        }

        private int ProcessExtraFieldWindowsTimes(byte[] buffer, int j, short dataSize, long posn)
        {
            if (dataSize != 0x20)
            {
                throw new BadReadException(string.Format("  Unexpected size (0x{0:X4}) for NTFS times extra field at position 0x{1:X16}", dataSize, posn));
            }
            j += 4;
            short num = (short) (buffer[j] + (buffer[j + 1] * 0x100));
            short num2 = (short) (buffer[j + 2] + (buffer[j + 3] * 0x100));
            j += 4;
            if ((num == 1) && (num2 == 0x18))
            {
                long fileTime = BitConverter.ToInt64(buffer, j);
                this._Mtime = DateTime.FromFileTimeUtc(fileTime);
                j += 8;
                fileTime = BitConverter.ToInt64(buffer, j);
                this._Atime = DateTime.FromFileTimeUtc(fileTime);
                j += 8;
                fileTime = BitConverter.ToInt64(buffer, j);
                this._Ctime = DateTime.FromFileTimeUtc(fileTime);
                j += 8;
                this._ntfsTimesAreSet = true;
                this._timestamp |= ZipEntryTimestamp.Windows;
                this._emitNtfsTimes = true;
            }
            return j;
        }

        private int ProcessExtraFieldWinZipAes(byte[] buffer, int j, short dataSize, long posn)
        {
            if (this._CompressionMethod == 0x63)
            {
                if ((this._BitField & 1) != 1)
                {
                    throw new BadReadException(string.Format("  Inconsistent metadata at position 0x{0:X16}", posn));
                }
                this._sourceIsEncrypted = true;
                if (dataSize != 7)
                {
                    throw new BadReadException(string.Format("  Inconsistent size (0x{0:X4}) in WinZip AES field at position 0x{1:X16}", dataSize, posn));
                }
                this._WinZipAesMethod = BitConverter.ToInt16(buffer, j);
                j += 2;
                if ((this._WinZipAesMethod != 1) && (this._WinZipAesMethod != 2))
                {
                    throw new BadReadException(string.Format("  Unexpected vendor version number (0x{0:X4}) for WinZip AES metadata at position 0x{1:X16}", this._WinZipAesMethod, posn));
                }
                short num = BitConverter.ToInt16(buffer, j);
                j += 2;
                if (num != 0x4541)
                {
                    throw new BadReadException(string.Format("  Unexpected vendor ID (0x{0:X4}) for WinZip AES metadata at position 0x{1:X16}", num, posn));
                }
                int num2 = (buffer[j] == 1) ? 0x80 : ((buffer[j] == 3) ? 0x100 : -1);
                if (num2 < 0)
                {
                    throw new BadReadException(string.Format("Invalid key strength ({0})", num2));
                }
                this._Encryption_FromZipFile = this._Encryption = (num2 == 0x80) ? EncryptionAlgorithm.WinZipAes128 : EncryptionAlgorithm.WinZipAes256;
                j++;
                this._CompressionMethod_FromZipFile = this._CompressionMethod = BitConverter.ToInt16(buffer, j);
                j += 2;
            }
            return j;
        }

        private int ProcessExtraFieldZip64(byte[] buffer, int j, short dataSize, long posn)
        {
            this._InputUsesZip64 = true;
            if (dataSize > 0x1c)
            {
                throw new BadReadException(string.Format("  Inconsistent size (0x{0:X4}) for ZIP64 extra field at position 0x{1:X16}", dataSize, posn));
            }
            int remainingData = dataSize;
            Func<long> func = delegate {
                if (remainingData < 8)
                {
                    throw new BadReadException(string.Format("  Missing data for ZIP64 extra field, position 0x{0:X16}", posn));
                }
                long num = BitConverter.ToInt64(buffer, j);
                j += 8;
                remainingData -= 8;
                return num;
            };
            if (this._UncompressedSize == 0xffffffffL)
            {
                this._UncompressedSize = func();
            }
            if (this._CompressedSize == 0xffffffffL)
            {
                this._CompressedSize = func();
            }
            if (this._RelativeOffsetOfLocalHeader == 0xffffffffL)
            {
                this._RelativeOffsetOfLocalHeader = func();
            }
            return j;
        }

        internal static ZipEntry ReadDirEntry(ZipFile zf, Dictionary<string, object> previouslySeen)
        {
            Stream readStream = zf.ReadStream;
            Encoding encoding = (zf.AlternateEncodingUsage == ZipOption.Always) ? zf.AlternateEncoding : ZipFile.DefaultEncoding;
            int signature = SharedUtilities.ReadSignature(readStream);
            if (IsNotValidZipDirEntrySig(signature))
            {
                readStream.Seek(-4L, SeekOrigin.Current);
                if (((signature != 0x6054b50L) && (signature != 0x6064b50L)) && (signature != 0x4034b50))
                {
                    throw new BadReadException(string.Format("  Bad signature (0x{0:X8}) at position 0x{1:X8}", signature, readStream.Position));
                }
                return null;
            }
            int num2 = 0x2e;
            byte[] buffer = new byte[0x2a];
            if (readStream.Read(buffer, 0, buffer.Length) != buffer.Length)
            {
                return null;
            }
            int num4 = 0;
            ZipEntry entry = new ZipEntry {
                AlternateEncoding = encoding,
                _Source = ZipEntrySource.ZipFile,
                _container = new ZipContainer(zf),
                _VersionMadeBy = (short) (buffer[num4++] + (buffer[num4++] * 0x100)),
                _VersionNeeded = (short) (buffer[num4++] + (buffer[num4++] * 0x100)),
                _BitField = (short) (buffer[num4++] + (buffer[num4++] * 0x100)),
                _CompressionMethod = (short) (buffer[num4++] + (buffer[num4++] * 0x100)),
                _TimeBlob = ((buffer[num4++] + (buffer[num4++] * 0x100)) + ((buffer[num4++] * 0x100) * 0x100)) + (((buffer[num4++] * 0x100) * 0x100) * 0x100)
            };
            entry._LastModified = SharedUtilities.PackedToDateTime(entry._TimeBlob);
            entry._timestamp |= ZipEntryTimestamp.DOS;
            entry._Crc32 = ((buffer[num4++] + (buffer[num4++] * 0x100)) + ((buffer[num4++] * 0x100) * 0x100)) + (((buffer[num4++] * 0x100) * 0x100) * 0x100);
            entry._CompressedSize = (long) ((ulong) (((buffer[num4++] + (buffer[num4++] * 0x100)) + ((buffer[num4++] * 0x100) * 0x100)) + (((buffer[num4++] * 0x100) * 0x100) * 0x100)));
            entry._UncompressedSize = (long) ((ulong) (((buffer[num4++] + (buffer[num4++] * 0x100)) + ((buffer[num4++] * 0x100) * 0x100)) + (((buffer[num4++] * 0x100) * 0x100) * 0x100)));
            entry._CompressionMethod_FromZipFile = entry._CompressionMethod;
            entry._filenameLength = (short) (buffer[num4++] + (buffer[num4++] * 0x100));
            entry._extraFieldLength = (short) (buffer[num4++] + (buffer[num4++] * 0x100));
            entry._commentLength = (short) (buffer[num4++] + (buffer[num4++] * 0x100));
            entry._diskNumber = (uint) (buffer[num4++] + (buffer[num4++] * 0x100));
            entry._InternalFileAttrs = (short) (buffer[num4++] + (buffer[num4++] * 0x100));
            entry._ExternalFileAttrs = ((buffer[num4++] + (buffer[num4++] * 0x100)) + ((buffer[num4++] * 0x100) * 0x100)) + (((buffer[num4++] * 0x100) * 0x100) * 0x100);
            entry._RelativeOffsetOfLocalHeader = (long) ((ulong) (((buffer[num4++] + (buffer[num4++] * 0x100)) + ((buffer[num4++] * 0x100) * 0x100)) + (((buffer[num4++] * 0x100) * 0x100) * 0x100)));
            entry.IsText = (entry._InternalFileAttrs & 1) == 1;
            buffer = new byte[entry._filenameLength];
            int num3 = readStream.Read(buffer, 0, buffer.Length);
            num2 += num3;
            if ((entry._BitField & 0x800) == 0x800)
            {
                entry._FileNameInArchive = SharedUtilities.Utf8StringFromBuffer(buffer);
            }
            else
            {
                entry._FileNameInArchive = SharedUtilities.StringFromBuffer(buffer, encoding);
            }
            while (previouslySeen.ContainsKey(entry._FileNameInArchive))
            {
                entry._FileNameInArchive = CopyHelper.AppendCopyToFileName(entry._FileNameInArchive);
                entry._metadataChanged = true;
            }
            if (entry.AttributesIndicateDirectory)
            {
                entry.MarkAsDirectory();
            }
            else if (entry._FileNameInArchive.EndsWith("/"))
            {
                entry.MarkAsDirectory();
            }
            entry._CompressedFileDataSize = entry._CompressedSize;
            if ((entry._BitField & 1) == 1)
            {
                entry._Encryption_FromZipFile = entry._Encryption = EncryptionAlgorithm.PkzipWeak;
                entry._sourceIsEncrypted = true;
            }
            if (entry._extraFieldLength > 0)
            {
                entry._InputUsesZip64 = ((entry._CompressedSize == 0xffffffffL) || (entry._UncompressedSize == 0xffffffffL)) || (entry._RelativeOffsetOfLocalHeader == 0xffffffffL);
                num2 += entry.ProcessExtraField(readStream, entry._extraFieldLength);
                entry._CompressedFileDataSize = entry._CompressedSize;
            }
            if (entry._Encryption == EncryptionAlgorithm.PkzipWeak)
            {
                entry._CompressedFileDataSize -= 12L;
            }
            else if ((entry.Encryption == EncryptionAlgorithm.WinZipAes128) || (entry.Encryption == EncryptionAlgorithm.WinZipAes256))
            {
                entry._CompressedFileDataSize = entry.CompressedSize - (GetLengthOfCryptoHeaderBytes(entry.Encryption) + 10);
                entry._LengthOfTrailer = 10;
            }
            if ((entry._BitField & 8) == 8)
            {
                if (entry._InputUsesZip64)
                {
                    entry._LengthOfTrailer += 0x18;
                }
                else
                {
                    entry._LengthOfTrailer += 0x10;
                }
            }
            entry.AlternateEncoding = ((entry._BitField & 0x800) == 0x800) ? Encoding.UTF8 : encoding;
            entry.AlternateEncodingUsage = ZipOption.Always;
            if (entry._commentLength > 0)
            {
                buffer = new byte[entry._commentLength];
                num3 = readStream.Read(buffer, 0, buffer.Length);
                num2 += num3;
                if ((entry._BitField & 0x800) == 0x800)
                {
                    entry._Comment = SharedUtilities.Utf8StringFromBuffer(buffer);
                }
                else
                {
                    entry._Comment = SharedUtilities.StringFromBuffer(buffer, encoding);
                }
            }
            return entry;
        }

        internal static ZipEntry ReadEntry(ZipContainer zc, bool first)
        {
            ZipFile zipFile = zc.ZipFile;
            Stream readStream = zc.ReadStream;
            Encoding alternateEncoding = zc.AlternateEncoding;
            ZipEntry ze = new ZipEntry {
                _Source = ZipEntrySource.ZipFile,
                _container = zc,
                _archiveStream = readStream
            };
            if (zipFile != null)
            {
                zipFile.OnReadEntry(true, null);
            }
            if (first)
            {
                HandlePK00Prefix(readStream);
            }
            if (!ReadHeader(ze, alternateEncoding))
            {
                return null;
            }
            ze.__FileDataPosition = ze.ArchiveStream.Position;
            readStream.Seek(ze._CompressedFileDataSize + ze._LengthOfTrailer, SeekOrigin.Current);
            HandleUnexpectedDataDescriptor(ze);
            if (zipFile != null)
            {
                zipFile.OnReadBytes(ze);
                zipFile.OnReadEntry(false, ze);
            }
            return ze;
        }

        private void ReadExtraField()
        {
            this._readExtraDepth++;
            long position = this.ArchiveStream.Position;
            this.ArchiveStream.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
            byte[] buffer = new byte[30];
            this.ArchiveStream.Read(buffer, 0, buffer.Length);
            int num2 = 0x1a;
            short num3 = (short) (buffer[num2++] + (buffer[num2++] * 0x100));
            short extraFieldLength = (short) (buffer[num2++] + (buffer[num2++] * 0x100));
            this.ArchiveStream.Seek((long) num3, SeekOrigin.Current);
            this.ProcessExtraField(this.ArchiveStream, extraFieldLength);
            this.ArchiveStream.Seek(position, SeekOrigin.Begin);
            this._readExtraDepth--;
        }

        private static bool ReadHeader(ZipEntry ze, Encoding defaultEncoding)
        {
            int num = 0;
            ze._RelativeOffsetOfLocalHeader = ze.ArchiveStream.Position;
            int signature = SharedUtilities.ReadEntrySignature(ze.ArchiveStream);
            num += 4;
            if (IsNotValidSig(signature))
            {
                ze.ArchiveStream.Seek(-4L, SeekOrigin.Current);
                if (IsNotValidZipDirEntrySig(signature) && (signature != 0x6054b50L))
                {
                    throw new BadReadException(string.Format("  Bad signature (0x{0:X8}) at position  0x{1:X8}", signature, ze.ArchiveStream.Position));
                }
                return false;
            }
            byte[] buffer = new byte[0x1a];
            int num3 = ze.ArchiveStream.Read(buffer, 0, buffer.Length);
            if (num3 != buffer.Length)
            {
                return false;
            }
            num += num3;
            int startIndex = 0;
            ze._VersionNeeded = (short) (buffer[startIndex++] + (buffer[startIndex++] * 0x100));
            ze._BitField = (short) (buffer[startIndex++] + (buffer[startIndex++] * 0x100));
            ze._CompressionMethod_FromZipFile = ze._CompressionMethod = (short) (buffer[startIndex++] + (buffer[startIndex++] * 0x100));
            ze._TimeBlob = ((buffer[startIndex++] + (buffer[startIndex++] * 0x100)) + ((buffer[startIndex++] * 0x100) * 0x100)) + (((buffer[startIndex++] * 0x100) * 0x100) * 0x100);
            ze._LastModified = SharedUtilities.PackedToDateTime(ze._TimeBlob);
            ze._timestamp |= ZipEntryTimestamp.DOS;
            if ((ze._BitField & 1) == 1)
            {
                ze._Encryption_FromZipFile = ze._Encryption = EncryptionAlgorithm.PkzipWeak;
                ze._sourceIsEncrypted = true;
            }
            ze._Crc32 = ((buffer[startIndex++] + (buffer[startIndex++] * 0x100)) + ((buffer[startIndex++] * 0x100) * 0x100)) + (((buffer[startIndex++] * 0x100) * 0x100) * 0x100);
            ze._CompressedSize = (long) ((ulong) (((buffer[startIndex++] + (buffer[startIndex++] * 0x100)) + ((buffer[startIndex++] * 0x100) * 0x100)) + (((buffer[startIndex++] * 0x100) * 0x100) * 0x100)));
            ze._UncompressedSize = (long) ((ulong) (((buffer[startIndex++] + (buffer[startIndex++] * 0x100)) + ((buffer[startIndex++] * 0x100) * 0x100)) + (((buffer[startIndex++] * 0x100) * 0x100) * 0x100)));
            if ((((uint) ze._CompressedSize) == uint.MaxValue) || (((uint) ze._UncompressedSize) == uint.MaxValue))
            {
                ze._InputUsesZip64 = true;
            }
            short num5 = (short) (buffer[startIndex++] + (buffer[startIndex++] * 0x100));
            short extraFieldLength = (short) (buffer[startIndex++] + (buffer[startIndex++] * 0x100));
            buffer = new byte[num5];
            num3 = ze.ArchiveStream.Read(buffer, 0, buffer.Length);
            num += num3;
            if ((ze._BitField & 0x800) == 0x800)
            {
                ze.AlternateEncoding = Encoding.UTF8;
                ze.AlternateEncodingUsage = ZipOption.Always;
            }
            ze._FileNameInArchive = ze.AlternateEncoding.GetString(buffer, 0, buffer.Length);
            if (ze._FileNameInArchive.EndsWith("/"))
            {
                ze.MarkAsDirectory();
            }
            num += ze.ProcessExtraField(ze.ArchiveStream, extraFieldLength);
            ze._LengthOfTrailer = 0;
            if (!ze._FileNameInArchive.EndsWith("/") && ((ze._BitField & 8) == 8))
            {
                long position = ze.ArchiveStream.Position;
                bool flag = true;
                long num8 = 0L;
                int num9 = 0;
                while (flag)
                {
                    num9++;
                    if (ze._container.ZipFile != null)
                    {
                        ze._container.ZipFile.OnReadBytes(ze);
                    }
                    long num10 = SharedUtilities.FindSignature(ze.ArchiveStream, 0x8074b50);
                    if (num10 == -1L)
                    {
                        return false;
                    }
                    num8 += num10;
                    if (ze._InputUsesZip64)
                    {
                        buffer = new byte[20];
                        if (ze.ArchiveStream.Read(buffer, 0, buffer.Length) != 20)
                        {
                            return false;
                        }
                        startIndex = 0;
                        ze._Crc32 = ((buffer[startIndex++] + (buffer[startIndex++] * 0x100)) + ((buffer[startIndex++] * 0x100) * 0x100)) + (((buffer[startIndex++] * 0x100) * 0x100) * 0x100);
                        ze._CompressedSize = BitConverter.ToInt64(buffer, startIndex);
                        startIndex += 8;
                        ze._UncompressedSize = BitConverter.ToInt64(buffer, startIndex);
                        startIndex += 8;
                        ze._LengthOfTrailer += 0x18;
                    }
                    else
                    {
                        buffer = new byte[12];
                        if (ze.ArchiveStream.Read(buffer, 0, buffer.Length) != 12)
                        {
                            return false;
                        }
                        startIndex = 0;
                        ze._Crc32 = ((buffer[startIndex++] + (buffer[startIndex++] * 0x100)) + ((buffer[startIndex++] * 0x100) * 0x100)) + (((buffer[startIndex++] * 0x100) * 0x100) * 0x100);
                        ze._CompressedSize = (long) ((ulong) (((buffer[startIndex++] + (buffer[startIndex++] * 0x100)) + ((buffer[startIndex++] * 0x100) * 0x100)) + (((buffer[startIndex++] * 0x100) * 0x100) * 0x100)));
                        ze._UncompressedSize = (long) ((ulong) (((buffer[startIndex++] + (buffer[startIndex++] * 0x100)) + ((buffer[startIndex++] * 0x100) * 0x100)) + (((buffer[startIndex++] * 0x100) * 0x100) * 0x100)));
                        ze._LengthOfTrailer += 0x10;
                    }
                    if (num8 != ze._CompressedSize)
                    {
                        ze.ArchiveStream.Seek(-12L, SeekOrigin.Current);
                        num8 += 4L;
                    }
                }
                ze.ArchiveStream.Seek(position, SeekOrigin.Begin);
            }
            ze._CompressedFileDataSize = ze._CompressedSize;
            if ((ze._BitField & 1) == 1)
            {
                if ((ze.Encryption == EncryptionAlgorithm.WinZipAes128) || (ze.Encryption == EncryptionAlgorithm.WinZipAes256))
                {
                    int keyStrengthInBits = GetKeyStrengthInBits(ze._Encryption_FromZipFile);
                    ze._aesCrypto_forExtract = WinZipAesCrypto.ReadFromStream(null, keyStrengthInBits, ze.ArchiveStream);
                    num += ze._aesCrypto_forExtract.SizeOfEncryptionMetadata - 10;
                    ze._CompressedFileDataSize -= ze._aesCrypto_forExtract.SizeOfEncryptionMetadata;
                    ze._LengthOfTrailer += 10;
                }
                else
                {
                    ze._WeakEncryptionHeader = new byte[12];
                    num += ReadWeakEncryptionHeader(ze._archiveStream, ze._WeakEncryptionHeader);
                    ze._CompressedFileDataSize -= 12L;
                }
            }
            ze._LengthOfHeader = num;
            ze._TotalEntrySize = (ze._LengthOfHeader + ze._CompressedFileDataSize) + ze._LengthOfTrailer;
            return true;
        }

        internal static int ReadWeakEncryptionHeader(Stream s, byte[] buffer)
        {
            int num = s.Read(buffer, 0, 12);
            if (num != 12)
            {
                throw new ZipException(string.Format("Unexpected end of data at position 0x{0:X8}", s.Position));
            }
            return num;
        }

        private static void ReallyDelete(string fileName)
        {
            if ((File.GetAttributes(fileName) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
            {
                File.SetAttributes(fileName, FileAttributes.Normal);
            }
            File.Delete(fileName);
        }

        internal void ResetDirEntry()
        {
            this.__FileDataPosition = -1L;
            this._LengthOfHeader = 0;
        }

        public void SetEntryTimes(DateTime created, DateTime accessed, DateTime modified)
        {
            this._ntfsTimesAreSet = true;
            if ((created == _zeroHour) && (created.Kind == _zeroHour.Kind))
            {
                created = _win32Epoch;
            }
            if ((accessed == _zeroHour) && (accessed.Kind == _zeroHour.Kind))
            {
                accessed = _win32Epoch;
            }
            if ((modified == _zeroHour) && (modified.Kind == _zeroHour.Kind))
            {
                modified = _win32Epoch;
            }
            this._Ctime = created.ToUniversalTime();
            this._Atime = accessed.ToUniversalTime();
            this._Mtime = modified.ToUniversalTime();
            this._LastModified = this._Mtime;
            if (!(this._emitUnixTimes || this._emitNtfsTimes))
            {
                this._emitNtfsTimes = true;
            }
            this._metadataChanged = true;
        }

        private void SetFdpLoh()
        {
            long position = this.ArchiveStream.Position;
            try
            {
                this.ArchiveStream.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
            }
            catch (IOException exception)
            {
                throw new BadStateException(string.Format("Exception seeking  entry({0}) offset(0x{1:X8}) len(0x{2:X8})", this.FileName, this._RelativeOffsetOfLocalHeader, this.ArchiveStream.Length), exception);
            }
            byte[] buffer = new byte[30];
            this.ArchiveStream.Read(buffer, 0, buffer.Length);
            short num2 = (short) (buffer[0x1a] + (buffer[0x1b] * 0x100));
            short num3 = (short) (buffer[0x1c] + (buffer[0x1d] * 0x100));
            this.ArchiveStream.Seek((long) (num2 + num3), SeekOrigin.Current);
            this._LengthOfHeader = ((30 + num3) + num2) + GetLengthOfCryptoHeaderBytes(this._Encryption_FromZipFile);
            this.__FileDataPosition = this._RelativeOffsetOfLocalHeader + this._LengthOfHeader;
            this.ArchiveStream.Seek(position, SeekOrigin.Begin);
        }

        private long SetInputAndFigureFileLength(ref Stream input)
        {
            long length = -1L;
            if (this._Source == ZipEntrySource.Stream)
            {
                this.PrepSourceStream();
                input = this._sourceStream;
                try
                {
                    length = this._sourceStream.Length;
                }
                catch (NotSupportedException)
                {
                }
                return length;
            }
            if (this._Source == ZipEntrySource.ZipFile)
            {
                string password = (this._Encryption_FromZipFile == EncryptionAlgorithm.None) ? null : (this._Password ?? this._container.Password);
                this._sourceStream = this.InternalOpenReader(password);
                this.PrepSourceStream();
                input = this._sourceStream;
                return this._sourceStream.Length;
            }
            if (this._Source == ZipEntrySource.JitStream)
            {
                if (this._sourceStream == null)
                {
                    this._sourceStream = this._OpenDelegate(this.FileName);
                }
                this.PrepSourceStream();
                input = this._sourceStream;
                try
                {
                    length = this._sourceStream.Length;
                }
                catch (NotSupportedException)
                {
                }
                return length;
            }
            if (this._Source == ZipEntrySource.FileSystem)
            {
                FileShare readWrite = FileShare.ReadWrite;
                readWrite |= FileShare.Delete;
                input = File.Open(this.LocalFileName, FileMode.Open, FileAccess.Read, readWrite);
                length = input.Length;
            }
            return length;
        }

        private void SetupCryptoForExtract(string password)
        {
            if (this._Encryption_FromZipFile != EncryptionAlgorithm.None)
            {
                if (this._Encryption_FromZipFile == EncryptionAlgorithm.PkzipWeak)
                {
                    if (password == null)
                    {
                        throw new ZipException("Missing password.");
                    }
                    this.ArchiveStream.Seek(this.FileDataPosition - 12L, SeekOrigin.Begin);
                    this._zipCrypto_forExtract = ZipCrypto.ForRead(password, this);
                }
                else if ((this._Encryption_FromZipFile == EncryptionAlgorithm.WinZipAes128) || (this._Encryption_FromZipFile == EncryptionAlgorithm.WinZipAes256))
                {
                    if (password == null)
                    {
                        throw new ZipException("Missing password.");
                    }
                    if (this._aesCrypto_forExtract != null)
                    {
                        this._aesCrypto_forExtract.Password = password;
                    }
                    else
                    {
                        int lengthOfCryptoHeaderBytes = GetLengthOfCryptoHeaderBytes(this._Encryption_FromZipFile);
                        this.ArchiveStream.Seek(this.FileDataPosition - lengthOfCryptoHeaderBytes, SeekOrigin.Begin);
                        int keyStrengthInBits = GetKeyStrengthInBits(this._Encryption_FromZipFile);
                        this._aesCrypto_forExtract = WinZipAesCrypto.ReadFromStream(password, keyStrengthInBits, this.ArchiveStream);
                    }
                }
            }
        }

        private void SetZip64Flags()
        {
            this._entryRequiresZip64 = new bool?(((this._CompressedSize >= 0xffffffffL) || (this._UncompressedSize >= 0xffffffffL)) || (this._RelativeOffsetOfLocalHeader >= 0xffffffffL));
            if (!((this._container.Zip64 != Zip64Option.Default) ? true : !this._entryRequiresZip64.Value))
            {
                throw new ZipException("Compressed or Uncompressed size, or offset exceeds the maximum value. Consider setting the UseZip64WhenSaving property on the ZipFile instance.");
            }
            this._OutputUsesZip64 = new bool?((this._container.Zip64 == Zip64Option.Always) ? true : this._entryRequiresZip64.Value);
        }

        internal void StoreRelativeOffset()
        {
            this._RelativeOffsetOfLocalHeader = this._future_ROLH;
        }

        public override string ToString()
        {
            return string.Format("ZipEntry::{0}", this.FileName);
        }

        [Conditional("Trace")]
        private void TraceWriteLine(string format, params object[] varParams)
        {
            lock (this._outputLock)
            {
                int hashCode = Thread.CurrentThread.GetHashCode();
                Console.ForegroundColor = (ConsoleColor) ((hashCode % 8) + 8);
                Console.Write("{0:000} ZipEntry.Write ", hashCode);
                Console.WriteLine(format, varParams);
                Console.ResetColor();
            }
        }

        private void ValidateCompression()
        {
            if (((this._CompressionMethod_FromZipFile != 0) && (this._CompressionMethod_FromZipFile != 8)) && (this._CompressionMethod_FromZipFile != 12))
            {
                throw new ZipException(string.Format("Entry {0} uses an unsupported compression method (0x{1:X2}, {2})", this.FileName, this._CompressionMethod_FromZipFile, this.UnsupportedCompressionMethod));
            }
        }

        internal void ValidateEncryption()
        {
            if ((((this.Encryption != EncryptionAlgorithm.PkzipWeak) && (this.Encryption != EncryptionAlgorithm.WinZipAes128)) && (this.Encryption != EncryptionAlgorithm.WinZipAes256)) && (this.Encryption != EncryptionAlgorithm.None))
            {
                if (this._UnsupportedAlgorithmId != 0)
                {
                    throw new ZipException(string.Format("Cannot extract: Entry {0} is encrypted with an algorithm not supported by DotNetZip: {1}", this.FileName, this.UnsupportedAlgorithm));
                }
                throw new ZipException(string.Format("Cannot extract: Entry {0} uses an unsupported encryption algorithm ({1:X2})", this.FileName, (int) this.Encryption));
            }
        }

        private bool ValidateOutput(string basedir, Stream outstream, out string outFileName)
        {
            if (basedir != null)
            {
                string path = this.FileName.Replace(@"\", "/");
                if (path.IndexOf(':') == 1)
                {
                    path = path.Substring(2);
                }
                if (path.StartsWith("/"))
                {
                    path = path.Substring(1);
                }
                if (this._container.ZipFile.FlattenFoldersOnExtract)
                {
                    outFileName = Path.Combine(basedir, (path.IndexOf('/') != -1) ? Path.GetFileName(path) : path);
                }
                else
                {
                    outFileName = Path.Combine(basedir, path);
                }
                outFileName = outFileName.Replace("/", @"\");
                if (!this.IsDirectory && !this.FileName.EndsWith("/"))
                {
                    return false;
                }
                if (!Directory.Exists(outFileName))
                {
                    Directory.CreateDirectory(outFileName);
                    this._SetTimes(outFileName, false);
                }
                else if (this.ExtractExistingFile == ExtractExistingFileAction.OverwriteSilently)
                {
                    this._SetTimes(outFileName, false);
                }
                return true;
            }
            if (outstream == null)
            {
                throw new ArgumentNullException("outstream");
            }
            outFileName = null;
            return (this.IsDirectory || this.FileName.EndsWith("/"));
        }

        internal void VerifyCrcAfterExtract(int actualCrc32)
        {
            if ((actualCrc32 != this._Crc32) && (((this.Encryption != EncryptionAlgorithm.WinZipAes128) && (this.Encryption != EncryptionAlgorithm.WinZipAes256)) || (this._WinZipAesMethod != 2)))
            {
                throw new BadCrcException("CRC error: the file being extracted appears to be corrupted. " + string.Format("Expected 0x{0:X8}, Actual 0x{1:X8}", this._Crc32, actualCrc32));
            }
            if ((this.UncompressedSize != 0L) && ((this.Encryption == EncryptionAlgorithm.WinZipAes128) || (this.Encryption == EncryptionAlgorithm.WinZipAes256)))
            {
                WinZipAesCipherStream stream = this._inputDecryptorStream as WinZipAesCipherStream;
                this._aesCrypto_forExtract.CalculatedMac = stream.FinalAuthentication;
                this._aesCrypto_forExtract.ReadAndVerifyMac(this.ArchiveStream);
            }
        }

        private bool WantReadAgain()
        {
            if (this._UncompressedSize < 0x10L)
            {
                return false;
            }
            if (this._CompressionMethod == 0)
            {
                return false;
            }
            if (this.CompressionLevel == Ionic.Zlib.CompressionLevel.None)
            {
                return false;
            }
            if (this._CompressedSize < this._UncompressedSize)
            {
                return false;
            }
            if (!((this._Source != ZipEntrySource.Stream) || this._sourceStream.CanSeek))
            {
                return false;
            }
            if ((this._aesCrypto_forWrite != null) && ((this.CompressedSize - this._aesCrypto_forWrite.SizeOfEncryptionMetadata) <= (this.UncompressedSize + 0x10L)))
            {
                return false;
            }
            if ((this._zipCrypto_forWrite != null) && ((this.CompressedSize - 12L) <= this.UncompressedSize))
            {
                return false;
            }
            return true;
        }

        internal void Write(Stream s)
        {
            CountingStream stream = s as CountingStream;
            ZipSegmentedStream stream2 = s as ZipSegmentedStream;
            bool flag = false;
        Label_0011:;
            try
            {
                if (!((this._Source != ZipEntrySource.ZipFile) || this._restreamRequiredOnSave))
                {
                    this.CopyThroughOneEntry(s);
                    return;
                }
                if (this.IsDirectory)
                {
                    this.WriteHeader(s, 1);
                    this.StoreRelativeOffset();
                    this._entryRequiresZip64 = new bool?(this._RelativeOffsetOfLocalHeader >= 0xffffffffL);
                    this._OutputUsesZip64 = new bool?((this._container.Zip64 == Zip64Option.Always) ? true : this._entryRequiresZip64.Value);
                    if (stream2 != null)
                    {
                        this._diskNumber = stream2.CurrentSegment;
                    }
                    return;
                }
                bool flag2 = true;
                int cycle = 0;
                do
                {
                    cycle++;
                    this.WriteHeader(s, cycle);
                    this.WriteSecurityMetadata(s);
                    this._WriteEntryData(s);
                    this._TotalEntrySize = (this._LengthOfHeader + this._CompressedFileDataSize) + this._LengthOfTrailer;
                    if (cycle > 1)
                    {
                        flag2 = false;
                    }
                    else if (!s.CanSeek)
                    {
                        flag2 = false;
                    }
                    else
                    {
                        flag2 = this.WantReadAgain();
                    }
                    if (flag2)
                    {
                        if (stream2 != null)
                        {
                            stream2.TruncateBackward(this._diskNumber, this._RelativeOffsetOfLocalHeader);
                        }
                        else
                        {
                            s.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
                        }
                        s.SetLength(s.Position);
                        if (stream != null)
                        {
                            stream.Adjust(this._TotalEntrySize);
                        }
                    }
                }
                while (flag2);
                this._skippedDuringSave = false;
                flag = true;
            }
            catch (Exception exception)
            {
                Ionic.Zip.ZipErrorAction zipErrorAction = this.ZipErrorAction;
                int num2 = 0;
            Label_01A5:
                if (this.ZipErrorAction == Ionic.Zip.ZipErrorAction.Throw)
                {
                    throw;
                }
                if ((this.ZipErrorAction == Ionic.Zip.ZipErrorAction.Skip) || (this.ZipErrorAction == Ionic.Zip.ZipErrorAction.Retry))
                {
                    long num3 = (stream != null) ? stream.ComputedPosition : s.Position;
                    long offset = num3 - this._future_ROLH;
                    if (offset > 0L)
                    {
                        s.Seek(offset, SeekOrigin.Current);
                        long position = s.Position;
                        s.SetLength(s.Position);
                        if (stream != null)
                        {
                            stream.Adjust(num3 - position);
                        }
                    }
                    if (this.ZipErrorAction == Ionic.Zip.ZipErrorAction.Skip)
                    {
                        this.WriteStatus("Skipping file {0} (exception: {1})", new object[] { this.LocalFileName, exception.ToString() });
                        this._skippedDuringSave = true;
                        flag = true;
                    }
                    else
                    {
                        this.ZipErrorAction = zipErrorAction;
                    }
                }
                else
                {
                    if (num2 > 0)
                    {
                        throw;
                    }
                    if (this.ZipErrorAction == Ionic.Zip.ZipErrorAction.InvokeErrorEvent)
                    {
                        this.OnZipErrorWhileSaving(exception);
                        if (this._ioOperationCanceled)
                        {
                            flag = true;
                            goto Label_02E8;
                        }
                    }
                    num2++;
                    goto Label_01A5;
                }
            }
        Label_02E8:
            if (!flag)
            {
                goto Label_0011;
            }
        }

        internal void WriteCentralDirectoryEntry(Stream s)
        {
            byte[] dst = new byte[0x1000];
            int dstOffset = 0;
            dst[dstOffset++] = 80;
            dst[dstOffset++] = 0x4b;
            dst[dstOffset++] = 1;
            dst[dstOffset++] = 2;
            dst[dstOffset++] = (byte) (this._VersionMadeBy & 0xff);
            dst[dstOffset++] = (byte) ((this._VersionMadeBy & 0xff00) >> 8);
            short num2 = (this.VersionNeeded != 0) ? this.VersionNeeded : ((short) 20);
            if (!this._OutputUsesZip64.HasValue)
            {
                this._OutputUsesZip64 = new bool?(this._container.Zip64 == Zip64Option.Always);
            }
            short num3 = this._OutputUsesZip64.Value ? ((short) 0x2d) : num2;
            if (this.CompressionMethod == Ionic.Zip.CompressionMethod.BZip2)
            {
                num3 = 0x2e;
            }
            dst[dstOffset++] = (byte) (num3 & 0xff);
            dst[dstOffset++] = (byte) ((num3 & 0xff00) >> 8);
            dst[dstOffset++] = (byte) (this._BitField & 0xff);
            dst[dstOffset++] = (byte) ((this._BitField & 0xff00) >> 8);
            dst[dstOffset++] = (byte) (this._CompressionMethod & 0xff);
            dst[dstOffset++] = (byte) ((this._CompressionMethod & 0xff00) >> 8);
            if ((this.Encryption == EncryptionAlgorithm.WinZipAes128) || (this.Encryption == EncryptionAlgorithm.WinZipAes256))
            {
                dstOffset -= 2;
                dst[dstOffset++] = 0x63;
                dst[dstOffset++] = 0;
            }
            dst[dstOffset++] = (byte) (this._TimeBlob & 0xff);
            dst[dstOffset++] = (byte) ((this._TimeBlob & 0xff00) >> 8);
            dst[dstOffset++] = (byte) ((this._TimeBlob & 0xff0000) >> 0x10);
            dst[dstOffset++] = (byte) ((this._TimeBlob & 0xff000000L) >> 0x18);
            dst[dstOffset++] = (byte) (this._Crc32 & 0xff);
            dst[dstOffset++] = (byte) ((this._Crc32 & 0xff00) >> 8);
            dst[dstOffset++] = (byte) ((this._Crc32 & 0xff0000) >> 0x10);
            dst[dstOffset++] = (byte) ((this._Crc32 & 0xff000000L) >> 0x18);
            int num4 = 0;
            if (this._OutputUsesZip64.Value)
            {
                for (num4 = 0; num4 < 8; num4++)
                {
                    dst[dstOffset++] = 0xff;
                }
            }
            else
            {
                dst[dstOffset++] = (byte) (this._CompressedSize & 0xffL);
                dst[dstOffset++] = (byte) ((this._CompressedSize & 0xff00L) >> 8);
                dst[dstOffset++] = (byte) ((this._CompressedSize & 0xff0000L) >> 0x10);
                dst[dstOffset++] = (byte) ((this._CompressedSize & 0xff000000L) >> 0x18);
                dst[dstOffset++] = (byte) (this._UncompressedSize & 0xffL);
                dst[dstOffset++] = (byte) ((this._UncompressedSize & 0xff00L) >> 8);
                dst[dstOffset++] = (byte) ((this._UncompressedSize & 0xff0000L) >> 0x10);
                dst[dstOffset++] = (byte) ((this._UncompressedSize & 0xff000000L) >> 0x18);
            }
            byte[] encodedFileNameBytes = this.GetEncodedFileNameBytes();
            short length = (short) encodedFileNameBytes.Length;
            dst[dstOffset++] = (byte) (length & 0xff);
            dst[dstOffset++] = (byte) ((length & 0xff00) >> 8);
            this._presumeZip64 = this._OutputUsesZip64.Value;
            this._Extra = this.ConstructExtraField(true);
            short count = (this._Extra == null) ? ((short) 0) : ((short) this._Extra.Length);
            dst[dstOffset++] = (byte) (count & 0xff);
            dst[dstOffset++] = (byte) ((count & 0xff00) >> 8);
            int num7 = (this._CommentBytes == null) ? 0 : this._CommentBytes.Length;
            if ((num7 + dstOffset) > dst.Length)
            {
                num7 = dst.Length - dstOffset;
            }
            dst[dstOffset++] = (byte) (num7 & 0xff);
            dst[dstOffset++] = (byte) ((num7 & 0xff00) >> 8);
            if ((this._container.ZipFile != null) && (this._container.ZipFile.MaxOutputSegmentSize != 0))
            {
                dst[dstOffset++] = (byte) (this._diskNumber & 0xff);
                dst[dstOffset++] = (byte) ((this._diskNumber & 0xff00) >> 8);
            }
            else
            {
                dst[dstOffset++] = 0;
                dst[dstOffset++] = 0;
            }
            dst[dstOffset++] = this._IsText ? ((byte) 1) : ((byte) 0);
            dst[dstOffset++] = 0;
            dst[dstOffset++] = (byte) (this._ExternalFileAttrs & 0xff);
            dst[dstOffset++] = (byte) ((this._ExternalFileAttrs & 0xff00) >> 8);
            dst[dstOffset++] = (byte) ((this._ExternalFileAttrs & 0xff0000) >> 0x10);
            dst[dstOffset++] = (byte) ((this._ExternalFileAttrs & 0xff000000L) >> 0x18);
            if (this._RelativeOffsetOfLocalHeader > 0xffffffffL)
            {
                dst[dstOffset++] = 0xff;
                dst[dstOffset++] = 0xff;
                dst[dstOffset++] = 0xff;
                dst[dstOffset++] = 0xff;
            }
            else
            {
                dst[dstOffset++] = (byte) (this._RelativeOffsetOfLocalHeader & 0xffL);
                dst[dstOffset++] = (byte) ((this._RelativeOffsetOfLocalHeader & 0xff00L) >> 8);
                dst[dstOffset++] = (byte) ((this._RelativeOffsetOfLocalHeader & 0xff0000L) >> 0x10);
                dst[dstOffset++] = (byte) ((this._RelativeOffsetOfLocalHeader & 0xff000000L) >> 0x18);
            }
            Buffer.BlockCopy(encodedFileNameBytes, 0, dst, dstOffset, length);
            dstOffset += length;
            if (this._Extra != null)
            {
                byte[] src = this._Extra;
                int srcOffset = 0;
                Buffer.BlockCopy(src, srcOffset, dst, dstOffset, count);
                dstOffset += count;
            }
            if (num7 != 0)
            {
                Buffer.BlockCopy(this._CommentBytes, 0, dst, dstOffset, num7);
                dstOffset += num7;
            }
            s.Write(dst, 0, dstOffset);
        }

        internal void WriteHeader(Stream s, int cycle)
        {
            CountingStream stream = s as CountingStream;
            this._future_ROLH = (stream != null) ? stream.ComputedPosition : s.Position;
            int num = 0;
            int count = 0;
            byte[] src = new byte[30];
            src[count++] = 80;
            src[count++] = 0x4b;
            src[count++] = 3;
            src[count++] = 4;
            this._presumeZip64 = (this._container.Zip64 == Zip64Option.Always) || ((this._container.Zip64 == Zip64Option.AsNecessary) && !s.CanSeek);
            short num3 = this._presumeZip64 ? ((short) 0x2d) : ((short) 20);
            if (this.CompressionMethod == Ionic.Zip.CompressionMethod.BZip2)
            {
                num3 = 0x2e;
            }
            src[count++] = (byte) (num3 & 0xff);
            src[count++] = (byte) ((num3 & 0xff00) >> 8);
            byte[] encodedFileNameBytes = this.GetEncodedFileNameBytes();
            short length = (short) encodedFileNameBytes.Length;
            if (this._Encryption == EncryptionAlgorithm.None)
            {
                this._BitField = (short) (this._BitField & -2);
            }
            else
            {
                this._BitField = (short) (this._BitField | 1);
            }
            if (this._actualEncoding.CodePage == Encoding.UTF8.CodePage)
            {
                this._BitField = (short) (this._BitField | 0x800);
            }
            if (this.IsDirectory || (cycle == 0x63))
            {
                this._BitField = (short) (this._BitField & -9);
                this._BitField = (short) (this._BitField & -2);
                this.Encryption = EncryptionAlgorithm.None;
                this.Password = null;
            }
            else if (!s.CanSeek)
            {
                this._BitField = (short) (this._BitField | 8);
            }
            src[count++] = (byte) (this._BitField & 0xff);
            src[count++] = (byte) ((this._BitField & 0xff00) >> 8);
            if (this.__FileDataPosition == -1L)
            {
                this._CompressedSize = 0L;
                this._crcCalculated = false;
            }
            this.MaybeUnsetCompressionMethodForWriting(cycle);
            src[count++] = (byte) (this._CompressionMethod & 0xff);
            src[count++] = (byte) ((this._CompressionMethod & 0xff00) >> 8);
            if (cycle == 0x63)
            {
                this.SetZip64Flags();
            }
            else if ((this.Encryption == EncryptionAlgorithm.WinZipAes128) || (this.Encryption == EncryptionAlgorithm.WinZipAes256))
            {
                count -= 2;
                src[count++] = 0x63;
                src[count++] = 0;
            }
            this._TimeBlob = SharedUtilities.DateTimeToPacked(this.LastModified);
            src[count++] = (byte) (this._TimeBlob & 0xff);
            src[count++] = (byte) ((this._TimeBlob & 0xff00) >> 8);
            src[count++] = (byte) ((this._TimeBlob & 0xff0000) >> 0x10);
            src[count++] = (byte) ((this._TimeBlob & 0xff000000L) >> 0x18);
            src[count++] = (byte) (this._Crc32 & 0xff);
            src[count++] = (byte) ((this._Crc32 & 0xff00) >> 8);
            src[count++] = (byte) ((this._Crc32 & 0xff0000) >> 0x10);
            src[count++] = (byte) ((this._Crc32 & 0xff000000L) >> 0x18);
            if (this._presumeZip64)
            {
                for (num = 0; num < 8; num++)
                {
                    src[count++] = 0xff;
                }
            }
            else
            {
                src[count++] = (byte) (this._CompressedSize & 0xffL);
                src[count++] = (byte) ((this._CompressedSize & 0xff00L) >> 8);
                src[count++] = (byte) ((this._CompressedSize & 0xff0000L) >> 0x10);
                src[count++] = (byte) ((this._CompressedSize & 0xff000000L) >> 0x18);
                src[count++] = (byte) (this._UncompressedSize & 0xffL);
                src[count++] = (byte) ((this._UncompressedSize & 0xff00L) >> 8);
                src[count++] = (byte) ((this._UncompressedSize & 0xff0000L) >> 0x10);
                src[count++] = (byte) ((this._UncompressedSize & 0xff000000L) >> 0x18);
            }
            src[count++] = (byte) (length & 0xff);
            src[count++] = (byte) ((length & 0xff00) >> 8);
            this._Extra = this.ConstructExtraField(false);
            short num5 = (this._Extra == null) ? ((short) 0) : ((short) this._Extra.Length);
            src[count++] = (byte) (num5 & 0xff);
            src[count++] = (byte) ((num5 & 0xff00) >> 8);
            byte[] dst = new byte[(count + length) + num5];
            Buffer.BlockCopy(src, 0, dst, 0, count);
            Buffer.BlockCopy(encodedFileNameBytes, 0, dst, count, encodedFileNameBytes.Length);
            count += encodedFileNameBytes.Length;
            if (this._Extra != null)
            {
                Buffer.BlockCopy(this._Extra, 0, dst, count, this._Extra.Length);
                count += this._Extra.Length;
            }
            this._LengthOfHeader = count;
            ZipSegmentedStream stream2 = s as ZipSegmentedStream;
            if (stream2 != null)
            {
                stream2.ContiguousWrite = true;
                uint num6 = stream2.ComputeSegment(count);
                if (num6 != stream2.CurrentSegment)
                {
                    this._future_ROLH = 0L;
                }
                else
                {
                    this._future_ROLH = stream2.Position;
                }
                this._diskNumber = num6;
            }
            if ((this._container.Zip64 == Zip64Option.Default) && (((uint) this._RelativeOffsetOfLocalHeader) >= uint.MaxValue))
            {
                throw new ZipException("Offset within the zip archive exceeds 0xFFFFFFFF. Consider setting the UseZip64WhenSaving property on the ZipFile instance.");
            }
            s.Write(dst, 0, count);
            if (stream2 != null)
            {
                stream2.ContiguousWrite = false;
            }
            this._EntryHeader = dst;
        }

        internal void WriteSecurityMetadata(Stream outstream)
        {
            if (this.Encryption != EncryptionAlgorithm.None)
            {
                string password = this._Password;
                if ((this._Source == ZipEntrySource.ZipFile) && (password == null))
                {
                    password = this._container.Password;
                }
                if (password == null)
                {
                    this._zipCrypto_forWrite = null;
                    this._aesCrypto_forWrite = null;
                }
                else if (this.Encryption == EncryptionAlgorithm.PkzipWeak)
                {
                    this._zipCrypto_forWrite = ZipCrypto.ForWrite(password);
                    Random random = new Random();
                    byte[] buffer = new byte[12];
                    random.NextBytes(buffer);
                    if ((this._BitField & 8) == 8)
                    {
                        this._TimeBlob = SharedUtilities.DateTimeToPacked(this.LastModified);
                        buffer[11] = (byte) ((this._TimeBlob >> 8) & 0xff);
                    }
                    else
                    {
                        this.FigureCrc32();
                        buffer[11] = (byte) ((this._Crc32 >> 0x18) & 0xff);
                    }
                    byte[] buffer2 = this._zipCrypto_forWrite.EncryptMessage(buffer, buffer.Length);
                    outstream.Write(buffer2, 0, buffer2.Length);
                    this._LengthOfHeader += buffer2.Length;
                }
                else if ((this.Encryption == EncryptionAlgorithm.WinZipAes128) || (this.Encryption == EncryptionAlgorithm.WinZipAes256))
                {
                    int keyStrengthInBits = GetKeyStrengthInBits(this.Encryption);
                    this._aesCrypto_forWrite = WinZipAesCrypto.Generate(password, keyStrengthInBits);
                    outstream.Write(this._aesCrypto_forWrite.Salt, 0, this._aesCrypto_forWrite._Salt.Length);
                    outstream.Write(this._aesCrypto_forWrite.GeneratedPV, 0, this._aesCrypto_forWrite.GeneratedPV.Length);
                    this._LengthOfHeader += this._aesCrypto_forWrite._Salt.Length + this._aesCrypto_forWrite.GeneratedPV.Length;
                }
            }
        }

        private void WriteStatus(string format, params object[] args)
        {
            if ((this._container.ZipFile != null) && this._container.ZipFile.Verbose)
            {
                this._container.ZipFile.StatusMessageTextWriter.WriteLine(format, args);
            }
        }

        public DateTime AccessedTime
        {
            get
            {
                return this._Atime;
            }
            set
            {
                this.SetEntryTimes(this._Ctime, value, this._Mtime);
            }
        }

        public Encoding AlternateEncoding { get; set; }

        public ZipOption AlternateEncodingUsage { get; set; }

        internal Stream ArchiveStream
        {
            get
            {
                if (this._archiveStream == null)
                {
                    if (this._container.ZipFile != null)
                    {
                        ZipFile zipFile = this._container.ZipFile;
                        zipFile.Reset(false);
                        this._archiveStream = zipFile.StreamForDiskNumber(this._diskNumber);
                    }
                    else
                    {
                        this._archiveStream = this._container.ZipOutputStream.OutputStream;
                    }
                }
                return this._archiveStream;
            }
        }

        public FileAttributes Attributes
        {
            get
            {
                return (FileAttributes) this._ExternalFileAttrs;
            }
            set
            {
                this._ExternalFileAttrs = (int) value;
                this._VersionMadeBy = 0x2d;
                this._metadataChanged = true;
            }
        }

        internal bool AttributesIndicateDirectory
        {
            get
            {
                return ((this._InternalFileAttrs == 0) && ((this._ExternalFileAttrs & 0x10) == 0x10));
            }
        }

        public short BitField
        {
            get
            {
                return this._BitField;
            }
        }

        private int BufferSize
        {
            get
            {
                return this._container.BufferSize;
            }
        }

        public string Comment
        {
            get
            {
                return this._Comment;
            }
            set
            {
                this._Comment = value;
                this._metadataChanged = true;
            }
        }

        public long CompressedSize
        {
            get
            {
                return this._CompressedSize;
            }
        }

        public Ionic.Zlib.CompressionLevel CompressionLevel
        {
            get
            {
                return this._CompressionLevel;
            }
            set
            {
                if (((this._CompressionMethod == 8) || (this._CompressionMethod == 0)) && ((value != Ionic.Zlib.CompressionLevel.Default) || (this._CompressionMethod != 8)))
                {
                    this._CompressionLevel = value;
                    if ((value != Ionic.Zlib.CompressionLevel.None) || (this._CompressionMethod != 0))
                    {
                        if (this._CompressionLevel == Ionic.Zlib.CompressionLevel.None)
                        {
                            this._CompressionMethod = 0;
                        }
                        else
                        {
                            this._CompressionMethod = 8;
                        }
                        if (this._container.ZipFile != null)
                        {
                            this._container.ZipFile.NotifyEntryChanged();
                        }
                        this._restreamRequiredOnSave = true;
                    }
                }
            }
        }

        public Ionic.Zip.CompressionMethod CompressionMethod
        {
            get
            {
                return (Ionic.Zip.CompressionMethod) this._CompressionMethod;
            }
            set
            {
                if (value != ((Ionic.Zip.CompressionMethod) this._CompressionMethod))
                {
                    if (((value != Ionic.Zip.CompressionMethod.None) && (value != Ionic.Zip.CompressionMethod.Deflate)) && (value != Ionic.Zip.CompressionMethod.BZip2))
                    {
                        throw new InvalidOperationException("Unsupported compression method.");
                    }
                    this._CompressionMethod = (short) value;
                    if (this._CompressionMethod == 0)
                    {
                        this._CompressionLevel = Ionic.Zlib.CompressionLevel.None;
                    }
                    else if (this.CompressionLevel == Ionic.Zlib.CompressionLevel.None)
                    {
                        this._CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
                    }
                    if (this._container.ZipFile != null)
                    {
                        this._container.ZipFile.NotifyEntryChanged();
                    }
                    this._restreamRequiredOnSave = true;
                }
            }
        }

        public double CompressionRatio
        {
            get
            {
                if (this.UncompressedSize == 0L)
                {
                    return 0.0;
                }
                return (100.0 * (1.0 - ((1.0 * this.CompressedSize) / (1.0 * this.UncompressedSize))));
            }
        }

        public int Crc
        {
            get
            {
                return this._Crc32;
            }
        }

        public DateTime CreationTime
        {
            get
            {
                return this._Ctime;
            }
            set
            {
                this.SetEntryTimes(value, this._Atime, this._Mtime);
            }
        }

        public bool EmitTimesInUnixFormatWhenSaving
        {
            get
            {
                return this._emitUnixTimes;
            }
            set
            {
                this._emitUnixTimes = value;
                this._metadataChanged = true;
            }
        }

        public bool EmitTimesInWindowsFormatWhenSaving
        {
            get
            {
                return this._emitNtfsTimes;
            }
            set
            {
                this._emitNtfsTimes = value;
                this._metadataChanged = true;
            }
        }

        public EncryptionAlgorithm Encryption
        {
            get
            {
                return this._Encryption;
            }
            set
            {
                if (value != this._Encryption)
                {
                    if (value == EncryptionAlgorithm.Unsupported)
                    {
                        throw new InvalidOperationException("You may not set Encryption to that value.");
                    }
                    this._Encryption = value;
                    this._restreamRequiredOnSave = true;
                    if (this._container.ZipFile != null)
                    {
                        this._container.ZipFile.NotifyEntryChanged();
                    }
                }
            }
        }

        public ExtractExistingFileAction ExtractExistingFile { get; set; }

        internal long FileDataPosition
        {
            get
            {
                if (this.__FileDataPosition == -1L)
                {
                    this.SetFdpLoh();
                }
                return this.__FileDataPosition;
            }
        }

        public string FileName
        {
            get
            {
                return this._FileNameInArchive;
            }
            set
            {
                if (this._container.ZipFile == null)
                {
                    throw new ZipException("Cannot rename; this is not supported in ZipOutputStream/ZipInputStream.");
                }
                if (string.IsNullOrEmpty(value))
                {
                    throw new ZipException("The FileName must be non empty and non-null.");
                }
                string name = NameInArchive(value, null);
                if (this._FileNameInArchive != name)
                {
                    this._container.ZipFile.RemoveEntry(this);
                    this._container.ZipFile.InternalAddEntry(name, this);
                    this._FileNameInArchive = name;
                    this._container.ZipFile.NotifyEntryChanged();
                    this._metadataChanged = true;
                }
            }
        }

        public bool IncludedInMostRecentSave
        {
            get
            {
                return !this._skippedDuringSave;
            }
        }

        public string Info
        {
            get
            {
                StringBuilder builder = new StringBuilder();
                builder.Append(string.Format("          ZipEntry: {0}\n", this.FileName)).Append(string.Format("   Version Made By: {0}\n", this._VersionMadeBy)).Append(string.Format(" Needed to extract: {0}\n", this.VersionNeeded));
                if (this._IsDirectory)
                {
                    builder.Append("        Entry type: directory\n");
                }
                else
                {
                    builder.Append(string.Format("         File type: {0}\n", this._IsText ? "text" : "binary")).Append(string.Format("       Compression: {0}\n", this.CompressionMethod)).Append(string.Format("        Compressed: 0x{0:X}\n", this.CompressedSize)).Append(string.Format("      Uncompressed: 0x{0:X}\n", this.UncompressedSize)).Append(string.Format("             CRC32: 0x{0:X8}\n", this._Crc32));
                }
                builder.Append(string.Format("       Disk Number: {0}\n", this._diskNumber));
                if (this._RelativeOffsetOfLocalHeader > 0xffffffffL)
                {
                    builder.Append(string.Format("   Relative Offset: 0x{0:X16}\n", this._RelativeOffsetOfLocalHeader));
                }
                else
                {
                    builder.Append(string.Format("   Relative Offset: 0x{0:X8}\n", this._RelativeOffsetOfLocalHeader));
                }
                builder.Append(string.Format("         Bit Field: 0x{0:X4}\n", this._BitField)).Append(string.Format("        Encrypted?: {0}\n", this._sourceIsEncrypted)).Append(string.Format("          Timeblob: 0x{0:X8}\n", this._TimeBlob)).Append(string.Format("              Time: {0}\n", SharedUtilities.PackedToDateTime(this._TimeBlob)));
                builder.Append(string.Format("         Is Zip64?: {0}\n", this._InputUsesZip64));
                if (!string.IsNullOrEmpty(this._Comment))
                {
                    builder.Append(string.Format("           Comment: {0}\n", this._Comment));
                }
                builder.Append("\n");
                return builder.ToString();
            }
        }

        public Stream InputStream
        {
            get
            {
                return this._sourceStream;
            }
            set
            {
                if (this._Source != ZipEntrySource.Stream)
                {
                    throw new ZipException("You must not set the input stream for this entry.");
                }
                this._sourceWasJitProvided = true;
                this._sourceStream = value;
            }
        }

        public bool InputStreamWasJitProvided
        {
            get
            {
                return this._sourceWasJitProvided;
            }
        }

        internal bool IsChanged
        {
            get
            {
                return (this._restreamRequiredOnSave | this._metadataChanged);
            }
        }

        public bool IsDirectory
        {
            get
            {
                return this._IsDirectory;
            }
        }

        public bool IsText
        {
            get
            {
                return this._IsText;
            }
            set
            {
                this._IsText = value;
            }
        }

        public DateTime LastModified
        {
            get
            {
                return this._LastModified.ToLocalTime();
            }
            set
            {
                this._LastModified = (value.Kind == DateTimeKind.Unspecified) ? DateTime.SpecifyKind(value, DateTimeKind.Local) : value.ToLocalTime();
                this._Mtime = SharedUtilities.AdjustTime_Reverse(this._LastModified).ToUniversalTime();
                this._metadataChanged = true;
            }
        }

        private int LengthOfHeader
        {
            get
            {
                if (this._LengthOfHeader == 0)
                {
                    this.SetFdpLoh();
                }
                return this._LengthOfHeader;
            }
        }

        internal string LocalFileName
        {
            get
            {
                return this._LocalFileName;
            }
        }

        public DateTime ModifiedTime
        {
            get
            {
                return this._Mtime;
            }
            set
            {
                this.SetEntryTimes(this._Ctime, this._Atime, value);
            }
        }

        public bool? OutputUsedZip64
        {
            get
            {
                return this._OutputUsesZip64;
            }
        }

        public string Password
        {
            private get
            {
                return this._Password;
            }
            set
            {
                this._Password = value;
                if (this._Password == null)
                {
                    this._Encryption = EncryptionAlgorithm.None;
                }
                else
                {
                    if (!((this._Source != ZipEntrySource.ZipFile) || this._sourceIsEncrypted))
                    {
                        this._restreamRequiredOnSave = true;
                    }
                    if (this.Encryption == EncryptionAlgorithm.None)
                    {
                        this._Encryption = EncryptionAlgorithm.PkzipWeak;
                    }
                }
            }
        }

        [Obsolete("This property is obsolete since v1.9.1.6. Use AlternateEncoding and AlternateEncodingUsage instead.", true)]
        public Encoding ProvisionalAlternateEncoding { get; set; }

        public bool? RequiresZip64
        {
            get
            {
                return this._entryRequiresZip64;
            }
        }

        public SetCompressionCallback SetCompression { get; set; }

        public ZipEntrySource Source
        {
            get
            {
                return this._Source;
            }
        }

        public ZipEntryTimestamp Timestamp
        {
            get
            {
                return this._timestamp;
            }
        }

        public long UncompressedSize
        {
            get
            {
                return this._UncompressedSize;
            }
        }

        private string UnsupportedAlgorithm
        {
            get
            {
                switch (this._UnsupportedAlgorithmId)
                {
                    case 0x660e:
                        return "PKWare AES128";

                    case 0x660f:
                        return "PKWare AES192";

                    case 0x6610:
                        return "PKWare AES256";

                    case 0x6609:
                        return "3DES-112";

                    case 0x6601:
                        return "DES";

                    case 0x6602:
                        return "RC2";

                    case 0x6603:
                        return "3DES-168";

                    case 0:
                        return "--";

                    case 0x6720:
                        return "Blowfish";

                    case 0x6721:
                        return "Twofish";

                    case 0x6702:
                        return "RC2";

                    case 0x6801:
                        return "RC4";
                }
                return string.Format("Unknown (0x{0:X4})", this._UnsupportedAlgorithmId);
            }
        }

        private string UnsupportedCompressionMethod
        {
            get
            {
                switch (this._CompressionMethod)
                {
                    case 0x13:
                        return "LZ77";

                    case 0x62:
                        return "PPMd";

                    case 0:
                        return "Store";

                    case 1:
                        return "Shrink";

                    case 8:
                        return "DEFLATE";

                    case 9:
                        return "Deflate64";

                    case 12:
                        return "BZIP2";

                    case 14:
                        return "LZMA";
                }
                return string.Format("Unknown (0x{0:X4})", this._CompressionMethod);
            }
        }

        public bool UsesEncryption
        {
            get
            {
                return (this._Encryption_FromZipFile != EncryptionAlgorithm.None);
            }
        }

        [Obsolete("Beginning with v1.9.1.6 of DotNetZip, this property is obsolete.  It will be removed in a future version of the library. Your applications should  use AlternateEncoding and AlternateEncodingUsage instead.")]
        public bool UseUnicodeAsNecessary
        {
            get
            {
                return ((this.AlternateEncoding == Encoding.GetEncoding("UTF-8")) && (this.AlternateEncodingUsage == ZipOption.AsNecessary));
            }
            set
            {
                if (value)
                {
                    this.AlternateEncoding = Encoding.GetEncoding("UTF-8");
                    this.AlternateEncodingUsage = ZipOption.AsNecessary;
                }
                else
                {
                    this.AlternateEncoding = ZipFile.DefaultEncoding;
                    this.AlternateEncodingUsage = ZipOption.Default;
                }
            }
        }

        public short VersionNeeded
        {
            get
            {
                return this._VersionNeeded;
            }
        }

        public Ionic.Zip.ZipErrorAction ZipErrorAction { get; set; }

        private class CopyHelper
        {
            private static int callCount = 0;
            private static Regex re = new Regex(@" \(copy (\d+)\)$");

            internal static string AppendCopyToFileName(string f)
            {
                Match match;
                string str;
                callCount++;
                if (callCount > 0x19)
                {
                    throw new OverflowException("overflow while creating filename");
                }
                int num = 1;
                int length = f.LastIndexOf(".");
                if (length == -1)
                {
                    match = re.Match(f);
                    if (match.Success)
                    {
                        num = int.Parse(match.Groups[1].Value) + 1;
                        str = string.Format(" (copy {0})", num);
                        f = f.Substring(0, match.Index) + str;
                        return f;
                    }
                    str = string.Format(" (copy {0})", num);
                    f = f + str;
                    return f;
                }
                match = re.Match(f.Substring(0, length));
                if (match.Success)
                {
                    num = int.Parse(match.Groups[1].Value) + 1;
                    str = string.Format(" (copy {0})", num);
                    f = f.Substring(0, match.Index) + str + f.Substring(length);
                    return f;
                }
                str = string.Format(" (copy {0})", num);
                f = f.Substring(0, length) + str + f.Substring(length);
                return f;
            }
        }

        private delegate T Func<T>();
    }
}

