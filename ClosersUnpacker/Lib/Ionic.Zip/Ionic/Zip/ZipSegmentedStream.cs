﻿namespace Ionic.Zip
{
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;

    internal class ZipSegmentedStream : Stream
    {
        private string _baseDir;
        private string _baseName;
        private uint _currentDiskNumber;
        private string _currentName;
        private string _currentTempName;
        private bool _exceptionPending = false;
        private Stream _innerStream;
        private uint _maxDiskNumber;
        private int _maxSegmentSize;
        private RwMode rwMode;

        private ZipSegmentedStream()
        {
        }

        private string _NameForSegment(uint diskNumber)
        {
            if (diskNumber >= 0x63)
            {
                this._exceptionPending = true;
                throw new OverflowException("The number of zip segments would exceed 99.");
            }
            return string.Format("{0}.z{1:D2}", Path.Combine(Path.GetDirectoryName(this._baseName), Path.GetFileNameWithoutExtension(this._baseName)), diskNumber + 1);
        }

        private void _SetReadStream()
        {
            if (this._innerStream != null)
            {
                this._innerStream.Dispose();
            }
            if ((this.CurrentSegment + 1) == this._maxDiskNumber)
            {
                this._currentName = this._baseName;
            }
            this._innerStream = File.OpenRead(this.CurrentName);
        }

        private void _SetWriteStream(uint increment)
        {
            if (this._innerStream != null)
            {
                this._innerStream.Dispose();
                if (File.Exists(this.CurrentName))
                {
                    File.Delete(this.CurrentName);
                }
                File.Move(this._currentTempName, this.CurrentName);
            }
            if (increment > 0)
            {
                this.CurrentSegment += increment;
            }
            SharedUtilities.CreateAndOpenUniqueTempFile(this._baseDir, out this._innerStream, out this._currentTempName);
            if (this.CurrentSegment == 0)
            {
                this._innerStream.Write(BitConverter.GetBytes(0x8074b50), 0, 4);
            }
        }

        public uint ComputeSegment(int length)
        {
            if ((this._innerStream.Position + length) > this._maxSegmentSize)
            {
                return (this.CurrentSegment + 1);
            }
            return this.CurrentSegment;
        }

        protected override void Dispose(bool disposing)
        {
            try
            {
                if (this._innerStream != null)
                {
                    this._innerStream.Dispose();
                    if ((this.rwMode == RwMode.Write) && this._exceptionPending)
                    {
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        public override void Flush()
        {
            this._innerStream.Flush();
        }

        public static ZipSegmentedStream ForReading(string name, uint initialDiskNumber, uint maxDiskNumber)
        {
            ZipSegmentedStream stream = new ZipSegmentedStream {
                rwMode = RwMode.ReadOnly,
                CurrentSegment = initialDiskNumber,
                _maxDiskNumber = maxDiskNumber,
                _baseName = name
            };
            stream._SetReadStream();
            return stream;
        }

        public static Stream ForUpdate(string name, uint diskNumber)
        {
            if (diskNumber >= 0x63)
            {
                throw new ArgumentOutOfRangeException("diskNumber");
            }
            return File.Open(string.Format("{0}.z{1:D2}", Path.Combine(Path.GetDirectoryName(name), Path.GetFileNameWithoutExtension(name)), diskNumber + 1), FileMode.Open, FileAccess.ReadWrite, FileShare.None);
        }

        public static ZipSegmentedStream ForWriting(string name, int maxSegmentSize)
        {
            ZipSegmentedStream stream = new ZipSegmentedStream {
                rwMode = RwMode.Write,
                CurrentSegment = 0,
                _baseName = name,
                _maxSegmentSize = maxSegmentSize,
                _baseDir = Path.GetDirectoryName(name)
            };
            if (stream._baseDir == "")
            {
                stream._baseDir = ".";
            }
            stream._SetWriteStream(0);
            return stream;
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            if (this.rwMode != RwMode.ReadOnly)
            {
                this._exceptionPending = true;
                throw new InvalidOperationException("Stream Error: Cannot Read.");
            }
            int num = this._innerStream.Read(buffer, offset, count);
            int num2 = num;
            while (num2 != count)
            {
                if (this._innerStream.Position != this._innerStream.Length)
                {
                    this._exceptionPending = true;
                    throw new ZipException(string.Format("Read error in file {0}", this.CurrentName));
                }
                if ((this.CurrentSegment + 1) == this._maxDiskNumber)
                {
                    return num;
                }
                this.CurrentSegment++;
                this._SetReadStream();
                offset += num2;
                count -= num2;
                num2 = this._innerStream.Read(buffer, offset, count);
                num += num2;
            }
            return num;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return this._innerStream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            if (this.rwMode != RwMode.Write)
            {
                this._exceptionPending = true;
                throw new InvalidOperationException();
            }
            this._innerStream.SetLength(value);
        }

        public override string ToString()
        {
            return string.Format("{0}[{1}][{2}], pos=0x{3:X})", new object[] { "ZipSegmentedStream", this.CurrentName, this.rwMode.ToString(), this.Position });
        }

        public long TruncateBackward(uint diskNumber, long offset)
        {
            if (diskNumber >= 0x63)
            {
                throw new ArgumentOutOfRangeException("diskNumber");
            }
            if (this.rwMode != RwMode.Write)
            {
                this._exceptionPending = true;
                throw new ZipException("bad state.");
            }
            if (diskNumber != this.CurrentSegment)
            {
                if (this._innerStream != null)
                {
                    this._innerStream.Dispose();
                    if (File.Exists(this._currentTempName))
                    {
                        File.Delete(this._currentTempName);
                    }
                }
                for (uint i = this.CurrentSegment - 1; i > diskNumber; i--)
                {
                    string path = this._NameForSegment(i);
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }
                }
                this.CurrentSegment = diskNumber;
                for (int j = 0; j < 3; j++)
                {
                    try
                    {
                        this._currentTempName = SharedUtilities.InternalGetTempFileName();
                        File.Move(this.CurrentName, this._currentTempName);
                        break;
                    }
                    catch (IOException)
                    {
                        if (j == 2)
                        {
                            throw;
                        }
                    }
                }
                this._innerStream = new FileStream(this._currentTempName, FileMode.Open);
            }
            return this._innerStream.Seek(offset, SeekOrigin.Begin);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            if (this.rwMode != RwMode.Write)
            {
                this._exceptionPending = true;
                throw new InvalidOperationException("Stream Error: Cannot Write.");
            }
            if (!this.ContiguousWrite)
            {
                while ((this._innerStream.Position + count) > this._maxSegmentSize)
                {
                    int num = this._maxSegmentSize - ((int) this._innerStream.Position);
                    this._innerStream.Write(buffer, offset, num);
                    this._SetWriteStream(1);
                    count -= num;
                    offset += num;
                }
            }
            else if ((this._innerStream.Position + count) > this._maxSegmentSize)
            {
                this._SetWriteStream(1);
            }
            this._innerStream.Write(buffer, offset, count);
        }

        public override bool CanRead
        {
            get
            {
                return (((this.rwMode == RwMode.ReadOnly) && (this._innerStream != null)) && this._innerStream.CanRead);
            }
        }

        public override bool CanSeek
        {
            get
            {
                return ((this._innerStream != null) && this._innerStream.CanSeek);
            }
        }

        public override bool CanWrite
        {
            get
            {
                return (((this.rwMode == RwMode.Write) && (this._innerStream != null)) && this._innerStream.CanWrite);
            }
        }

        public bool ContiguousWrite { get; set; }

        public string CurrentName
        {
            get
            {
                if (this._currentName == null)
                {
                    this._currentName = this._NameForSegment(this.CurrentSegment);
                }
                return this._currentName;
            }
        }

        public uint CurrentSegment
        {
            get
            {
                return this._currentDiskNumber;
            }
            private set
            {
                this._currentDiskNumber = value;
                this._currentName = null;
            }
        }

        public string CurrentTempName
        {
            get
            {
                return this._currentTempName;
            }
        }

        public override long Length
        {
            get
            {
                return this._innerStream.Length;
            }
        }

        public override long Position
        {
            get
            {
                return this._innerStream.Position;
            }
            set
            {
                this._innerStream.Position = value;
            }
        }

        private enum RwMode
        {
            None,
            ReadOnly,
            Write
        }
    }
}

