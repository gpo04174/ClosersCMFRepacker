﻿namespace Ionic.Zip
{
    using System;

    public class ZipErrorEventArgs : ZipProgressEventArgs
    {
        private System.Exception _exc;

        private ZipErrorEventArgs()
        {
        }

        internal static ZipErrorEventArgs Saving(string archiveName, ZipEntry entry, System.Exception exception)
        {
            return new ZipErrorEventArgs { 
                EventType = ZipProgressEventType.Error_Saving,
                ArchiveName = archiveName,
                CurrentEntry = entry,
                _exc = exception
            };
        }

        public System.Exception Exception
        {
            get
            {
                return this._exc;
            }
        }

        public string FileName
        {
            get
            {
                return base.CurrentEntry.LocalFileName;
            }
        }
    }
}

