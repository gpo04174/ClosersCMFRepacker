﻿namespace Ionic.Zip
{
    using System;

    public enum ZipOption
    {
        Always = 2,
        AsNecessary = 1,
        Default = 0,
        Never = 0
    }
}

