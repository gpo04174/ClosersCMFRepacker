﻿namespace Ionic.Zip
{
    using Ionic.Zlib;
    using System;
    using System.Runtime.CompilerServices;

    public delegate CompressionLevel SetCompressionCallback(string localFileName, string fileNameInArchive);
}

