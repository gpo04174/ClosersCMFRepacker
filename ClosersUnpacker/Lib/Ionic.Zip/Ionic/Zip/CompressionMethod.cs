﻿namespace Ionic.Zip
{
    using System;

    public enum CompressionMethod
    {
        BZip2 = 12,
        Deflate = 8,
        None = 0
    }
}

