﻿namespace Ionic.Zip
{
    using Ionic;
    using Ionic.Zlib;
    using Microsoft.CSharp;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.IO;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Threading;

    [ComVisible(true), ClassInterface(ClassInterfaceType.AutoDispatch), Guid("ebc25cf6-9120-4283-b972-0e5520d00005")]
    public class ZipFile : IEnumerable<ZipEntry>, IEnumerable, IDisposable
    {
        private bool _addOperationCanceled;
        private Encoding _alternateEncoding;
        private ZipOption _alternateEncodingUsage;
        private int _BufferSize;
        private bool _CaseSensitiveRetrieval;
        private string _Comment;
        private Ionic.Zip.CompressionMethod _compressionMethod;
        private bool _contentsChanged;
        private static Encoding _defaultEncoding = Encoding.GetEncoding("IBM437");
        private uint _diskNumberWithCd;
        private bool _disposed;
        private bool _emitNtfsTimes;
        private bool _emitUnixTimes;
        private EncryptionAlgorithm _Encryption;
        private Dictionary<string, ZipEntry> _entries;
        private bool _extractOperationCanceled;
        private bool _fileAlreadyExists;
        private bool _hasBeenSaved;
        internal bool _inExtractAll;
        private bool _JustSaved;
        private long _lengthOfReadStream;
        private long _locEndOfCDS;
        private int _maxBufferPairs;
        private int _maxOutputSegmentSize;
        private string _name;
        private uint _numberOfSegmentsForMostRecentSave;
        private uint _OffsetOfCentralDirectory;
        private long _OffsetOfCentralDirectory64;
        private bool? _OutputUsesZip64;
        private long _ParallelDeflateThreshold;
        internal string _Password;
        private string _readName;
        private Stream _readstream;
        private bool _ReadStreamIsOurs;
        private bool _saveOperationCanceled;
        private bool _SavingSfx;
        private TextWriter _StatusMessageTextWriter;
        private CompressionStrategy _Strategy;
        private string _TempFileFolder;
        private string _temporaryFileName;
        private ushort _versionMadeBy;
        private ushort _versionNeededToExtract;
        private Stream _writestream;
        internal Zip64Option _zip64;
        private List<ZipEntry> _zipEntriesAsList;
        private Ionic.Zip.ZipErrorAction _zipErrorAction;
        public static readonly int BufferSizeDefault = 0x8000;
        private object LOCK;
        internal ParallelDeflateOutputStream ParallelDeflater;
        private static ExtractorSettings[] SettingsList;

        public event EventHandler<AddProgressEventArgs> AddProgress;

        public event EventHandler<ExtractProgressEventArgs> ExtractProgress;

        public event EventHandler<ReadProgressEventArgs> ReadProgress;

        public event EventHandler<SaveProgressEventArgs> SaveProgress;

        public event EventHandler<ZipErrorEventArgs> ZipError;

        static ZipFile()
        {
            ExtractorSettings[] settingsArray = new ExtractorSettings[2];
            ExtractorSettings settings = new ExtractorSettings {
                Flavor = SelfExtractorFlavor.WinFormsApplication
            };
            List<string> list = new List<string> { 
                "System.dll",
                "System.Windows.Forms.dll",
                "System.Drawing.dll"
            };
            settings.ReferencedAssemblies = list;
            List<string> list2 = new List<string> { 
                "Ionic.Zip.WinFormsSelfExtractorStub.resources",
                "Ionic.Zip.Forms.PasswordDialog.resources",
                "Ionic.Zip.Forms.ZipContentsDialog.resources"
            };
            settings.CopyThroughResources = list2;
            List<string> list3 = new List<string> { 
                "WinFormsSelfExtractorStub.cs",
                "WinFormsSelfExtractorStub.Designer.cs",
                "PasswordDialog.cs",
                "PasswordDialog.Designer.cs",
                "ZipContentsDialog.cs",
                "ZipContentsDialog.Designer.cs",
                "FolderBrowserDialogEx.cs"
            };
            settings.ResourcesToCompile = list3;
            settingsArray[0] = settings;
            ExtractorSettings settings2 = new ExtractorSettings {
                Flavor = SelfExtractorFlavor.ConsoleApplication
            };
            List<string> list4 = new List<string> { "System.dll" };
            settings2.ReferencedAssemblies = list4;
            settings2.CopyThroughResources = null;
            List<string> list5 = new List<string> { "CommandLineSelfExtractorStub.cs" };
            settings2.ResourcesToCompile = list5;
            settingsArray[1] = settings2;
            SettingsList = settingsArray;
        }

        public ZipFile()
        {
            this._emitNtfsTimes = true;
            this._Strategy = CompressionStrategy.Default;
            this._compressionMethod = Ionic.Zip.CompressionMethod.Deflate;
            this._ReadStreamIsOurs = true;
            this.LOCK = new object();
            this._locEndOfCDS = -1L;
            this._alternateEncoding = Encoding.GetEncoding("IBM437");
            this._alternateEncodingUsage = ZipOption.Default;
            this._BufferSize = BufferSizeDefault;
            this._maxBufferPairs = 0x10;
            this._zip64 = Zip64Option.Default;
            this._lengthOfReadStream = -99L;
            this._InitInstance(null, null);
        }

        public ZipFile(string fileName)
        {
            this._emitNtfsTimes = true;
            this._Strategy = CompressionStrategy.Default;
            this._compressionMethod = Ionic.Zip.CompressionMethod.Deflate;
            this._ReadStreamIsOurs = true;
            this.LOCK = new object();
            this._locEndOfCDS = -1L;
            this._alternateEncoding = Encoding.GetEncoding("IBM437");
            this._alternateEncodingUsage = ZipOption.Default;
            this._BufferSize = BufferSizeDefault;
            this._maxBufferPairs = 0x10;
            this._zip64 = Zip64Option.Default;
            this._lengthOfReadStream = -99L;
            try
            {
                this._InitInstance(fileName, null);
            }
            catch (Exception exception)
            {
                throw new ZipException(string.Format("Could not read {0} as a zip file", fileName), exception);
            }
        }

        public ZipFile(Encoding encoding)
        {
            this._emitNtfsTimes = true;
            this._Strategy = CompressionStrategy.Default;
            this._compressionMethod = Ionic.Zip.CompressionMethod.Deflate;
            this._ReadStreamIsOurs = true;
            this.LOCK = new object();
            this._locEndOfCDS = -1L;
            this._alternateEncoding = Encoding.GetEncoding("IBM437");
            this._alternateEncodingUsage = ZipOption.Default;
            this._BufferSize = BufferSizeDefault;
            this._maxBufferPairs = 0x10;
            this._zip64 = Zip64Option.Default;
            this._lengthOfReadStream = -99L;
            this.AlternateEncoding = encoding;
            this.AlternateEncodingUsage = ZipOption.Always;
            this._InitInstance(null, null);
        }

        public ZipFile(string fileName, TextWriter statusMessageWriter)
        {
            this._emitNtfsTimes = true;
            this._Strategy = CompressionStrategy.Default;
            this._compressionMethod = Ionic.Zip.CompressionMethod.Deflate;
            this._ReadStreamIsOurs = true;
            this.LOCK = new object();
            this._locEndOfCDS = -1L;
            this._alternateEncoding = Encoding.GetEncoding("IBM437");
            this._alternateEncodingUsage = ZipOption.Default;
            this._BufferSize = BufferSizeDefault;
            this._maxBufferPairs = 0x10;
            this._zip64 = Zip64Option.Default;
            this._lengthOfReadStream = -99L;
            try
            {
                this._InitInstance(fileName, statusMessageWriter);
            }
            catch (Exception exception)
            {
                throw new ZipException(string.Format("{0} is not a valid zip file", fileName), exception);
            }
        }

        public ZipFile(string fileName, Encoding encoding)
        {
            this._emitNtfsTimes = true;
            this._Strategy = CompressionStrategy.Default;
            this._compressionMethod = Ionic.Zip.CompressionMethod.Deflate;
            this._ReadStreamIsOurs = true;
            this.LOCK = new object();
            this._locEndOfCDS = -1L;
            this._alternateEncoding = Encoding.GetEncoding("IBM437");
            this._alternateEncodingUsage = ZipOption.Default;
            this._BufferSize = BufferSizeDefault;
            this._maxBufferPairs = 0x10;
            this._zip64 = Zip64Option.Default;
            this._lengthOfReadStream = -99L;
            try
            {
                this.AlternateEncoding = encoding;
                this.AlternateEncodingUsage = ZipOption.Always;
                this._InitInstance(fileName, null);
            }
            catch (Exception exception)
            {
                throw new ZipException(string.Format("{0} is not a valid zip file", fileName), exception);
            }
        }

        public ZipFile(string fileName, TextWriter statusMessageWriter, Encoding encoding)
        {
            this._emitNtfsTimes = true;
            this._Strategy = CompressionStrategy.Default;
            this._compressionMethod = Ionic.Zip.CompressionMethod.Deflate;
            this._ReadStreamIsOurs = true;
            this.LOCK = new object();
            this._locEndOfCDS = -1L;
            this._alternateEncoding = Encoding.GetEncoding("IBM437");
            this._alternateEncodingUsage = ZipOption.Default;
            this._BufferSize = BufferSizeDefault;
            this._maxBufferPairs = 0x10;
            this._zip64 = Zip64Option.Default;
            this._lengthOfReadStream = -99L;
            try
            {
                this.AlternateEncoding = encoding;
                this.AlternateEncodingUsage = ZipOption.Always;
                this._InitInstance(fileName, statusMessageWriter);
            }
            catch (Exception exception)
            {
                throw new ZipException(string.Format("{0} is not a valid zip file", fileName), exception);
            }
        }

        private void _AddOrUpdateSelectedFiles(string selectionCriteria, string directoryOnDisk, string directoryPathInArchive, bool recurseDirectories, bool wantUpdate)
        {
            if ((directoryOnDisk == null) && Directory.Exists(selectionCriteria))
            {
                directoryOnDisk = selectionCriteria;
                selectionCriteria = "*.*";
            }
            else if (string.IsNullOrEmpty(directoryOnDisk))
            {
                directoryOnDisk = ".";
            }
            while (directoryOnDisk.EndsWith(@"\"))
            {
                directoryOnDisk = directoryOnDisk.Substring(0, directoryOnDisk.Length - 1);
            }
            if (this.Verbose)
            {
                this.StatusMessageTextWriter.WriteLine("adding selection '{0}' from dir '{1}'...", selectionCriteria, directoryOnDisk);
            }
            ReadOnlyCollection<string> onlys = new FileSelector(selectionCriteria, this.AddDirectoryWillTraverseReparsePoints).SelectFiles(directoryOnDisk, recurseDirectories);
            if (this.Verbose)
            {
                this.StatusMessageTextWriter.WriteLine("found {0} files...", onlys.Count);
            }
            this.OnAddStarted();
            AddOrUpdateAction action = wantUpdate ? AddOrUpdateAction.AddOrUpdate : AddOrUpdateAction.AddOnly;
            foreach (string str in onlys)
            {
                string str2 = (directoryPathInArchive == null) ? null : ReplaceLeadingDirectory(Path.GetDirectoryName(str), directoryOnDisk, directoryPathInArchive);
                if (File.Exists(str))
                {
                    if (wantUpdate)
                    {
                        this.UpdateFile(str, str2);
                    }
                    else
                    {
                        this.AddFile(str, str2);
                    }
                }
                else
                {
                    this.AddOrUpdateDirectoryImpl(str, str2, action, false, 0);
                }
            }
            this.OnAddCompleted();
        }

        private void _initEntriesDictionary()
        {
            StringComparer comparer = this.CaseSensitiveRetrieval ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase;
            this._entries = (this._entries == null) ? new Dictionary<string, ZipEntry>(comparer) : new Dictionary<string, ZipEntry>(this._entries, comparer);
        }

        private void _InitInstance(string zipFileName, TextWriter statusMessageWriter)
        {
            this._name = zipFileName;
            this._StatusMessageTextWriter = statusMessageWriter;
            this._contentsChanged = true;
            this.AddDirectoryWillTraverseReparsePoints = true;
            this.CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
            this.ParallelDeflateThreshold = 0x80000L;
            this._initEntriesDictionary();
            if (File.Exists(this._name))
            {
                if (this.FullScan)
                {
                    ReadIntoInstance_Orig(this);
                }
                else
                {
                    ReadIntoInstance(this);
                }
                this._fileAlreadyExists = true;
            }
        }

        private ZipEntry _InternalAddEntry(ZipEntry ze)
        {
            ze._container = new ZipContainer(this);
            ze.CompressionMethod = this.CompressionMethod;
            ze.CompressionLevel = this.CompressionLevel;
            ze.ExtractExistingFile = this.ExtractExistingFile;
            ze.ZipErrorAction = this.ZipErrorAction;
            ze.SetCompression = this.SetCompression;
            ze.AlternateEncoding = this.AlternateEncoding;
            ze.AlternateEncodingUsage = this.AlternateEncodingUsage;
            ze.Password = this._Password;
            ze.Encryption = this.Encryption;
            ze.EmitTimesInWindowsFormatWhenSaving = this._emitNtfsTimes;
            ze.EmitTimesInUnixFormatWhenSaving = this._emitUnixTimes;
            this.InternalAddEntry(ze.FileName, ze);
            this.AfterAddEntry(ze);
            return ze;
        }

        private void _InternalExtractAll(string path, bool overrideExtractExistingProperty)
        {
            bool verbose = this.Verbose;
            this._inExtractAll = true;
            try
            {
                this.OnExtractAllStarted(path);
                int current = 0;
                foreach (ZipEntry entry in this._entries.Values)
                {
                    if (verbose)
                    {
                        this.StatusMessageTextWriter.WriteLine("\n{1,-22} {2,-8} {3,4}   {4,-8}  {0}", new object[] { "Name", "Modified", "Size", "Ratio", "Packed" });
                        this.StatusMessageTextWriter.WriteLine(new string('-', 0x48));
                        verbose = false;
                    }
                    if (this.Verbose)
                    {
                        this.StatusMessageTextWriter.WriteLine("{1,-22} {2,-8} {3,4:F0}%   {4,-8} {0}", new object[] { entry.FileName, entry.LastModified.ToString("yyyy-MM-dd HH:mm:ss"), entry.UncompressedSize, entry.CompressionRatio, entry.CompressedSize });
                        if (!string.IsNullOrEmpty(entry.Comment))
                        {
                            this.StatusMessageTextWriter.WriteLine("  Comment: {0}", entry.Comment);
                        }
                    }
                    entry.Password = this._Password;
                    this.OnExtractEntry(current, true, entry, path);
                    if (overrideExtractExistingProperty)
                    {
                        entry.ExtractExistingFile = this.ExtractExistingFile;
                    }
                    entry.Extract(path);
                    current++;
                    this.OnExtractEntry(current, false, entry, path);
                    if (this._extractOperationCanceled)
                    {
                        break;
                    }
                }
                if (!this._extractOperationCanceled)
                {
                    foreach (ZipEntry entry in this._entries.Values)
                    {
                        if (entry.IsDirectory || entry.FileName.EndsWith("/"))
                        {
                            string fileOrDirectory = entry.FileName.StartsWith("/") ? Path.Combine(path, entry.FileName.Substring(1)) : Path.Combine(path, entry.FileName);
                            entry._SetTimes(fileOrDirectory, false);
                        }
                    }
                    this.OnExtractAllCompleted(path);
                }
            }
            finally
            {
                this._inExtractAll = false;
            }
        }

        private void _SaveSfxStub(string exeToGenerate, SelfExtractorSaveOptions options)
        {
            string str = null;
            string path = null;
            string str3 = null;
            string dir = null;
            try
            {
                if (File.Exists(exeToGenerate) && this.Verbose)
                {
                    this.StatusMessageTextWriter.WriteLine("The existing file ({0}) will be overwritten.", exeToGenerate);
                }
                if (!exeToGenerate.EndsWith(".exe") && this.Verbose)
                {
                    this.StatusMessageTextWriter.WriteLine("Warning: The generated self-extracting file will not have an .exe extension.");
                }
                dir = this.TempFileFolder ?? Path.GetDirectoryName(exeToGenerate);
                path = GenerateTempPathname(dir, "exe");
                Assembly assembly = typeof(ZipFile).Assembly;
                using (CSharpCodeProvider provider = new CSharpCodeProvider())
                {
                    ExtractorSettings settings = null;
                    foreach (ExtractorSettings settings2 in SettingsList)
                    {
                        if (settings2.Flavor == options.Flavor)
                        {
                            settings = settings2;
                            break;
                        }
                    }
                    if (settings == null)
                    {
                        throw new BadStateException(string.Format("While saving a Self-Extracting Zip, Cannot find that flavor ({0})?", options.Flavor));
                    }
                    CompilerParameters parameters = new CompilerParameters {
                        ReferencedAssemblies = { assembly.Location }
                    };
                    if (settings.ReferencedAssemblies != null)
                    {
                        foreach (string str5 in settings.ReferencedAssemblies)
                        {
                            parameters.ReferencedAssemblies.Add(str5);
                        }
                    }
                    parameters.GenerateInMemory = false;
                    parameters.GenerateExecutable = true;
                    parameters.IncludeDebugInformation = false;
                    parameters.CompilerOptions = "";
                    Assembly executingAssembly = Assembly.GetExecutingAssembly();
                    StringBuilder builder = new StringBuilder();
                    string str6 = GenerateTempPathname(dir, "cs");
                    using (ZipFile file = Read(executingAssembly.GetManifestResourceStream("Ionic.Zip.Resources.ZippedResources.zip")))
                    {
                        str3 = GenerateTempPathname(dir, "tmp");
                        if (string.IsNullOrEmpty(options.IconFile))
                        {
                            Directory.CreateDirectory(str3);
                            ZipEntry entry = file["zippedFile.ico"];
                            if ((entry.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                            {
                                entry.Attributes ^= FileAttributes.ReadOnly;
                            }
                            entry.Extract(str3);
                            str = Path.Combine(str3, "zippedFile.ico");
                            parameters.CompilerOptions = parameters.CompilerOptions + string.Format("/win32icon:\"{0}\"", str);
                        }
                        else
                        {
                            parameters.CompilerOptions = parameters.CompilerOptions + string.Format("/win32icon:\"{0}\"", options.IconFile);
                        }
                        parameters.OutputAssembly = path;
                        if (options.Flavor == SelfExtractorFlavor.WinFormsApplication)
                        {
                            parameters.CompilerOptions = parameters.CompilerOptions + " /target:winexe";
                        }
                        if (!string.IsNullOrEmpty(options.AdditionalCompilerSwitches))
                        {
                            parameters.CompilerOptions = parameters.CompilerOptions + " " + options.AdditionalCompilerSwitches;
                        }
                        if (string.IsNullOrEmpty(parameters.CompilerOptions))
                        {
                            parameters.CompilerOptions = null;
                        }
                        if ((settings.CopyThroughResources != null) && (settings.CopyThroughResources.Count != 0))
                        {
                            if (!Directory.Exists(str3))
                            {
                                Directory.CreateDirectory(str3);
                            }
                            foreach (string str7 in settings.CopyThroughResources)
                            {
                                string filename = Path.Combine(str3, str7);
                                ExtractResourceToFile(executingAssembly, str7, filename);
                                parameters.EmbeddedResources.Add(filename);
                            }
                        }
                        parameters.EmbeddedResources.Add(assembly.Location);
                        builder.Append("// " + Path.GetFileName(str6) + "\n").Append("// --------------------------------------------\n//\n").Append("// This SFX source file was generated by DotNetZip ").Append(LibraryVersion.ToString()).Append("\n//         at ").Append(DateTime.Now.ToString("yyyy MMMM dd  HH:mm:ss")).Append("\n//\n// --------------------------------------------\n\n\n");
                        if (!string.IsNullOrEmpty(options.Description))
                        {
                            builder.Append("[assembly: System.Reflection.AssemblyTitle(\"" + options.Description.Replace("\"", "") + "\")]\n");
                        }
                        else
                        {
                            builder.Append("[assembly: System.Reflection.AssemblyTitle(\"DotNetZip SFX Archive\")]\n");
                        }
                        if (!string.IsNullOrEmpty(options.ProductVersion))
                        {
                            builder.Append("[assembly: System.Reflection.AssemblyInformationalVersion(\"" + options.ProductVersion.Replace("\"", "") + "\")]\n");
                        }
                        string str9 = string.IsNullOrEmpty(options.Copyright) ? "Extractor: Copyright \x00a9 Dino Chiesa 2008-2011" : options.Copyright.Replace("\"", "");
                        if (!string.IsNullOrEmpty(options.ProductName))
                        {
                            builder.Append("[assembly: System.Reflection.AssemblyProduct(\"").Append(options.ProductName.Replace("\"", "")).Append("\")]\n");
                        }
                        else
                        {
                            builder.Append("[assembly: System.Reflection.AssemblyProduct(\"DotNetZip\")]\n");
                        }
                        builder.Append("[assembly: System.Reflection.AssemblyCopyright(\"" + str9 + "\")]\n").Append(string.Format("[assembly: System.Reflection.AssemblyVersion(\"{0}\")]\n", LibraryVersion.ToString()));
                        if (options.FileVersion != null)
                        {
                            builder.Append(string.Format("[assembly: System.Reflection.AssemblyFileVersion(\"{0}\")]\n", options.FileVersion.ToString()));
                        }
                        builder.Append("\n\n\n");
                        string defaultExtractDirectory = options.DefaultExtractDirectory;
                        if (defaultExtractDirectory != null)
                        {
                            defaultExtractDirectory = defaultExtractDirectory.Replace("\"", "").Replace(@"\", @"\\");
                        }
                        string postExtractCommandLine = options.PostExtractCommandLine;
                        if (postExtractCommandLine != null)
                        {
                            postExtractCommandLine = postExtractCommandLine.Replace(@"\", @"\\").Replace("\"", "\\\"");
                        }
                        foreach (string str12 in settings.ResourcesToCompile)
                        {
                            using (Stream stream = file[str12].OpenReader())
                            {
                                if (stream == null)
                                {
                                    throw new ZipException(string.Format("missing resource '{0}'", str12));
                                }
                                using (StreamReader reader = new StreamReader(stream))
                                {
                                    while (reader.Peek() >= 0)
                                    {
                                        string str13 = reader.ReadLine();
                                        if (defaultExtractDirectory != null)
                                        {
                                            str13 = str13.Replace("@@EXTRACTLOCATION", defaultExtractDirectory);
                                        }
                                        str13 = str13.Replace("@@REMOVE_AFTER_EXECUTE", options.RemoveUnpackedFilesAfterExecute.ToString()).Replace("@@QUIET", options.Quiet.ToString());
                                        if (!string.IsNullOrEmpty(options.SfxExeWindowTitle))
                                        {
                                            str13 = str13.Replace("@@SFX_EXE_WINDOW_TITLE", options.SfxExeWindowTitle);
                                        }
                                        str13 = str13.Replace("@@EXTRACT_EXISTING_FILE", ((int) options.ExtractExistingFile).ToString());
                                        if (postExtractCommandLine != null)
                                        {
                                            str13 = str13.Replace("@@POST_UNPACK_CMD_LINE", postExtractCommandLine);
                                        }
                                        builder.Append(str13).Append("\n");
                                    }
                                }
                                builder.Append("\n\n");
                            }
                        }
                    }
                    string str14 = builder.ToString();
                    CompilerResults results = provider.CompileAssemblyFromSource(parameters, new string[] { str14 });
                    if (results == null)
                    {
                        throw new SfxGenerationException("Cannot compile the extraction logic!");
                    }
                    if (this.Verbose)
                    {
                        foreach (string str15 in results.Output)
                        {
                            this.StatusMessageTextWriter.WriteLine(str15);
                        }
                    }
                    if (results.Errors.Count != 0)
                    {
                        using (TextWriter writer = new StreamWriter(str6))
                        {
                            writer.Write(str14);
                            writer.Write("\n\n\n// ------------------------------------------------------------------\n");
                            writer.Write("// Errors during compilation: \n//\n");
                            string fileName = Path.GetFileName(str6);
                            foreach (CompilerError error in results.Errors)
                            {
                                writer.Write(string.Format("//   {0}({1},{2}): {3} {4}: {5}\n//\n", new object[] { fileName, error.Line, error.Column, error.IsWarning ? "Warning" : "error", error.ErrorNumber, error.ErrorText }));
                            }
                        }
                        throw new SfxGenerationException(string.Format("Errors compiling the extraction logic!  {0}", str6));
                    }
                    this.OnSaveEvent(ZipProgressEventType.Saving_AfterCompileSelfExtractor);
                    using (Stream stream2 = File.OpenRead(path))
                    {
                        byte[] buffer = new byte[0xfa0];
                        int count = 1;
                        while (count != 0)
                        {
                            count = stream2.Read(buffer, 0, buffer.Length);
                            if (count != 0)
                            {
                                this.WriteStream.Write(buffer, 0, count);
                            }
                        }
                    }
                }
                this.OnSaveEvent(ZipProgressEventType.Saving_AfterSaveTempArchive);
            }
            finally
            {
                try
                {
                    IOException exception;
                    if (Directory.Exists(str3))
                    {
                        try
                        {
                            Directory.Delete(str3, true);
                        }
                        catch (IOException exception1)
                        {
                            exception = exception1;
                            this.StatusMessageTextWriter.WriteLine("Warning: Exception: {0}", exception);
                        }
                    }
                    if (File.Exists(path))
                    {
                        try
                        {
                            File.Delete(path);
                        }
                        catch (IOException exception2)
                        {
                            exception = exception2;
                            this.StatusMessageTextWriter.WriteLine("Warning: Exception: {0}", exception);
                        }
                    }
                }
                catch (IOException)
                {
                }
            }
        }

        public ZipEntry AddDirectory(string directoryName)
        {
            return this.AddDirectory(directoryName, null);
        }

        public ZipEntry AddDirectory(string directoryName, string directoryPathInArchive)
        {
            return this.AddOrUpdateDirectoryImpl(directoryName, directoryPathInArchive, AddOrUpdateAction.AddOnly);
        }

        public ZipEntry AddDirectoryByName(string directoryNameInArchive)
        {
            ZipEntry entry = ZipEntry.CreateFromNothing(directoryNameInArchive);
            entry._container = new ZipContainer(this);
            entry.MarkAsDirectory();
            entry.AlternateEncoding = this.AlternateEncoding;
            entry.AlternateEncodingUsage = this.AlternateEncodingUsage;
            entry.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
            entry.EmitTimesInWindowsFormatWhenSaving = this._emitNtfsTimes;
            entry.EmitTimesInUnixFormatWhenSaving = this._emitUnixTimes;
            entry._Source = ZipEntrySource.Stream;
            this.InternalAddEntry(entry.FileName, entry);
            this.AfterAddEntry(entry);
            return entry;
        }

        public ZipEntry AddEntry(string entryName, WriteDelegate writer)
        {
            ZipEntry ze = ZipEntry.CreateForWriter(entryName, writer);
            if (this.Verbose)
            {
                this.StatusMessageTextWriter.WriteLine("adding {0}...", entryName);
            }
            return this._InternalAddEntry(ze);
        }

        public ZipEntry AddEntry(string entryName, Stream stream)
        {
            ZipEntry ze = ZipEntry.CreateForStream(entryName, stream);
            ze.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
            if (this.Verbose)
            {
                this.StatusMessageTextWriter.WriteLine("adding {0}...", entryName);
            }
            return this._InternalAddEntry(ze);
        }

        public ZipEntry AddEntry(string entryName, string content)
        {
            return this.AddEntry(entryName, content, Encoding.Default);
        }

        public ZipEntry AddEntry(string entryName, byte[] byteContent)
        {
            if (byteContent == null)
            {
                throw new ArgumentException("bad argument", "byteContent");
            }
            MemoryStream stream = new MemoryStream(byteContent);
            return this.AddEntry(entryName, stream);
        }

        public ZipEntry AddEntry(string entryName, OpenDelegate opener, CloseDelegate closer)
        {
            ZipEntry ze = ZipEntry.CreateForJitStreamProvider(entryName, opener, closer);
            ze.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
            if (this.Verbose)
            {
                this.StatusMessageTextWriter.WriteLine("adding {0}...", entryName);
            }
            return this._InternalAddEntry(ze);
        }

        public ZipEntry AddEntry(string entryName, string content, Encoding encoding)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream, encoding);
            writer.Write(content);
            writer.Flush();
            stream.Seek(0L, SeekOrigin.Begin);
            return this.AddEntry(entryName, stream);
        }

        public ZipEntry AddFile(string fileName)
        {
            return this.AddFile(fileName, null);
        }

        public ZipEntry AddFile(string fileName, string directoryPathInArchive)
        {
            string nameInArchive = ZipEntry.NameInArchive(fileName, directoryPathInArchive);
            ZipEntry ze = ZipEntry.CreateFromFile(fileName, nameInArchive);
            if (this.Verbose)
            {
                this.StatusMessageTextWriter.WriteLine("adding {0}...", fileName);
            }
            return this._InternalAddEntry(ze);
        }

        public void AddFiles(IEnumerable<string> fileNames)
        {
            this.AddFiles(fileNames, null);
        }

        public void AddFiles(IEnumerable<string> fileNames, string directoryPathInArchive)
        {
            this.AddFiles(fileNames, false, directoryPathInArchive);
        }

        public void AddFiles(IEnumerable<string> fileNames, bool preserveDirHierarchy, string directoryPathInArchive)
        {
            if (fileNames == null)
            {
                throw new ArgumentNullException("fileNames");
            }
            this._addOperationCanceled = false;
            this.OnAddStarted();
            if (preserveDirHierarchy)
            {
                foreach (string str in fileNames)
                {
                    if (this._addOperationCanceled)
                    {
                        break;
                    }
                    if (directoryPathInArchive != null)
                    {
                        string fullPath = Path.GetFullPath(Path.Combine(directoryPathInArchive, Path.GetDirectoryName(str)));
                        this.AddFile(str, fullPath);
                    }
                    else
                    {
                        this.AddFile(str, null);
                    }
                }
            }
            else
            {
                foreach (string str in fileNames)
                {
                    if (this._addOperationCanceled)
                    {
                        break;
                    }
                    this.AddFile(str, directoryPathInArchive);
                }
            }
            if (!this._addOperationCanceled)
            {
                this.OnAddCompleted();
            }
        }

        public ZipEntry AddItem(string fileOrDirectoryName)
        {
            return this.AddItem(fileOrDirectoryName, null);
        }

        public ZipEntry AddItem(string fileOrDirectoryName, string directoryPathInArchive)
        {
            if (File.Exists(fileOrDirectoryName))
            {
                return this.AddFile(fileOrDirectoryName, directoryPathInArchive);
            }
            if (!Directory.Exists(fileOrDirectoryName))
            {
                throw new FileNotFoundException(string.Format("That file or directory ({0}) does not exist!", fileOrDirectoryName));
            }
            return this.AddDirectory(fileOrDirectoryName, directoryPathInArchive);
        }

        private ZipEntry AddOrUpdateDirectoryImpl(string directoryName, string rootDirectoryPathInArchive, AddOrUpdateAction action)
        {
            if (rootDirectoryPathInArchive == null)
            {
                rootDirectoryPathInArchive = "";
            }
            return this.AddOrUpdateDirectoryImpl(directoryName, rootDirectoryPathInArchive, action, true, 0);
        }

        private ZipEntry AddOrUpdateDirectoryImpl(string directoryName, string rootDirectoryPathInArchive, AddOrUpdateAction action, bool recurse, int level)
        {
            if (this.Verbose)
            {
                this.StatusMessageTextWriter.WriteLine("{0} {1}...", (action == AddOrUpdateAction.AddOnly) ? "adding" : "Adding or updating", directoryName);
            }
            if (level == 0)
            {
                this._addOperationCanceled = false;
                this.OnAddStarted();
            }
            if (this._addOperationCanceled)
            {
                return null;
            }
            string fileName = rootDirectoryPathInArchive;
            ZipEntry entry = null;
            if (level > 0)
            {
                int length = directoryName.Length;
                for (int i = level; i > 0; i--)
                {
                    length = directoryName.LastIndexOfAny(@"/\".ToCharArray(), length - 1, length - 1);
                }
                fileName = directoryName.Substring(length + 1);
                fileName = Path.Combine(rootDirectoryPathInArchive, fileName);
            }
            if ((level > 0) || (rootDirectoryPathInArchive != ""))
            {
                entry = ZipEntry.CreateFromFile(directoryName, fileName);
                entry._container = new ZipContainer(this);
                entry.AlternateEncoding = this.AlternateEncoding;
                entry.AlternateEncodingUsage = this.AlternateEncodingUsage;
                entry.MarkAsDirectory();
                entry.EmitTimesInWindowsFormatWhenSaving = this._emitNtfsTimes;
                entry.EmitTimesInUnixFormatWhenSaving = this._emitUnixTimes;
                if (!this._entries.ContainsKey(entry.FileName))
                {
                    this.InternalAddEntry(entry.FileName, entry);
                    this.AfterAddEntry(entry);
                }
                fileName = entry.FileName;
            }
            if (!this._addOperationCanceled)
            {
                string[] files = Directory.GetFiles(directoryName);
                if (recurse)
                {
                    foreach (string str2 in files)
                    {
                        if (this._addOperationCanceled)
                        {
                            break;
                        }
                        if (action == AddOrUpdateAction.AddOnly)
                        {
                            this.AddFile(str2, fileName);
                        }
                        else
                        {
                            this.UpdateFile(str2, fileName);
                        }
                    }
                    if (!this._addOperationCanceled)
                    {
                        string[] directories = Directory.GetDirectories(directoryName);
                        foreach (string str3 in directories)
                        {
                            FileAttributes attributes = File.GetAttributes(str3);
                            if (this.AddDirectoryWillTraverseReparsePoints || ((attributes & FileAttributes.ReparsePoint) == 0))
                            {
                                this.AddOrUpdateDirectoryImpl(str3, rootDirectoryPathInArchive, action, recurse, level + 1);
                            }
                        }
                    }
                }
            }
            if (level == 0)
            {
                this.OnAddCompleted();
            }
            return entry;
        }

        public void AddSelectedFiles(string selectionCriteria)
        {
            this.AddSelectedFiles(selectionCriteria, ".", null, false);
        }

        public void AddSelectedFiles(string selectionCriteria, bool recurseDirectories)
        {
            this.AddSelectedFiles(selectionCriteria, ".", null, recurseDirectories);
        }

        public void AddSelectedFiles(string selectionCriteria, string directoryOnDisk)
        {
            this.AddSelectedFiles(selectionCriteria, directoryOnDisk, null, false);
        }

        public void AddSelectedFiles(string selectionCriteria, string directoryOnDisk, bool recurseDirectories)
        {
            this.AddSelectedFiles(selectionCriteria, directoryOnDisk, null, recurseDirectories);
        }

        public void AddSelectedFiles(string selectionCriteria, string directoryOnDisk, string directoryPathInArchive)
        {
            this.AddSelectedFiles(selectionCriteria, directoryOnDisk, directoryPathInArchive, false);
        }

        public void AddSelectedFiles(string selectionCriteria, string directoryOnDisk, string directoryPathInArchive, bool recurseDirectories)
        {
            this._AddOrUpdateSelectedFiles(selectionCriteria, directoryOnDisk, directoryPathInArchive, recurseDirectories, false);
        }

        internal void AfterAddEntry(ZipEntry entry)
        {
            EventHandler<AddProgressEventArgs> addProgress = this.AddProgress;
            if (addProgress != null)
            {
                AddProgressEventArgs e = AddProgressEventArgs.AfterEntry(this.ArchiveNameForEvent, entry, this._entries.Count);
                addProgress(this, e);
                if (e.Cancel)
                {
                    this._addOperationCanceled = true;
                }
            }
        }

        public static bool CheckZip(string zipFileName)
        {
            return CheckZip(zipFileName, false, null);
        }

        public static bool CheckZip(string zipFileName, bool fixIfNecessary, TextWriter writer)
        {
            ZipFile file = null;
            ZipFile file2 = null;
            bool flag = true;
            try
            {
                file = new ZipFile {
                    FullScan = true
                };
                file.Initialize(zipFileName);
                file2 = Read(zipFileName);
                foreach (ZipEntry entry in file)
                {
                    foreach (ZipEntry entry2 in file2)
                    {
                        if (entry.FileName == entry2.FileName)
                        {
                            if (entry._RelativeOffsetOfLocalHeader != entry2._RelativeOffsetOfLocalHeader)
                            {
                                flag = false;
                                if (writer != null)
                                {
                                    writer.WriteLine("{0}: mismatch in RelativeOffsetOfLocalHeader  (0x{1:X16} != 0x{2:X16})", entry.FileName, entry._RelativeOffsetOfLocalHeader, entry2._RelativeOffsetOfLocalHeader);
                                }
                            }
                            if (entry._CompressedSize != entry2._CompressedSize)
                            {
                                flag = false;
                                if (writer != null)
                                {
                                    writer.WriteLine("{0}: mismatch in CompressedSize  (0x{1:X16} != 0x{2:X16})", entry.FileName, entry._CompressedSize, entry2._CompressedSize);
                                }
                            }
                            if (entry._UncompressedSize != entry2._UncompressedSize)
                            {
                                flag = false;
                                if (writer != null)
                                {
                                    writer.WriteLine("{0}: mismatch in UncompressedSize  (0x{1:X16} != 0x{2:X16})", entry.FileName, entry._UncompressedSize, entry2._UncompressedSize);
                                }
                            }
                            if (entry.CompressionMethod != entry2.CompressionMethod)
                            {
                                flag = false;
                                if (writer != null)
                                {
                                    writer.WriteLine("{0}: mismatch in CompressionMethod  (0x{1:X4} != 0x{2:X4})", entry.FileName, entry.CompressionMethod, entry2.CompressionMethod);
                                }
                            }
                            if (entry.Crc != entry2.Crc)
                            {
                                flag = false;
                                if (writer != null)
                                {
                                    writer.WriteLine("{0}: mismatch in Crc32  (0x{1:X4} != 0x{2:X4})", entry.FileName, entry.Crc, entry2.Crc);
                                }
                            }
                            break;
                        }
                    }
                }
                file2.Dispose();
                file2 = null;
                if (!(flag || !fixIfNecessary))
                {
                    string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(zipFileName);
                    fileNameWithoutExtension = string.Format("{0}_fixed.zip", fileNameWithoutExtension);
                    file.Save(fileNameWithoutExtension);
                }
            }
            finally
            {
                if (file != null)
                {
                    file.Dispose();
                }
                if (file2 != null)
                {
                    file2.Dispose();
                }
            }
            return flag;
        }

        public static bool CheckZipPassword(string zipFileName, string password)
        {
            bool flag = false;
            try
            {
                using (ZipFile file = Read(zipFileName))
                {
                    foreach (ZipEntry entry in file)
                    {
                        if (!(entry.IsDirectory || !entry.UsesEncryption))
                        {
                            entry.ExtractWithPassword(Stream.Null, password);
                        }
                    }
                }
                flag = true;
            }
            catch (BadPasswordException)
            {
            }
            return flag;
        }

        private void CleanupAfterSaveOperation()
        {
            if (this._name != null)
            {
                if (this._writestream != null)
                {
                    try
                    {
                        this._writestream.Dispose();
                    }
                    catch (IOException)
                    {
                    }
                }
                this._writestream = null;
                if (this._temporaryFileName != null)
                {
                    this.RemoveTempFile();
                    this._temporaryFileName = null;
                }
            }
        }

        public bool ContainsEntry(string name)
        {
            return this._entries.ContainsKey(SharedUtilities.NormalizePathForUseInZipFile(name));
        }

        private void DeleteFileWithRetry(string filename)
        {
            bool flag = false;
            int num = 3;
            for (int i = 0; (i < num) && !flag; i++)
            {
                try
                {
                    File.Delete(filename);
                    flag = true;
                }
                catch (UnauthorizedAccessException)
                {
                    Console.WriteLine("************************************************** Retry delete.");
                    Thread.Sleep((int) (200 + (i * 200)));
                }
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposeManagedResources)
        {
            if (!this._disposed)
            {
                if (disposeManagedResources)
                {
                    if (this._ReadStreamIsOurs && (this._readstream != null))
                    {
                        this._readstream.Dispose();
                        this._readstream = null;
                    }
                    if (((this._temporaryFileName != null) && (this._name != null)) && (this._writestream != null))
                    {
                        this._writestream.Dispose();
                        this._writestream = null;
                    }
                    if (this.ParallelDeflater != null)
                    {
                        this.ParallelDeflater.Dispose();
                        this.ParallelDeflater = null;
                    }
                }
                this._disposed = true;
            }
        }

        private string EnsureendInSlash(string s)
        {
            if (s.EndsWith(@"\"))
            {
                return s;
            }
            return (s + @"\");
        }

        public void ExtractAll(string path)
        {
            this._InternalExtractAll(path, true);
        }

        public void ExtractAll(string path, ExtractExistingFileAction extractExistingFile)
        {
            this.ExtractExistingFile = extractExistingFile;
            this._InternalExtractAll(path, true);
        }

        private static void ExtractResourceToFile(Assembly a, string resourceName, string filename)
        {
            int count = 0;
            byte[] buffer = new byte[0x400];
            using (Stream stream = a.GetManifestResourceStream(resourceName))
            {
                if (stream == null)
                {
                    throw new ZipException(string.Format("missing resource '{0}'", resourceName));
                }
                using (FileStream stream2 = File.OpenWrite(filename))
                {
                    do
                    {
                        count = stream.Read(buffer, 0, buffer.Length);
                        stream2.Write(buffer, 0, count);
                    }
                    while (count > 0);
                }
            }
        }

        public void ExtractSelectedEntries(string selectionCriteria)
        {
            foreach (ZipEntry entry in this.SelectEntries(selectionCriteria))
            {
                entry.Password = this._Password;
                entry.Extract();
            }
        }

        public void ExtractSelectedEntries(string selectionCriteria, ExtractExistingFileAction extractExistingFile)
        {
            foreach (ZipEntry entry in this.SelectEntries(selectionCriteria))
            {
                entry.Password = this._Password;
                entry.Extract(extractExistingFile);
            }
        }

        public void ExtractSelectedEntries(string selectionCriteria, string directoryPathInArchive)
        {
            foreach (ZipEntry entry in this.SelectEntries(selectionCriteria, directoryPathInArchive))
            {
                entry.Password = this._Password;
                entry.Extract();
            }
        }

        public void ExtractSelectedEntries(string selectionCriteria, string directoryInArchive, string extractDirectory)
        {
            foreach (ZipEntry entry in this.SelectEntries(selectionCriteria, directoryInArchive))
            {
                entry.Password = this._Password;
                entry.Extract(extractDirectory);
            }
        }

        public void ExtractSelectedEntries(string selectionCriteria, string directoryPathInArchive, string extractDirectory, ExtractExistingFileAction extractExistingFile)
        {
            foreach (ZipEntry entry in this.SelectEntries(selectionCriteria, directoryPathInArchive))
            {
                entry.Password = this._Password;
                entry.Extract(extractDirectory, extractExistingFile);
            }
        }

        public static void FixZipDirectory(string zipFileName)
        {
            using (ZipFile file = new ZipFile())
            {
                file.FullScan = true;
                file.Initialize(zipFileName);
                file.Save(zipFileName);
            }
        }

        internal static string GenerateTempPathname(string dir, string extension)
        {
            string path = null;
            string name = Assembly.GetExecutingAssembly().GetName().Name;
            do
            {
                string str3 = Guid.NewGuid().ToString();
                string str4 = string.Format("{0}-{1}-{2}.{3}", new object[] { name, DateTime.Now.ToString("yyyyMMMdd-HHmmss"), str3, extension });
                path = Path.Combine(dir, str4);
            }
            while (File.Exists(path) || Directory.Exists(path));
            return path;
        }

        public IEnumerator<ZipEntry> GetEnumerator()
        {
            foreach (ZipEntry iteratorVariable0 in this._entries.Values)
            {
                yield return iteratorVariable0;
            }
        }

        [DispId(-4)]
        public IEnumerator GetNewEnum()
        {
            return this.GetEnumerator();
        }

        public void Initialize(string fileName)
        {
            try
            {
                this._InitInstance(fileName, null);
            }
            catch (Exception exception)
            {
                throw new ZipException(string.Format("{0} is not a valid zip file", fileName), exception);
            }
        }

        internal void InternalAddEntry(string name, ZipEntry entry)
        {
            this._entries.Add(name, entry);
            this._zipEntriesAsList = null;
            this._contentsChanged = true;
        }

        public static bool IsZipFile(string fileName)
        {
            return IsZipFile(fileName, false);
        }

        public static bool IsZipFile(Stream stream, bool testExtract)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }
            bool flag = false;
            try
            {
                if (!stream.CanRead)
                {
                    return false;
                }
                Stream @null = Stream.Null;
                using (ZipFile file = Read(stream, null, null, null))
                {
                    if (testExtract)
                    {
                        foreach (ZipEntry entry in file)
                        {
                            if (!entry.IsDirectory)
                            {
                                entry.Extract(@null);
                            }
                        }
                    }
                }
                flag = true;
            }
            catch (IOException)
            {
            }
            catch (ZipException)
            {
            }
            return flag;
        }

        public static bool IsZipFile(string fileName, bool testExtract)
        {
            bool flag = false;
            try
            {
                if (!File.Exists(fileName))
                {
                    return false;
                }
                using (FileStream stream = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    flag = IsZipFile(stream, testExtract);
                }
            }
            catch (IOException)
            {
            }
            catch (ZipException)
            {
            }
            return flag;
        }

        private static void NotifyEntriesSaveComplete(ICollection<ZipEntry> c)
        {
            foreach (ZipEntry entry in c)
            {
                entry.NotifySaveComplete();
            }
        }

        internal void NotifyEntryChanged()
        {
            this._contentsChanged = true;
        }

        private void OnAddCompleted()
        {
            EventHandler<AddProgressEventArgs> addProgress = this.AddProgress;
            if (addProgress != null)
            {
                AddProgressEventArgs e = AddProgressEventArgs.Completed(this.ArchiveNameForEvent);
                addProgress(this, e);
            }
        }

        private void OnAddStarted()
        {
            EventHandler<AddProgressEventArgs> addProgress = this.AddProgress;
            if (addProgress != null)
            {
                AddProgressEventArgs e = AddProgressEventArgs.Started(this.ArchiveNameForEvent);
                addProgress(this, e);
                if (e.Cancel)
                {
                    this._addOperationCanceled = true;
                }
            }
        }

        private void OnExtractAllCompleted(string path)
        {
            EventHandler<ExtractProgressEventArgs> extractProgress = this.ExtractProgress;
            if (extractProgress != null)
            {
                ExtractProgressEventArgs e = ExtractProgressEventArgs.ExtractAllCompleted(this.ArchiveNameForEvent, path);
                extractProgress(this, e);
            }
        }

        private void OnExtractAllStarted(string path)
        {
            EventHandler<ExtractProgressEventArgs> extractProgress = this.ExtractProgress;
            if (extractProgress != null)
            {
                ExtractProgressEventArgs e = ExtractProgressEventArgs.ExtractAllStarted(this.ArchiveNameForEvent, path);
                extractProgress(this, e);
            }
        }

        internal bool OnExtractBlock(ZipEntry entry, long bytesWritten, long totalBytesToWrite)
        {
            EventHandler<ExtractProgressEventArgs> extractProgress = this.ExtractProgress;
            if (extractProgress != null)
            {
                ExtractProgressEventArgs e = ExtractProgressEventArgs.ByteUpdate(this.ArchiveNameForEvent, entry, bytesWritten, totalBytesToWrite);
                extractProgress(this, e);
                if (e.Cancel)
                {
                    this._extractOperationCanceled = true;
                }
            }
            return this._extractOperationCanceled;
        }

        private void OnExtractEntry(int current, bool before, ZipEntry currentEntry, string path)
        {
            EventHandler<ExtractProgressEventArgs> extractProgress = this.ExtractProgress;
            if (extractProgress != null)
            {
                ExtractProgressEventArgs e = new ExtractProgressEventArgs(this.ArchiveNameForEvent, before, this._entries.Count, current, currentEntry, path);
                extractProgress(this, e);
                if (e.Cancel)
                {
                    this._extractOperationCanceled = true;
                }
            }
        }

        internal bool OnExtractExisting(ZipEntry entry, string path)
        {
            EventHandler<ExtractProgressEventArgs> extractProgress = this.ExtractProgress;
            if (extractProgress != null)
            {
                ExtractProgressEventArgs e = ExtractProgressEventArgs.ExtractExisting(this.ArchiveNameForEvent, entry, path);
                extractProgress(this, e);
                if (e.Cancel)
                {
                    this._extractOperationCanceled = true;
                }
            }
            return this._extractOperationCanceled;
        }

        internal void OnReadBytes(ZipEntry entry)
        {
            EventHandler<ReadProgressEventArgs> readProgress = this.ReadProgress;
            if (readProgress != null)
            {
                ReadProgressEventArgs e = ReadProgressEventArgs.ByteUpdate(this.ArchiveNameForEvent, entry, this.ReadStream.Position, this.LengthOfReadStream);
                readProgress(this, e);
            }
        }

        private void OnReadCompleted()
        {
            EventHandler<ReadProgressEventArgs> readProgress = this.ReadProgress;
            if (readProgress != null)
            {
                ReadProgressEventArgs e = ReadProgressEventArgs.Completed(this.ArchiveNameForEvent);
                readProgress(this, e);
            }
        }

        internal void OnReadEntry(bool before, ZipEntry entry)
        {
            EventHandler<ReadProgressEventArgs> readProgress = this.ReadProgress;
            if (readProgress != null)
            {
                ReadProgressEventArgs e = before ? ReadProgressEventArgs.Before(this.ArchiveNameForEvent, this._entries.Count) : ReadProgressEventArgs.After(this.ArchiveNameForEvent, entry, this._entries.Count);
                readProgress(this, e);
            }
        }

        private void OnReadStarted()
        {
            EventHandler<ReadProgressEventArgs> readProgress = this.ReadProgress;
            if (readProgress != null)
            {
                ReadProgressEventArgs e = ReadProgressEventArgs.Started(this.ArchiveNameForEvent);
                readProgress(this, e);
            }
        }

        internal bool OnSaveBlock(ZipEntry entry, long bytesXferred, long totalBytesToXfer)
        {
            EventHandler<SaveProgressEventArgs> saveProgress = this.SaveProgress;
            if (saveProgress != null)
            {
                SaveProgressEventArgs e = SaveProgressEventArgs.ByteUpdate(this.ArchiveNameForEvent, entry, bytesXferred, totalBytesToXfer);
                saveProgress(this, e);
                if (e.Cancel)
                {
                    this._saveOperationCanceled = true;
                }
            }
            return this._saveOperationCanceled;
        }

        private void OnSaveCompleted()
        {
            EventHandler<SaveProgressEventArgs> saveProgress = this.SaveProgress;
            if (saveProgress != null)
            {
                SaveProgressEventArgs e = SaveProgressEventArgs.Completed(this.ArchiveNameForEvent);
                saveProgress(this, e);
            }
        }

        private void OnSaveEntry(int current, ZipEntry entry, bool before)
        {
            EventHandler<SaveProgressEventArgs> saveProgress = this.SaveProgress;
            if (saveProgress != null)
            {
                SaveProgressEventArgs e = new SaveProgressEventArgs(this.ArchiveNameForEvent, before, this._entries.Count, current, entry);
                saveProgress(this, e);
                if (e.Cancel)
                {
                    this._saveOperationCanceled = true;
                }
            }
        }

        private void OnSaveEvent(ZipProgressEventType eventFlavor)
        {
            EventHandler<SaveProgressEventArgs> saveProgress = this.SaveProgress;
            if (saveProgress != null)
            {
                SaveProgressEventArgs e = new SaveProgressEventArgs(this.ArchiveNameForEvent, eventFlavor);
                saveProgress(this, e);
                if (e.Cancel)
                {
                    this._saveOperationCanceled = true;
                }
            }
        }

        private void OnSaveStarted()
        {
            EventHandler<SaveProgressEventArgs> saveProgress = this.SaveProgress;
            if (saveProgress != null)
            {
                SaveProgressEventArgs e = SaveProgressEventArgs.Started(this.ArchiveNameForEvent);
                saveProgress(this, e);
                if (e.Cancel)
                {
                    this._saveOperationCanceled = true;
                }
            }
        }

        internal bool OnSingleEntryExtract(ZipEntry entry, string path, bool before)
        {
            EventHandler<ExtractProgressEventArgs> extractProgress = this.ExtractProgress;
            if (extractProgress != null)
            {
                ExtractProgressEventArgs e = before ? ExtractProgressEventArgs.BeforeExtractEntry(this.ArchiveNameForEvent, entry, path) : ExtractProgressEventArgs.AfterExtractEntry(this.ArchiveNameForEvent, entry, path);
                extractProgress(this, e);
                if (e.Cancel)
                {
                    this._extractOperationCanceled = true;
                }
            }
            return this._extractOperationCanceled;
        }

        internal bool OnZipErrorSaving(ZipEntry entry, Exception exc)
        {
            if (this.ZipError != null)
            {
                lock (this.LOCK)
                {
                    ZipErrorEventArgs e = ZipErrorEventArgs.Saving(this.Name, entry, exc);
                    this.ZipError(this, e);
                    if (e.Cancel)
                    {
                        this._saveOperationCanceled = true;
                    }
                }
            }
            return this._saveOperationCanceled;
        }

        public static ZipFile Read(Stream zipStream)
        {
            return Read(zipStream, null, null, null);
        }

        public static ZipFile Read(string fileName)
        {
            return Read(fileName, null, null, null);
        }

        public static ZipFile Read(Stream zipStream, ReadOptions options)
        {
            if (options == null)
            {
                throw new ArgumentNullException("options");
            }
            return Read(zipStream, options.StatusMessageWriter, options.Encoding, options.ReadProgress);
        }

        public static ZipFile Read(string fileName, ReadOptions options)
        {
            if (options == null)
            {
                throw new ArgumentNullException("options");
            }
            return Read(fileName, options.StatusMessageWriter, options.Encoding, options.ReadProgress);
        }

        private static ZipFile Read(Stream zipStream, TextWriter statusMessageWriter, Encoding encoding, EventHandler<ReadProgressEventArgs> readProgress)
        {
            if (zipStream == null)
            {
                throw new ArgumentNullException("zipStream");
            }
            ZipFile zf = new ZipFile {
                _StatusMessageTextWriter = statusMessageWriter,
                _alternateEncoding = encoding ?? DefaultEncoding,
                _alternateEncodingUsage = ZipOption.Always
            };
            if (readProgress != null)
            {
                zf.ReadProgress += readProgress;
            }
            zf._readstream = (zipStream.Position == 0L) ? zipStream : new OffsetStream(zipStream);
            zf._ReadStreamIsOurs = false;
            if (zf.Verbose)
            {
                zf._StatusMessageTextWriter.WriteLine("reading from stream...");
            }
            ReadIntoInstance(zf);
            return zf;
        }

        private static ZipFile Read(string fileName, TextWriter statusMessageWriter, Encoding encoding, EventHandler<ReadProgressEventArgs> readProgress)
        {
            ZipFile zf = new ZipFile {
                AlternateEncoding = encoding ?? DefaultEncoding,
                AlternateEncodingUsage = ZipOption.Always,
                _StatusMessageTextWriter = statusMessageWriter,
                _name = fileName
            };
            if (readProgress != null)
            {
                zf.ReadProgress = readProgress;
            }
            if (zf.Verbose)
            {
                zf._StatusMessageTextWriter.WriteLine("reading from {0}...", fileName);
            }
            ReadIntoInstance(zf);
            zf._fileAlreadyExists = true;
            return zf;
        }

        private static void ReadCentralDirectory(ZipFile zf)
        {
            ZipEntry entry;
            bool flag = false;
            Dictionary<string, object> previouslySeen = new Dictionary<string, object>();
            while ((entry = ZipEntry.ReadDirEntry(zf, previouslySeen)) != null)
            {
                entry.ResetDirEntry();
                zf.OnReadEntry(true, null);
                if (zf.Verbose)
                {
                    zf.StatusMessageTextWriter.WriteLine("entry {0}", entry.FileName);
                }
                zf._entries.Add(entry.FileName, entry);
                if (entry._InputUsesZip64)
                {
                    flag = true;
                }
                previouslySeen.Add(entry.FileName, null);
            }
            if (flag)
            {
                zf.UseZip64WhenSaving = Zip64Option.Always;
            }
            if (zf._locEndOfCDS > 0L)
            {
                zf.ReadStream.Seek(zf._locEndOfCDS, SeekOrigin.Begin);
            }
            ReadCentralDirectoryFooter(zf);
            if (!(!zf.Verbose || string.IsNullOrEmpty(zf.Comment)))
            {
                zf.StatusMessageTextWriter.WriteLine("Zip file Comment: {0}", zf.Comment);
            }
            if (zf.Verbose)
            {
                zf.StatusMessageTextWriter.WriteLine("read in {0} entries.", zf._entries.Count);
            }
            zf.OnReadCompleted();
        }

        private static void ReadCentralDirectoryFooter(ZipFile zf)
        {
            Stream readStream = zf.ReadStream;
            int num = SharedUtilities.ReadSignature(readStream);
            byte[] buffer = null;
            int startIndex = 0;
            if (num == 0x6064b50L)
            {
                buffer = new byte[0x34];
                readStream.Read(buffer, 0, buffer.Length);
                long num3 = BitConverter.ToInt64(buffer, 0);
                if (num3 < 0x2cL)
                {
                    throw new ZipException("Bad size in the ZIP64 Central Directory.");
                }
                zf._versionMadeBy = BitConverter.ToUInt16(buffer, startIndex);
                startIndex += 2;
                zf._versionNeededToExtract = BitConverter.ToUInt16(buffer, startIndex);
                startIndex += 2;
                zf._diskNumberWithCd = BitConverter.ToUInt32(buffer, startIndex);
                startIndex += 2;
                buffer = new byte[num3 - 0x2cL];
                readStream.Read(buffer, 0, buffer.Length);
                if (SharedUtilities.ReadSignature(readStream) != 0x7064b50L)
                {
                    throw new ZipException("Inconsistent metadata in the ZIP64 Central Directory.");
                }
                buffer = new byte[0x10];
                readStream.Read(buffer, 0, buffer.Length);
                num = SharedUtilities.ReadSignature(readStream);
            }
            if (num != 0x6054b50L)
            {
                readStream.Seek(-4L, SeekOrigin.Current);
                throw new BadReadException(string.Format("Bad signature ({0:X8}) at position 0x{1:X8}", num, readStream.Position));
            }
            buffer = new byte[0x10];
            zf.ReadStream.Read(buffer, 0, buffer.Length);
            if (zf._diskNumberWithCd == 0)
            {
                zf._diskNumberWithCd = BitConverter.ToUInt16(buffer, 2);
            }
            ReadZipFileComment(zf);
        }

        private static uint ReadFirstFourBytes(Stream s)
        {
            return (uint) SharedUtilities.ReadInt(s);
        }

        private static void ReadIntoInstance(ZipFile zf)
        {
            Stream readStream = zf.ReadStream;
            try
            {
                zf._readName = zf._name;
                if (!readStream.CanSeek)
                {
                    ReadIntoInstance_Orig(zf);
                    return;
                }
                zf.OnReadStarted();
                if (ReadFirstFourBytes(readStream) == 0x6054b50)
                {
                    return;
                }
                int num2 = 0;
                bool flag = false;
                long offset = readStream.Length - 0x40L;
                long num4 = Math.Max((long) (readStream.Length - 0x4000L), (long) 10L);
                do
                {
                    if (offset < 0L)
                    {
                        offset = 0L;
                    }
                    readStream.Seek(offset, SeekOrigin.Begin);
                    if (SharedUtilities.FindSignature(readStream, 0x6054b50) != -1L)
                    {
                        flag = true;
                    }
                    else
                    {
                        if (offset == 0L)
                        {
                            break;
                        }
                        num2++;
                        offset -= (0x20 * (num2 + 1)) * num2;
                    }
                }
                while (!flag && (offset > num4));
                if (flag)
                {
                    zf._locEndOfCDS = readStream.Position - 4L;
                    byte[] buffer = new byte[0x10];
                    readStream.Read(buffer, 0, buffer.Length);
                    zf._diskNumberWithCd = BitConverter.ToUInt16(buffer, 2);
                    if (zf._diskNumberWithCd == 0xffff)
                    {
                        throw new ZipException("Spanned archives with more than 65534 segments are not supported at this time.");
                    }
                    zf._diskNumberWithCd++;
                    int startIndex = 12;
                    uint num7 = BitConverter.ToUInt32(buffer, startIndex);
                    if (num7 == uint.MaxValue)
                    {
                        Zip64SeekToCentralDirectory(zf);
                    }
                    else
                    {
                        zf._OffsetOfCentralDirectory = num7;
                        readStream.Seek((long) num7, SeekOrigin.Begin);
                    }
                    ReadCentralDirectory(zf);
                }
                else
                {
                    readStream.Seek(0L, SeekOrigin.Begin);
                    ReadIntoInstance_Orig(zf);
                }
            }
            catch (Exception exception)
            {
                if (zf._ReadStreamIsOurs && (zf._readstream != null))
                {
                    try
                    {
                        zf._readstream.Dispose();
                        zf._readstream = null;
                    }
                    finally
                    {
                    }
                }
                throw new ZipException("Cannot read that as a ZipFile", exception);
            }
            zf._contentsChanged = false;
        }

        private static void ReadIntoInstance_Orig(ZipFile zf)
        {
            ZipEntry entry;
            zf.OnReadStarted();
            zf._entries = new Dictionary<string, ZipEntry>();
            if (zf.Verbose)
            {
                if (zf.Name == null)
                {
                    zf.StatusMessageTextWriter.WriteLine("Reading zip from stream...");
                }
                else
                {
                    zf.StatusMessageTextWriter.WriteLine("Reading zip {0}...", zf.Name);
                }
            }
            bool first = true;
            ZipContainer zc = new ZipContainer(zf);
            while ((entry = ZipEntry.ReadEntry(zc, first)) != null)
            {
                if (zf.Verbose)
                {
                    zf.StatusMessageTextWriter.WriteLine("  {0}", entry.FileName);
                }
                zf._entries.Add(entry.FileName, entry);
                first = false;
            }
            try
            {
                ZipEntry entry2;
                Dictionary<string, object> previouslySeen = new Dictionary<string, object>();
                while ((entry2 = ZipEntry.ReadDirEntry(zf, previouslySeen)) != null)
                {
                    ZipEntry entry3 = zf._entries[entry2.FileName];
                    if (entry3 != null)
                    {
                        entry3._Comment = entry2.Comment;
                        if (entry2.IsDirectory)
                        {
                            entry3.MarkAsDirectory();
                        }
                    }
                    previouslySeen.Add(entry2.FileName, null);
                }
                if (zf._locEndOfCDS > 0L)
                {
                    zf.ReadStream.Seek(zf._locEndOfCDS, SeekOrigin.Begin);
                }
                ReadCentralDirectoryFooter(zf);
                if (!(!zf.Verbose || string.IsNullOrEmpty(zf.Comment)))
                {
                    zf.StatusMessageTextWriter.WriteLine("Zip file Comment: {0}", zf.Comment);
                }
            }
            catch (ZipException)
            {
            }
            catch (IOException)
            {
            }
            zf.OnReadCompleted();
        }

        private static void ReadZipFileComment(ZipFile zf)
        {
            byte[] buffer = new byte[2];
            zf.ReadStream.Read(buffer, 0, buffer.Length);
            short num = (short) (buffer[0] + (buffer[1] * 0x100));
            if (num > 0)
            {
                buffer = new byte[num];
                zf.ReadStream.Read(buffer, 0, buffer.Length);
                string str = zf.AlternateEncoding.GetString(buffer, 0, buffer.Length);
                zf.Comment = str;
            }
        }

        public void RemoveEntries(ICollection<ZipEntry> entriesToRemove)
        {
            if (entriesToRemove == null)
            {
                throw new ArgumentNullException("entriesToRemove");
            }
            foreach (ZipEntry entry in entriesToRemove)
            {
                this.RemoveEntry(entry);
            }
        }

        public void RemoveEntries(ICollection<string> entriesToRemove)
        {
            if (entriesToRemove == null)
            {
                throw new ArgumentNullException("entriesToRemove");
            }
            foreach (string str in entriesToRemove)
            {
                this.RemoveEntry(str);
            }
        }

        public void RemoveEntry(ZipEntry entry)
        {
            if (entry == null)
            {
                throw new ArgumentNullException("entry");
            }
            this._entries.Remove(SharedUtilities.NormalizePathForUseInZipFile(entry.FileName));
            this._zipEntriesAsList = null;
            this._contentsChanged = true;
        }

        public void RemoveEntry(string fileName)
        {
            string str = ZipEntry.NameInArchive(fileName, null);
            ZipEntry entry = this[str];
            if (entry == null)
            {
                throw new ArgumentException("The entry you specified was not found in the zip archive.");
            }
            this.RemoveEntry(entry);
        }

        private void RemoveEntryForUpdate(string entryName)
        {
            if (string.IsNullOrEmpty(entryName))
            {
                throw new ArgumentNullException("entryName");
            }
            string directoryPathInArchive = null;
            if (entryName.IndexOf('\\') != -1)
            {
                directoryPathInArchive = Path.GetDirectoryName(entryName);
                entryName = Path.GetFileName(entryName);
            }
            string fileName = ZipEntry.NameInArchive(entryName, directoryPathInArchive);
            if (this[fileName] != null)
            {
                this.RemoveEntry(fileName);
            }
        }

        public int RemoveSelectedEntries(string selectionCriteria)
        {
            ICollection<ZipEntry> entriesToRemove = this.SelectEntries(selectionCriteria);
            this.RemoveEntries(entriesToRemove);
            return entriesToRemove.Count;
        }

        public int RemoveSelectedEntries(string selectionCriteria, string directoryPathInArchive)
        {
            ICollection<ZipEntry> entriesToRemove = this.SelectEntries(selectionCriteria, directoryPathInArchive);
            this.RemoveEntries(entriesToRemove);
            return entriesToRemove.Count;
        }

        private void RemoveTempFile()
        {
            try
            {
                if (File.Exists(this._temporaryFileName))
                {
                    File.Delete(this._temporaryFileName);
                }
            }
            catch (IOException exception)
            {
                if (this.Verbose)
                {
                    this.StatusMessageTextWriter.WriteLine("ZipFile::Save: could not delete temp file: {0}.", exception.Message);
                }
            }
        }

        private static string ReplaceLeadingDirectory(string original, string pattern, string replacement)
        {
            string str = original.ToUpper();
            string str2 = pattern.ToUpper();
            if (str.IndexOf(str2) != 0)
            {
                return original;
            }
            return (replacement + original.Substring(str2.Length));
        }

        internal void Reset(bool whileSaving)
        {
            if (this._JustSaved)
            {
                using (ZipFile file = new ZipFile())
                {
                    file._readName = file._name = whileSaving ? (this._readName ?? this._name) : this._name;
                    file.AlternateEncoding = this.AlternateEncoding;
                    file.AlternateEncodingUsage = this.AlternateEncodingUsage;
                    ReadIntoInstance(file);
                    foreach (ZipEntry entry in file)
                    {
                        foreach (ZipEntry entry2 in this)
                        {
                            if (entry.FileName == entry2.FileName)
                            {
                                entry2.CopyMetaData(entry);
                                break;
                            }
                        }
                    }
                }
                this._JustSaved = false;
            }
        }

        public void Save()
        {
            try
            {
                bool flag = false;
                this._saveOperationCanceled = false;
                this._numberOfSegmentsForMostRecentSave = 0;
                this.OnSaveStarted();
                if (this.WriteStream == null)
                {
                    throw new BadStateException("You haven't specified where to save the zip.");
                }
                if (!(((this._name == null) || !this._name.EndsWith(".exe")) || this._SavingSfx))
                {
                    throw new BadStateException("You specified an EXE for a plain zip file.");
                }
                if (!this._contentsChanged)
                {
                    this.OnSaveCompleted();
                    if (this.Verbose)
                    {
                        this.StatusMessageTextWriter.WriteLine("No save is necessary....");
                    }
                }
                else
                {
                    this.Reset(true);
                    if (this.Verbose)
                    {
                        this.StatusMessageTextWriter.WriteLine("saving....");
                    }
                    if ((this._entries.Count >= 0xffff) && (this._zip64 == Zip64Option.Default))
                    {
                        throw new ZipException("The number of entries is 65535 or greater. Consider setting the UseZip64WhenSaving property on the ZipFile instance.");
                    }
                    int current = 0;
                    ICollection<ZipEntry> entries = this.SortEntriesBeforeSaving ? this.EntriesSorted : this.Entries;
                    foreach (ZipEntry entry in entries)
                    {
                        this.OnSaveEntry(current, entry, true);
                        entry.Write(this.WriteStream);
                        if (this._saveOperationCanceled)
                        {
                            break;
                        }
                        current++;
                        this.OnSaveEntry(current, entry, false);
                        if (this._saveOperationCanceled)
                        {
                            break;
                        }
                        if (entry.IncludedInMostRecentSave)
                        {
                            flag |= entry.OutputUsedZip64.Value;
                        }
                    }
                    if (!this._saveOperationCanceled)
                    {
                        ZipSegmentedStream writeStream = this.WriteStream as ZipSegmentedStream;
                        this._numberOfSegmentsForMostRecentSave = (writeStream != null) ? writeStream.CurrentSegment : 1;
                        bool flag2 = ZipOutput.WriteCentralDirectoryStructure(this.WriteStream, entries, this._numberOfSegmentsForMostRecentSave, this._zip64, this.Comment, new ZipContainer(this));
                        this.OnSaveEvent(ZipProgressEventType.Saving_AfterSaveTempArchive);
                        this._hasBeenSaved = true;
                        this._contentsChanged = false;
                        flag |= flag2;
                        this._OutputUsesZip64 = new bool?(flag);
                        if ((this._name != null) && ((this._temporaryFileName != null) || (writeStream != null)))
                        {
                            this.WriteStream.Dispose();
                            if (this._saveOperationCanceled)
                            {
                                return;
                            }
                            if (this._fileAlreadyExists && (this._readstream != null))
                            {
                                this._readstream.Close();
                                this._readstream = null;
                                foreach (ZipEntry entry in entries)
                                {
                                    ZipSegmentedStream stream2 = entry._archiveStream as ZipSegmentedStream;
                                    if (stream2 != null)
                                    {
                                        stream2.Dispose();
                                    }
                                    entry._archiveStream = null;
                                }
                            }
                            string path = null;
                            if (File.Exists(this._name))
                            {
                                path = this._name + "." + Path.GetRandomFileName();
                                if (File.Exists(path))
                                {
                                    this.DeleteFileWithRetry(path);
                                }
                                File.Move(this._name, path);
                            }
                            this.OnSaveEvent(ZipProgressEventType.Saving_BeforeRenameTempArchive);
                            File.Move((writeStream != null) ? writeStream.CurrentTempName : this._temporaryFileName, this._name);
                            this.OnSaveEvent(ZipProgressEventType.Saving_AfterRenameTempArchive);
                            if (path != null)
                            {
                                try
                                {
                                    if (File.Exists(path))
                                    {
                                        File.Delete(path);
                                    }
                                }
                                catch
                                {
                                }
                            }
                            this._fileAlreadyExists = true;
                        }
                        NotifyEntriesSaveComplete(entries);
                        this.OnSaveCompleted();
                        this._JustSaved = true;
                    }
                }
            }
            finally
            {
                this.CleanupAfterSaveOperation();
            }
        }

        public void Save(Stream outputStream)
        {
            if (outputStream == null)
            {
                throw new ArgumentNullException("outputStream");
            }
            if (!outputStream.CanWrite)
            {
                throw new ArgumentException("Must be a writable stream.", "outputStream");
            }
            this._name = null;
            this._writestream = new CountingStream(outputStream);
            this._contentsChanged = true;
            this._fileAlreadyExists = false;
            this.Save();
        }

        public void Save(string fileName)
        {
            if (this._name == null)
            {
                this._writestream = null;
            }
            else
            {
                this._readName = this._name;
            }
            this._name = fileName;
            if (Directory.Exists(this._name))
            {
                throw new ZipException("Bad Directory", new ArgumentException("That name specifies an existing directory. Please specify a filename.", "fileName"));
            }
            this._contentsChanged = true;
            this._fileAlreadyExists = File.Exists(this._name);
            this.Save();
        }

        public void SaveSelfExtractor(string exeToGenerate, SelfExtractorFlavor flavor)
        {
            SelfExtractorSaveOptions options = new SelfExtractorSaveOptions {
                Flavor = flavor
            };
            this.SaveSelfExtractor(exeToGenerate, options);
        }

        public void SaveSelfExtractor(string exeToGenerate, SelfExtractorSaveOptions options)
        {
            if (this._name == null)
            {
                this._writestream = null;
            }
            this._SavingSfx = true;
            this._name = exeToGenerate;
            if (Directory.Exists(this._name))
            {
                throw new ZipException("Bad Directory", new ArgumentException("That name specifies an existing directory. Please specify a filename.", "exeToGenerate"));
            }
            this._contentsChanged = true;
            this._fileAlreadyExists = File.Exists(this._name);
            this._SaveSfxStub(exeToGenerate, options);
            this.Save();
            this._SavingSfx = false;
        }

        public ICollection<ZipEntry> SelectEntries(string selectionCriteria)
        {
            FileSelector selector = new FileSelector(selectionCriteria, this.AddDirectoryWillTraverseReparsePoints);
            return selector.SelectEntries(this);
        }

        public ICollection<ZipEntry> SelectEntries(string selectionCriteria, string directoryPathInArchive)
        {
            FileSelector selector = new FileSelector(selectionCriteria, this.AddDirectoryWillTraverseReparsePoints);
            return selector.SelectEntries(this, directoryPathInArchive);
        }

        internal Stream StreamForDiskNumber(uint diskNumber)
        {
            if (((diskNumber + 1) == this._diskNumberWithCd) || ((diskNumber == 0) && (this._diskNumberWithCd == 0)))
            {
                return this.ReadStream;
            }
            return ZipSegmentedStream.ForReading(this._readName ?? this._name, diskNumber, this._diskNumberWithCd);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public override string ToString()
        {
            return string.Format("ZipFile::{0}", this.Name);
        }

        public ZipEntry UpdateDirectory(string directoryName)
        {
            return this.UpdateDirectory(directoryName, null);
        }

        public ZipEntry UpdateDirectory(string directoryName, string directoryPathInArchive)
        {
            return this.AddOrUpdateDirectoryImpl(directoryName, directoryPathInArchive, AddOrUpdateAction.AddOrUpdate);
        }

        public ZipEntry UpdateEntry(string entryName, WriteDelegate writer)
        {
            this.RemoveEntryForUpdate(entryName);
            return this.AddEntry(entryName, writer);
        }

        public ZipEntry UpdateEntry(string entryName, Stream stream)
        {
            this.RemoveEntryForUpdate(entryName);
            return this.AddEntry(entryName, stream);
        }

        public ZipEntry UpdateEntry(string entryName, string content)
        {
            return this.UpdateEntry(entryName, content, Encoding.Default);
        }

        public ZipEntry UpdateEntry(string entryName, byte[] byteContent)
        {
            this.RemoveEntryForUpdate(entryName);
            return this.AddEntry(entryName, byteContent);
        }

        public ZipEntry UpdateEntry(string entryName, OpenDelegate opener, CloseDelegate closer)
        {
            this.RemoveEntryForUpdate(entryName);
            return this.AddEntry(entryName, opener, closer);
        }

        public ZipEntry UpdateEntry(string entryName, string content, Encoding encoding)
        {
            this.RemoveEntryForUpdate(entryName);
            return this.AddEntry(entryName, content, encoding);
        }

        public ZipEntry UpdateFile(string fileName)
        {
            return this.UpdateFile(fileName, null);
        }

        public ZipEntry UpdateFile(string fileName, string directoryPathInArchive)
        {
            string str = ZipEntry.NameInArchive(fileName, directoryPathInArchive);
            if (this[str] != null)
            {
                this.RemoveEntry(str);
            }
            return this.AddFile(fileName, directoryPathInArchive);
        }

        public void UpdateFiles(IEnumerable<string> fileNames)
        {
            this.UpdateFiles(fileNames, null);
        }

        public void UpdateFiles(IEnumerable<string> fileNames, string directoryPathInArchive)
        {
            if (fileNames == null)
            {
                throw new ArgumentNullException("fileNames");
            }
            this.OnAddStarted();
            foreach (string str in fileNames)
            {
                this.UpdateFile(str, directoryPathInArchive);
            }
            this.OnAddCompleted();
        }

        public void UpdateItem(string itemName)
        {
            this.UpdateItem(itemName, null);
        }

        public void UpdateItem(string itemName, string directoryPathInArchive)
        {
            if (File.Exists(itemName))
            {
                this.UpdateFile(itemName, directoryPathInArchive);
            }
            else
            {
                if (!Directory.Exists(itemName))
                {
                    throw new FileNotFoundException(string.Format("That file or directory ({0}) does not exist!", itemName));
                }
                this.UpdateDirectory(itemName, directoryPathInArchive);
            }
        }

        public void UpdateSelectedFiles(string selectionCriteria, string directoryOnDisk, string directoryPathInArchive, bool recurseDirectories)
        {
            this._AddOrUpdateSelectedFiles(selectionCriteria, directoryOnDisk, directoryPathInArchive, recurseDirectories, true);
        }

        private static void Zip64SeekToCentralDirectory(ZipFile zf)
        {
            Stream readStream = zf.ReadStream;
            byte[] buffer = new byte[0x10];
            readStream.Seek(-40L, SeekOrigin.Current);
            readStream.Read(buffer, 0, 0x10);
            long offset = BitConverter.ToInt64(buffer, 8);
            zf._OffsetOfCentralDirectory = uint.MaxValue;
            zf._OffsetOfCentralDirectory64 = offset;
            readStream.Seek(offset, SeekOrigin.Begin);
            uint num2 = (uint) SharedUtilities.ReadInt(readStream);
            if (num2 != 0x6064b50)
            {
                throw new BadReadException(string.Format("  Bad signature (0x{0:X8}) looking for ZIP64 EoCD Record at position 0x{1:X8}", num2, readStream.Position));
            }
            readStream.Read(buffer, 0, 8);
            buffer = new byte[BitConverter.ToInt64(buffer, 0)];
            readStream.Read(buffer, 0, buffer.Length);
            offset = BitConverter.ToInt64(buffer, 0x24);
            readStream.Seek(offset, SeekOrigin.Begin);
        }

        public bool AddDirectoryWillTraverseReparsePoints { get; set; }

        public Encoding AlternateEncoding
        {
            get
            {
                return this._alternateEncoding;
            }
            set
            {
                this._alternateEncoding = value;
            }
        }

        public ZipOption AlternateEncodingUsage
        {
            get
            {
                return this._alternateEncodingUsage;
            }
            set
            {
                this._alternateEncodingUsage = value;
            }
        }

        private string ArchiveNameForEvent
        {
            get
            {
                return ((this._name != null) ? this._name : "(stream)");
            }
        }

        public int BufferSize
        {
            get
            {
                return this._BufferSize;
            }
            set
            {
                this._BufferSize = value;
            }
        }

        public bool CaseSensitiveRetrieval
        {
            get
            {
                return this._CaseSensitiveRetrieval;
            }
            set
            {
                if (value != this._CaseSensitiveRetrieval)
                {
                    this._CaseSensitiveRetrieval = value;
                    this._initEntriesDictionary();
                }
            }
        }

        public int CodecBufferSize { get; set; }

        public string Comment
        {
            get
            {
                return this._Comment;
            }
            set
            {
                this._Comment = value;
                this._contentsChanged = true;
            }
        }

        public Ionic.Zlib.CompressionLevel CompressionLevel { get; set; }

        public Ionic.Zip.CompressionMethod CompressionMethod
        {
            get
            {
                return this._compressionMethod;
            }
            set
            {
                this._compressionMethod = value;
            }
        }

        public int Count
        {
            get
            {
                return this._entries.Count;
            }
        }

        public static Encoding DefaultEncoding
        {
            get
            {
                return _defaultEncoding;
            }
        }

        public bool EmitTimesInUnixFormatWhenSaving
        {
            get
            {
                return this._emitUnixTimes;
            }
            set
            {
                this._emitUnixTimes = value;
            }
        }

        public bool EmitTimesInWindowsFormatWhenSaving
        {
            get
            {
                return this._emitNtfsTimes;
            }
            set
            {
                this._emitNtfsTimes = value;
            }
        }

        public EncryptionAlgorithm Encryption
        {
            get
            {
                return this._Encryption;
            }
            set
            {
                if (value == EncryptionAlgorithm.Unsupported)
                {
                    throw new InvalidOperationException("You may not set Encryption to that value.");
                }
                this._Encryption = value;
            }
        }

        public ICollection<ZipEntry> Entries
        {
            get
            {
                return this._entries.Values;
            }
        }

        public ICollection<ZipEntry> EntriesSorted
        {
            get
            {
                List<ZipEntry> list = new List<ZipEntry>();
                foreach (ZipEntry entry in this.Entries)
                {
                    list.Add(entry);
                }
                StringComparison sc = this.CaseSensitiveRetrieval ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase;
                list.Sort((x, y) => string.Compare(x.FileName, y.FileName, sc));
                return list.AsReadOnly();
            }
        }

        public ICollection<string> EntryFileNames
        {
            get
            {
                return this._entries.Keys;
            }
        }

        public ExtractExistingFileAction ExtractExistingFile { get; set; }

        public bool FlattenFoldersOnExtract { get; set; }

        public bool FullScan { get; set; }

        public string Info
        {
            get
            {
                StringBuilder builder = new StringBuilder();
                builder.Append(string.Format("          ZipFile: {0}\n", this.Name));
                if (!string.IsNullOrEmpty(this._Comment))
                {
                    builder.Append(string.Format("          Comment: {0}\n", this._Comment));
                }
                if (this._versionMadeBy != 0)
                {
                    builder.Append(string.Format("  version made by: 0x{0:X4}\n", this._versionMadeBy));
                }
                if (this._versionNeededToExtract != 0)
                {
                    builder.Append(string.Format("needed to extract: 0x{0:X4}\n", this._versionNeededToExtract));
                }
                builder.Append(string.Format("       uses ZIP64: {0}\n", this.InputUsesZip64));
                builder.Append(string.Format("     disk with CD: {0}\n", this._diskNumberWithCd));
                if (this._OffsetOfCentralDirectory == uint.MaxValue)
                {
                    builder.Append(string.Format("      CD64 offset: 0x{0:X16}\n", this._OffsetOfCentralDirectory64));
                }
                else
                {
                    builder.Append(string.Format("        CD offset: 0x{0:X8}\n", this._OffsetOfCentralDirectory));
                }
                builder.Append("\n");
                foreach (ZipEntry entry in this._entries.Values)
                {
                    builder.Append(entry.Info);
                }
                return builder.ToString();
            }
        }

        public bool? InputUsesZip64
        {
            get
            {
                if (this._entries.Count > 0xfffe)
                {
                    return true;
                }
                foreach (ZipEntry entry in this)
                {
                    if (entry.Source != ZipEntrySource.ZipFile)
                    {
                        return null;
                    }
                    if (entry._InputUsesZip64)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public ZipEntry this[int ix]
        {
            get
            {
                return this.ZipEntriesAsList[ix];
            }
        }

        public ZipEntry this[string fileName]
        {
            get
            {
                string key = SharedUtilities.NormalizePathForUseInZipFile(fileName);
                if (this._entries.ContainsKey(key))
                {
                    return this._entries[key];
                }
                key = key.Replace("/", @"\");
                if (this._entries.ContainsKey(key))
                {
                    return this._entries[key];
                }
                return null;
            }
        }

        private long LengthOfReadStream
        {
            get
            {
                if (this._lengthOfReadStream == -99L)
                {
                    this._lengthOfReadStream = this._ReadStreamIsOurs ? SharedUtilities.GetFileLength(this._name) : -1L;
                }
                return this._lengthOfReadStream;
            }
        }

        public static Version LibraryVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version;
            }
        }

        public int MaxOutputSegmentSize
        {
            get
            {
                return this._maxOutputSegmentSize;
            }
            set
            {
                if ((value < 0x10000) && (value != 0))
                {
                    throw new ZipException("The minimum acceptable segment size is 65536.");
                }
                this._maxOutputSegmentSize = value;
            }
        }

        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

        public int NumberOfSegmentsForMostRecentSave
        {
            get
            {
                return (((int) this._numberOfSegmentsForMostRecentSave) + 1);
            }
        }

        public bool? OutputUsedZip64
        {
            get
            {
                return this._OutputUsesZip64;
            }
        }

        public int ParallelDeflateMaxBufferPairs
        {
            get
            {
                return this._maxBufferPairs;
            }
            set
            {
                if (value < 4)
                {
                    throw new ArgumentOutOfRangeException("ParallelDeflateMaxBufferPairs", "Value must be 4 or greater.");
                }
                this._maxBufferPairs = value;
            }
        }

        public long ParallelDeflateThreshold
        {
            get
            {
                return this._ParallelDeflateThreshold;
            }
            set
            {
                if (((value != 0L) && (value != -1L)) && (value < 0x10000L))
                {
                    throw new ArgumentOutOfRangeException("ParallelDeflateThreshold should be -1, 0, or > 65536");
                }
                this._ParallelDeflateThreshold = value;
            }
        }

        public string Password
        {
            private get
            {
                return this._Password;
            }
            set
            {
                this._Password = value;
                if (this._Password == null)
                {
                    this.Encryption = EncryptionAlgorithm.None;
                }
                else if (this.Encryption == EncryptionAlgorithm.None)
                {
                    this.Encryption = EncryptionAlgorithm.PkzipWeak;
                }
            }
        }

        [Obsolete("use AlternateEncoding instead.")]
        public Encoding ProvisionalAlternateEncoding
        {
            get
            {
                if (this._alternateEncodingUsage == ZipOption.AsNecessary)
                {
                    return this._alternateEncoding;
                }
                return null;
            }
            set
            {
                this._alternateEncoding = value;
                this._alternateEncodingUsage = ZipOption.AsNecessary;
            }
        }

        internal Stream ReadStream
        {
            get
            {
                if ((this._readstream == null) && ((this._readName != null) || (this._name != null)))
                {
                    this._readstream = File.Open(this._readName ?? this._name, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    this._ReadStreamIsOurs = true;
                }
                return this._readstream;
            }
        }

        public bool? RequiresZip64
        {
            get
            {
                if (this._entries.Count > 0xfffe)
                {
                    return true;
                }
                if (!(this._hasBeenSaved && !this._contentsChanged))
                {
                    return null;
                }
                foreach (ZipEntry entry in this._entries.Values)
                {
                    if (entry.RequiresZip64.Value)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public SetCompressionCallback SetCompression { get; set; }

        public bool SortEntriesBeforeSaving { get; set; }

        public TextWriter StatusMessageTextWriter
        {
            get
            {
                return this._StatusMessageTextWriter;
            }
            set
            {
                this._StatusMessageTextWriter = value;
            }
        }

        public CompressionStrategy Strategy
        {
            get
            {
                return this._Strategy;
            }
            set
            {
                this._Strategy = value;
            }
        }

        public string TempFileFolder
        {
            get
            {
                return this._TempFileFolder;
            }
            set
            {
                this._TempFileFolder = value;
                if ((value != null) && !Directory.Exists(value))
                {
                    throw new FileNotFoundException(string.Format("That directory ({0}) does not exist.", value));
                }
            }
        }

        [Obsolete("Beginning with v1.9.1.6 of DotNetZip, this property is obsolete.  It will be removed in a future version of the library. Your applications should  use AlternateEncoding and AlternateEncodingUsage instead.")]
        public bool UseUnicodeAsNecessary
        {
            get
            {
                return ((this._alternateEncoding == Encoding.GetEncoding("UTF-8")) && (this._alternateEncodingUsage == ZipOption.AsNecessary));
            }
            set
            {
                if (value)
                {
                    this._alternateEncoding = Encoding.GetEncoding("UTF-8");
                    this._alternateEncodingUsage = ZipOption.AsNecessary;
                }
                else
                {
                    this._alternateEncoding = DefaultEncoding;
                    this._alternateEncodingUsage = ZipOption.Default;
                }
            }
        }

        public Zip64Option UseZip64WhenSaving
        {
            get
            {
                return this._zip64;
            }
            set
            {
                this._zip64 = value;
            }
        }

        internal bool Verbose
        {
            get
            {
                return (this._StatusMessageTextWriter != null);
            }
        }

        private Stream WriteStream
        {
            get
            {
                if (this._writestream == null)
                {
                    if (this._name == null)
                    {
                        return this._writestream;
                    }
                    if (this._maxOutputSegmentSize != 0)
                    {
                        this._writestream = ZipSegmentedStream.ForWriting(this._name, this._maxOutputSegmentSize);
                        return this._writestream;
                    }
                    SharedUtilities.CreateAndOpenUniqueTempFile(this.TempFileFolder ?? Path.GetDirectoryName(this._name), out this._writestream, out this._temporaryFileName);
                }
                return this._writestream;
            }
            set
            {
                if (value != null)
                {
                    throw new ZipException("Cannot set the stream to a non-null value.");
                }
                this._writestream = null;
            }
        }

        private List<ZipEntry> ZipEntriesAsList
        {
            get
            {
                if (this._zipEntriesAsList == null)
                {
                    this._zipEntriesAsList = new List<ZipEntry>(this._entries.Values);
                }
                return this._zipEntriesAsList;
            }
        }

        public Ionic.Zip.ZipErrorAction ZipErrorAction
        {
            get
            {
                if (this.ZipError != null)
                {
                    this._zipErrorAction = Ionic.Zip.ZipErrorAction.InvokeErrorEvent;
                }
                return this._zipErrorAction;
            }
            set
            {
                this._zipErrorAction = value;
                if ((this._zipErrorAction != Ionic.Zip.ZipErrorAction.InvokeErrorEvent) && (this.ZipError != null))
                {
                    this.ZipError = null;
                }
            }
        }


        private class ExtractorSettings
        {
            public List<string> CopyThroughResources;
            public SelfExtractorFlavor Flavor;
            public List<string> ReferencedAssemblies;
            public List<string> ResourcesToCompile;
        }
    }
}

