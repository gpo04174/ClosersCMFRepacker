﻿namespace Ionic.Zip
{
    using System;

    [Flags]
    public enum ZipEntryTimestamp
    {
        DOS = 1,
        InfoZip1 = 8,
        None = 0,
        Unix = 4,
        Windows = 2
    }
}

