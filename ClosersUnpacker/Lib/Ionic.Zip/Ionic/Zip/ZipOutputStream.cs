﻿namespace Ionic.Zip
{
    using Ionic.Crc;
    using Ionic.Zlib;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Text;

    public class ZipOutputStream : Stream
    {
        private Encoding _alternateEncoding;
        private ZipOption _alternateEncodingUsage;
        private bool _anyEntriesUsedZip64;
        private string _comment;
        private ZipEntry _currentEntry;
        private Stream _deflater;
        private bool _directoryNeededZip64;
        private bool _disposed;
        private bool _DontIgnoreCase;
        private EncryptionAlgorithm _encryption;
        private Stream _encryptor;
        private Dictionary<string, ZipEntry> _entriesWritten;
        private int _entryCount;
        private CrcCalculatorStream _entryOutputStream;
        private bool _exceptionPending;
        private bool _leaveUnderlyingStreamOpen;
        private int _maxBufferPairs;
        private string _name;
        private bool _needToWriteEntryHeader;
        private CountingStream _outputCounter;
        private Stream _outputStream;
        private long _ParallelDeflateThreshold;
        internal string _password;
        private ZipEntryTimestamp _timestamp;
        internal Zip64Option _zip64;
        internal ParallelDeflateOutputStream ParallelDeflater;

        public ZipOutputStream(Stream stream) : this(stream, false)
        {
        }

        public ZipOutputStream(string fileName)
        {
            this._alternateEncodingUsage = ZipOption.Default;
            this._alternateEncoding = Encoding.GetEncoding("IBM437");
            this._maxBufferPairs = 0x10;
            Stream stream = File.Open(fileName, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
            this._Init(stream, false, fileName);
        }

        public ZipOutputStream(Stream stream, bool leaveOpen)
        {
            this._alternateEncodingUsage = ZipOption.Default;
            this._alternateEncoding = Encoding.GetEncoding("IBM437");
            this._maxBufferPairs = 0x10;
            this._Init(stream, leaveOpen, null);
        }

        private void _FinishCurrentEntry()
        {
            if (this._currentEntry != null)
            {
                if (this._needToWriteEntryHeader)
                {
                    this._InitiateCurrentEntry(true);
                }
                this._currentEntry.FinishOutputStream(this._outputStream, this._outputCounter, this._encryptor, this._deflater, this._entryOutputStream);
                this._currentEntry.PostProcessOutput(this._outputStream);
                if (this._currentEntry.OutputUsedZip64.HasValue)
                {
                    this._anyEntriesUsedZip64 |= this._currentEntry.OutputUsedZip64.Value;
                }
                this._outputCounter = null;
                this._encryptor = (Stream) (this._deflater = null);
                this._entryOutputStream = null;
            }
        }

        private void _Init(Stream stream, bool leaveOpen, string name)
        {
            this._outputStream = stream.CanRead ? stream : new CountingStream(stream);
            this.CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
            this.CompressionMethod = Ionic.Zip.CompressionMethod.Deflate;
            this._encryption = EncryptionAlgorithm.None;
            this._entriesWritten = new Dictionary<string, ZipEntry>(StringComparer.Ordinal);
            this._zip64 = Zip64Option.Default;
            this._leaveUnderlyingStreamOpen = leaveOpen;
            this.Strategy = CompressionStrategy.Default;
            this._name = name ?? "(stream)";
            this.ParallelDeflateThreshold = -1L;
        }

        private void _InitiateCurrentEntry(bool finishing)
        {
            this._entriesWritten.Add(this._currentEntry.FileName, this._currentEntry);
            this._entryCount++;
            if ((this._entryCount > 0xfffe) && (this._zip64 == Zip64Option.Default))
            {
                this._exceptionPending = true;
                throw new InvalidOperationException("Too many entries. Consider setting ZipOutputStream.EnableZip64.");
            }
            this._currentEntry.WriteHeader(this._outputStream, finishing ? 0x63 : 0);
            this._currentEntry.StoreRelativeOffset();
            if (!this._currentEntry.IsDirectory)
            {
                this._currentEntry.WriteSecurityMetadata(this._outputStream);
                this._currentEntry.PrepOutputStream(this._outputStream, finishing ? ((long) 0) : ((long) (-1)), out this._outputCounter, out this._encryptor, out this._deflater, out this._entryOutputStream);
            }
            this._needToWriteEntryHeader = false;
        }

        public bool ContainsEntry(string name)
        {
            return this._entriesWritten.ContainsKey(SharedUtilities.NormalizePathForUseInZipFile(name));
        }

        protected override void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing && !this._exceptionPending)
                {
                    this._FinishCurrentEntry();
                    this._directoryNeededZip64 = ZipOutput.WriteCentralDirectoryStructure(this._outputStream, this._entriesWritten.Values, 1, this._zip64, this.Comment, new ZipContainer(this));
                    Stream wrappedStream = null;
                    CountingStream stream2 = this._outputStream as CountingStream;
                    if (stream2 != null)
                    {
                        wrappedStream = stream2.WrappedStream;
                        stream2.Dispose();
                    }
                    else
                    {
                        wrappedStream = this._outputStream;
                    }
                    if (!this._leaveUnderlyingStreamOpen)
                    {
                        wrappedStream.Dispose();
                    }
                    this._outputStream = null;
                }
                this._disposed = true;
            }
        }

        public override void Flush()
        {
        }

        private void InsureUniqueEntry(ZipEntry ze1)
        {
            if (this._entriesWritten.ContainsKey(ze1.FileName))
            {
                this._exceptionPending = true;
                throw new ArgumentException(string.Format("The entry '{0}' already exists in the zip archive.", ze1.FileName));
            }
        }

        public ZipEntry PutNextEntry(string entryName)
        {
            if (string.IsNullOrEmpty(entryName))
            {
                throw new ArgumentNullException("entryName");
            }
            if (this._disposed)
            {
                this._exceptionPending = true;
                throw new InvalidOperationException("The stream has been closed.");
            }
            this._FinishCurrentEntry();
            this._currentEntry = ZipEntry.CreateForZipOutputStream(entryName);
            this._currentEntry._container = new ZipContainer(this);
            this._currentEntry._BitField = (short) (this._currentEntry._BitField | 8);
            this._currentEntry.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
            this._currentEntry.CompressionLevel = this.CompressionLevel;
            this._currentEntry.CompressionMethod = this.CompressionMethod;
            this._currentEntry.Password = this._password;
            this._currentEntry.Encryption = this.Encryption;
            this._currentEntry.AlternateEncoding = this.AlternateEncoding;
            this._currentEntry.AlternateEncodingUsage = this.AlternateEncodingUsage;
            if (entryName.EndsWith("/"))
            {
                this._currentEntry.MarkAsDirectory();
            }
            this._currentEntry.EmitTimesInWindowsFormatWhenSaving = (this._timestamp & ZipEntryTimestamp.Windows) != ZipEntryTimestamp.None;
            this._currentEntry.EmitTimesInUnixFormatWhenSaving = (this._timestamp & ZipEntryTimestamp.Unix) != ZipEntryTimestamp.None;
            this.InsureUniqueEntry(this._currentEntry);
            this._needToWriteEntryHeader = true;
            return this._currentEntry;
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new NotSupportedException("Read");
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotSupportedException("Seek");
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }

        public override string ToString()
        {
            return string.Format("ZipOutputStream::{0}(leaveOpen({1})))", this._name, this._leaveUnderlyingStreamOpen);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            if (this._disposed)
            {
                this._exceptionPending = true;
                throw new InvalidOperationException("The stream has been closed.");
            }
            if (buffer == null)
            {
                this._exceptionPending = true;
                throw new ArgumentNullException("buffer");
            }
            if (this._currentEntry == null)
            {
                this._exceptionPending = true;
                throw new InvalidOperationException("You must call PutNextEntry() before calling Write().");
            }
            if (this._currentEntry.IsDirectory)
            {
                this._exceptionPending = true;
                throw new InvalidOperationException("You cannot Write() data for an entry that is a directory.");
            }
            if (this._needToWriteEntryHeader)
            {
                this._InitiateCurrentEntry(false);
            }
            if (count != 0)
            {
                this._entryOutputStream.Write(buffer, offset, count);
            }
        }

        public Encoding AlternateEncoding
        {
            get
            {
                return this._alternateEncoding;
            }
            set
            {
                this._alternateEncoding = value;
            }
        }

        public ZipOption AlternateEncodingUsage
        {
            get
            {
                return this._alternateEncodingUsage;
            }
            set
            {
                this._alternateEncodingUsage = value;
            }
        }

        public override bool CanRead
        {
            get
            {
                return false;
            }
        }

        public override bool CanSeek
        {
            get
            {
                return false;
            }
        }

        public override bool CanWrite
        {
            get
            {
                return true;
            }
        }

        public int CodecBufferSize { get; set; }

        public string Comment
        {
            get
            {
                return this._comment;
            }
            set
            {
                if (this._disposed)
                {
                    this._exceptionPending = true;
                    throw new InvalidOperationException("The stream has been closed.");
                }
                this._comment = value;
            }
        }

        public Ionic.Zlib.CompressionLevel CompressionLevel { get; set; }

        public Ionic.Zip.CompressionMethod CompressionMethod { get; set; }

        public static Encoding DefaultEncoding
        {
            get
            {
                return Encoding.GetEncoding("IBM437");
            }
        }

        public Zip64Option EnableZip64
        {
            get
            {
                return this._zip64;
            }
            set
            {
                if (this._disposed)
                {
                    this._exceptionPending = true;
                    throw new InvalidOperationException("The stream has been closed.");
                }
                this._zip64 = value;
            }
        }

        public EncryptionAlgorithm Encryption
        {
            get
            {
                return this._encryption;
            }
            set
            {
                if (this._disposed)
                {
                    this._exceptionPending = true;
                    throw new InvalidOperationException("The stream has been closed.");
                }
                if (value == EncryptionAlgorithm.Unsupported)
                {
                    this._exceptionPending = true;
                    throw new InvalidOperationException("You may not set Encryption to that value.");
                }
                this._encryption = value;
            }
        }

        public bool IgnoreCase
        {
            get
            {
                return !this._DontIgnoreCase;
            }
            set
            {
                this._DontIgnoreCase = !value;
            }
        }

        public override long Length
        {
            get
            {
                throw new NotSupportedException();
            }
        }

        internal string Name
        {
            get
            {
                return this._name;
            }
        }

        internal Stream OutputStream
        {
            get
            {
                return this._outputStream;
            }
        }

        public bool OutputUsedZip64
        {
            get
            {
                return (this._anyEntriesUsedZip64 || this._directoryNeededZip64);
            }
        }

        public int ParallelDeflateMaxBufferPairs
        {
            get
            {
                return this._maxBufferPairs;
            }
            set
            {
                if (value < 4)
                {
                    throw new ArgumentOutOfRangeException("ParallelDeflateMaxBufferPairs", "Value must be 4 or greater.");
                }
                this._maxBufferPairs = value;
            }
        }

        public long ParallelDeflateThreshold
        {
            get
            {
                return this._ParallelDeflateThreshold;
            }
            set
            {
                if (((value != 0L) && (value != -1L)) && (value < 0x10000L))
                {
                    throw new ArgumentOutOfRangeException("value must be greater than 64k, or 0, or -1");
                }
                this._ParallelDeflateThreshold = value;
            }
        }

        public string Password
        {
            set
            {
                if (this._disposed)
                {
                    this._exceptionPending = true;
                    throw new InvalidOperationException("The stream has been closed.");
                }
                this._password = value;
                if (this._password == null)
                {
                    this._encryption = EncryptionAlgorithm.None;
                }
                else if (this._encryption == EncryptionAlgorithm.None)
                {
                    this._encryption = EncryptionAlgorithm.PkzipWeak;
                }
            }
        }

        public override long Position
        {
            get
            {
                return this._outputStream.Position;
            }
            set
            {
                throw new NotSupportedException();
            }
        }

        [Obsolete("use AlternateEncoding and AlternateEncodingUsage instead.")]
        public Encoding ProvisionalAlternateEncoding
        {
            get
            {
                if (this._alternateEncodingUsage == ZipOption.AsNecessary)
                {
                    return this._alternateEncoding;
                }
                return null;
            }
            set
            {
                this._alternateEncoding = value;
                this._alternateEncodingUsage = ZipOption.AsNecessary;
            }
        }

        public CompressionStrategy Strategy { get; set; }

        public ZipEntryTimestamp Timestamp
        {
            get
            {
                return this._timestamp;
            }
            set
            {
                if (this._disposed)
                {
                    this._exceptionPending = true;
                    throw new InvalidOperationException("The stream has been closed.");
                }
                this._timestamp = value;
            }
        }

        [Obsolete("Beginning with v1.9.1.6 of DotNetZip, this property is obsolete. It will be removed in a future version of the library. Use AlternateEncoding and AlternateEncodingUsage instead.")]
        public bool UseUnicodeAsNecessary
        {
            get
            {
                return ((this._alternateEncoding == Encoding.UTF8) && (this.AlternateEncodingUsage == ZipOption.AsNecessary));
            }
            set
            {
                if (value)
                {
                    this._alternateEncoding = Encoding.UTF8;
                    this._alternateEncodingUsage = ZipOption.AsNecessary;
                }
                else
                {
                    this._alternateEncoding = DefaultEncoding;
                    this._alternateEncodingUsage = ZipOption.Default;
                }
            }
        }
    }
}

