﻿namespace Ionic.Zip
{
    using System;
    using System.Runtime.InteropServices;
    using System.Runtime.Serialization;

    [Serializable, Guid("ebc25cf6-9120-4283-b972-0e5520d00006")]
    public class ZipException : Exception
    {
        public ZipException()
        {
        }

        public ZipException(string message) : base(message)
        {
        }

        protected ZipException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public ZipException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}

