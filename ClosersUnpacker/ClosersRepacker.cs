﻿namespace Closers.Repacker
{
    using Closers.Repacker.CMF;
    using Closers.Repacker.Commands;
    using System;
    using System.Runtime.CompilerServices;

    public class ClosersRepacker
    {
        private readonly Closers.Repacker.CMF.CMFFileInfoManager _CMFFileeInfoManager = null;
        private readonly Closers.Repacker.CMF.CMFManager _CMFManager = null;
        private readonly Closers.Repacker.Commands.CommandManager _CommandManager = null;
        private readonly Closers.Repacker.Configuration _Configuration = null;
        private readonly Closers.Repacker.Commands.ConsoleCommandSender _ConsoleCommandSender = null;
        private readonly Closers.Repacker.DirectoryManager _DirectoryManager = null;
        private static readonly ClosersRepacker _Instance = new ClosersRepacker();

        public ClosersRepacker()
        {
            this._ConsoleCommandSender = new Closers.Repacker.Commands.ConsoleCommandSender();
            this._CommandManager = new Closers.Repacker.Commands.CommandManager();
            this._DirectoryManager = new Closers.Repacker.DirectoryManager();
            this._Configuration = new Closers.Repacker.Configuration();
            this._CMFFileeInfoManager = new Closers.Repacker.CMF.CMFFileInfoManager();
            this._CMFManager = new Closers.Repacker.CMF.CMFManager();
        }

        public static void Main(string[] args)
        {
            Console.Title = "클로져스 CMF 분해 프로그램 (조장찡)";
            Instance.Start();
        }

        private void Start()
        {
            this.DirectoryManager.GetOriginalCMFDirectory();
            this.DirectoryManager.GetOriginalUnpackDirectory();
            this.Configuration.Load();
            this.CMFFileInfoManager.Load();
            string str = Util.MergeToString<string>(this.CommandManager.Help.GetCommandLabelAndAliases().ToArray(), l => "'" + l + "'", ", ");
            Closers.Repacker.Commands.ConsoleCommandSender sender = new Closers.Repacker.Commands.ConsoleCommandSender();
            sender.SendMessage();
            sender.SendMessage("명령어를 입력해주세요. " + str + "를 입력하면 명령어 목록을 확인할 수 있습니다.");
            sender.SendMessage("명령어는 대소문자를 구분하지 않습니다.");
            sender.SendMessage("");
            Command help = this.CommandManager.Help;
            help.Execute(sender, help.Label, new string[0]);
            sender.SendMessage();
            while (true)
            {
                string line = sender.ReceiveMessage();
                sender.SendMessage();
                try
                {
                    if (!this.CommandManager.ExecuteCommandLine(sender, line))
                    {
                        sender.SendMessage("명령어를 찾을 수 없습니다.");
                    }
                }
                catch (Exception exception)
                {
                    sender.SendMessage("알 수 없는 오류가 발생했습니다.");
                    sender.SendMessage("아래 에러 메세지를 저장 및 캡쳐하여 개발자에게 보고해주세요.");
                    sender.SendMessage();
                    sender.SendMessage("====== 에러 메세지 시작 ======");
                    sender.SendMessage();
                    Console.WriteLine(exception);
                    sender.SendMessage();
                    sender.SendMessage("====== 에러 메세지 종료 ======");
                }
                sender.SendMessage();
            }
        }

        public Closers.Repacker.CMF.CMFFileInfoManager CMFFileInfoManager
        {
            get
            {
                return this._CMFFileeInfoManager;
            }
        }

        public Closers.Repacker.CMF.CMFManager CMFManager
        {
            get
            {
                return this._CMFManager;
            }
        }

        public Closers.Repacker.Commands.CommandManager CommandManager
        {
            get
            {
                return this._CommandManager;
            }
        }

        public Closers.Repacker.Configuration Configuration
        {
            get
            {
                return this._Configuration;
            }
        }

        public Closers.Repacker.Commands.ConsoleCommandSender ConsoleCommandSender
        {
            get
            {
                return this._ConsoleCommandSender;
            }
        }

        public Closers.Repacker.DirectoryManager DirectoryManager
        {
            get
            {
                return this._DirectoryManager;
            }
        }

        public static ClosersRepacker Instance
        {
            get
            {
                return _Instance;
            }
        }

        public string StartPath
        {
            get
            {
                return AppDomain.CurrentDomain.BaseDirectory;
            }
        }

        [Serializable, CompilerGenerated]
        private sealed class CRCS
        {
            public static readonly ClosersRepacker.CRCS CRCS_9 = new ClosersRepacker.CRCS();
            public static Func<string, string> CRCS_9__25_0;

            internal string Start_b__25_0(string l)
            {
                return ("'" + l + "'");
            }
        }
    }
}

