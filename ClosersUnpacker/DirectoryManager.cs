﻿namespace Closers.Repacker
{
    using System;
    using System.IO;

    public class DirectoryManager
    {
        public string GetChangesDirectory(DateTime recordTime)
        {
            return this.GetDirectory(@"Changes\" + recordTime.ToString("yyyy_MM_dd HHmmss"));
        }

        public string GetDirectory(string directoryName)
        {
            string path = ClosersRepacker.Instance.StartPath + directoryName;
            Directory.CreateDirectory(path);
            return Util.Suffix(path, @"\");
        }

        public string GetOriginalCMFDirectory()
        {
            return this.GetDirectory(@"Original\CMF");
        }

        public string GetOriginalUnpackDirectory()
        {
            return this.GetDirectory(@"Original\Unpack");
        }

        public string GetOriginalUnpackDirectory(string fileName)
        {
            return this.GetDirectory(@"Original\Unpack\" + fileName);
        }
    }
}

