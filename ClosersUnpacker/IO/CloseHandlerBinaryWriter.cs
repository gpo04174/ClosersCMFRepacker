﻿namespace Closers.Repacker.IO
{
    using System;
    using System.IO;

    public class CloseHandlerBinaryWriter : CloseHandler<BinaryWriter>
    {
        protected override void HandleClose(BinaryWriter value)
        {
            value.Close();
        }
    }
}

