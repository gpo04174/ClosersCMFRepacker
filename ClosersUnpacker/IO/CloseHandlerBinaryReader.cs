﻿namespace Closers.Repacker.IO
{
    using System;
    using System.IO;

    public class CloseHandlerBinaryReader : CloseHandler<BinaryReader>
    {
        protected override void HandleClose(BinaryReader value)
        {
            value.Close();
        }
    }
}

