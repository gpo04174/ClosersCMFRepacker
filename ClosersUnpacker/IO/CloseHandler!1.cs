﻿namespace Closers.Repacker.IO
{
    using System;

    public abstract class CloseHandler<T> : CloseHandler
    {
        protected CloseHandler()
        {
        }

        protected override void HandleClose(object value)
        {
            this.HandleClose((T) value);
        }

        protected abstract void HandleClose(T value);
        protected override bool IsInstance(object value)
        {
            return (value is T);
        }
    }
}

