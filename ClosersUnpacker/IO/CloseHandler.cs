﻿namespace Closers.Repacker.IO
{
    using System;

    public abstract class CloseHandler
    {
        protected CloseHandler()
        {
        }

        public bool CanClose(object value)
        {
            return this.IsInstance(value);
        }

        public void Close(object value)
        {
            this.HandleClose(value);
        }

        protected abstract void HandleClose(object value);
        protected abstract bool IsInstance(object value);
    }
}

