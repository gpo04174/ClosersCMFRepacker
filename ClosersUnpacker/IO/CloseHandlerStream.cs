﻿namespace Closers.Repacker.IO
{
    using System;
    using System.IO;

    public class CloseHandlerStream : CloseHandler<Stream>
    {
        protected override void HandleClose(Stream value)
        {
            value.Close();
        }
    }
}

