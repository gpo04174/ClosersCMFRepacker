﻿namespace Closers.Repacker.IO
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    public static class IOUtil
    {
        private static readonly List<CloseHandler> CloseHandlerList = new List<CloseHandler>();

        static IOUtil()
        {
            CloseHandlerList.Add(new CloseHandlerStream());
            CloseHandlerList.Add(new CloseHandlerBinaryReader());
            CloseHandlerList.Add(new CloseHandlerBinaryWriter());
        }

        public static void Close(object value)
        {
            CloseHandler handler = CloseHandlerList.Find(delegate (CloseHandler ch) {
                return ch.CanClose(value);
            });
            if (handler != null)handler.Close(value);
        }

        public static void CloseAndDisposeQuitly(IDisposable value)
        {
            CloseQuitly(value);
            if (value != null) value.Dispose();
        }

        public static void CloseQuitly(object value)
        {
            if (value != null)
            {
                try
                {
                    Close(value);
                }
                catch
                {
                }
            }
        }

        public static string ReadString(BinaryReader reader)
        {
            if (reader.ReadBoolean())
            {
                return null;
            }
            int count = reader.ReadInt32();
            byte[] bytes = reader.ReadBytes(count);
            return Encoding.UTF8.GetString(bytes, 0, count);
        }

        public static void WriteString(BinaryWriter writer, string value)
        {
            bool flag = value == null;
            writer.Write(flag);
            if (!flag)
            {
                byte[] bytes = Encoding.UTF8.GetBytes(value);
                writer.Write(bytes.Length);
                writer.Write(bytes, 0, bytes.Length);
            }
        }
    }
}

